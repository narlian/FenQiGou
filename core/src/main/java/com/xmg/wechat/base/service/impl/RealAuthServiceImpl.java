package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.RealAuth;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.mapper.RealAuthMapper;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.RealAuthQueryObject;
import com.xmg.wechat.base.service.IRealAuthService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.BitStatesUtils;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RealAuthServiceImpl implements IRealAuthService {
	@Autowired
	private RealAuthMapper realAuthMapper;
	@Autowired
	private IUserinfoService userinfoService;

	@Override
	public RealAuth get(long realAuthId) {
		return realAuthMapper.selectByPrimaryKey(realAuthId);
	}

	@Override
	public void apply(RealAuth realAuth) {
		Userinfo userinfo = userinfoService.getCurrent();
		if(!userinfo.getIsRealAuth() && userinfo.getRealAuthId() == null){
			RealAuth real = new RealAuth();
			real.setRealName(realAuth.getRealName());
			real.setGender(realAuth.getGender());
			real.setIdNumber(realAuth.getIdNumber());
			real.setBornDate(realAuth.getBornDate());
			real.setAddress(realAuth.getAddress());
			real.setApplier(UserContext.getCurrent());
			real.setApplyTime(new Date());
			real.setImage1(realAuth.getImage1());
			real.setImage2(realAuth.getImage2());
			//保存入库
			realAuthMapper.insert(real);
			//realAuthId设值
			userinfo.setRealAuthId(real.getId());
			userinfoService.updateByPrimaryKey(userinfo);
		}
	}

	@Override
	public PageResult queryPage(RealAuthQueryObject qo) {
		int count = realAuthMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<RealAuth> items = realAuthMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void audit(Long id, int state, String remark) {
//		根据realAuthId查询出RealAuth 对象,判断审核对象处于待审状态
		RealAuth realAuth = this.get(id);
		if(realAuth != null && realAuth.getState() == RealAuth.STATE_NORMAL){
	//		设置审核人, 审核时间和审核意见
			realAuth.setAuditor(UserContext.getCurrent());
			realAuth.setAuditTime(new Date());
			realAuth.setRemark(remark);
	//		根据申请人id查询出对应Userinfo对象
			Userinfo applierUserinfo = userinfoService.selectByPrimaryKey(realAuth.getApplier().getId());
			//判断是审核通过还是审核拒绝
			if(state == RealAuth.STATE_PASS){
				//通过: 把RealAuth的state设为审核通过, 并更新Userinfo的实名认证状态, 真实姓名和身份证号码
				realAuth.setState(RealAuth.STATE_PASS);
				applierUserinfo.addState(BitStatesUtils.OP_REAL_AUTH);
				applierUserinfo.setRealName(realAuth.getRealName());
				applierUserinfo.setIdNumber(realAuth.getIdNumber());
			}else{
				//拒绝: 把RealAuth的state设为未审核, 并把Userinfo的realAuthId设置为空,
				realAuth.setState(RealAuth.STATE_REJECT);
				applierUserinfo.setRealAuthId(null);
			}
			//		更新实名认证对象,和Userinfo对象
			realAuthMapper.updateByPrimaryKey(realAuth);
			userinfoService.updateByPrimaryKey(applierUserinfo);
		}

	}
}
