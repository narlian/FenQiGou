package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.MailVerify;

public interface IMailVerifyService {
    void sendEmail(String mail);

    MailVerify selectByUUID(String key);
}
