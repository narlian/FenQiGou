package com.xmg.wechat.base.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Helen MM on 2017/9/10.
 */
public class DateUtils {

    public static Date getStartDate(Date date){
        if(date == null){
            return date;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        return calendar.getTime();
    }

    //处理日期的时分秒问题: 页面传的时间 时分秒都是0
    public static Date getEndDate(Date endDate){
        if(endDate == null){
            return endDate;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        return calendar.getTime();
    }

    public static long getBetweenTime(Date d1,Date d2){
        return Math.abs((d1.getTime()-d2.getTime())/1000);
    }


}
