package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.SystemDictionary;
import com.xmg.wechat.base.query.QueryObject;

import java.util.List;

public interface SystemDictionaryMapper {
    int insert(SystemDictionary record);
    SystemDictionary selectByPrimaryKey(Long id);
    int updateByPrimaryKey(SystemDictionary record);
	Long queryPageCount(QueryObject qo);
	List<SystemDictionary> queryPageData(QueryObject qo);

    List<SystemDictionary> selectAll();
}