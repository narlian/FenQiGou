package com.xmg.wechat.base.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter@Getter
public class Message extends BaseDomain{
    private Long id;
    private Logininfo logininfo;//用户
    private String receiveContent;//接收内容
    private String replyContent;//回复内容
    private Date inputTime;
}
