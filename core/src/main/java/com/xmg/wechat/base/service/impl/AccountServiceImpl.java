package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.mapper.AccountMapper;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements IAccountService {
	@Autowired
	private AccountMapper accountMapper;

	@Override
	public int save(Account account) {
		return accountMapper.insert(account);
	}

	@Override
	public int updateByPrimaryKey(Account account) {
		int count = accountMapper.updateByPrimaryKey(account);
		if(count <= 0){
			throw  new RuntimeException("乐观锁异常,AccountID: " + account.getId());
		}
		return count;
	}

	@Override
	public Account selectByPrimaryKey(Long id) {
		return accountMapper.selectByPrimaryKey(id);
	}

	@Override
	public Account getCurrent() {
		return accountMapper.selectByPrimaryKey(UserContext.getCurrent().getId());
	}

	@Override
	public Account get(Long id) {
		return accountMapper.selectByPrimaryKey(id);
	}
}
