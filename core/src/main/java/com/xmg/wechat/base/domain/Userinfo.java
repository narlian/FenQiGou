package com.xmg.wechat.base.domain;

import com.xmg.wechat.base.util.BitStatesUtils;
import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class Userinfo extends BaseDomain {
    private int version;//版本号，用作乐观锁
    private long bitState = 0;//用户状态值
    private String realName;//用户实名值（冗余数据）
    private String idNumber;//用户身份证号（冗余数据）
    private String phoneNumber;//用户电话
    private String email;//电子邮箱
    private int score; //材料认证分数
    private Long realAuthId;//实名认证id
    private SystemDictionaryItem incomeGrade;//收入
    private SystemDictionaryItem marriage;//婚姻情况
    private SystemDictionaryItem kidCount;//子女情况
    private SystemDictionaryItem educationBackground;//学历
    private SystemDictionaryItem houseCondition;//住房条件
    //判断是否已绑定手机
    public boolean getIsBindPhone(){
        return BitStatesUtils.hasState(this.bitState,BitStatesUtils.OP_BIND_PHONE);
    }
    //判断是否已绑定邮箱
    public boolean getIsBasicInfo(){
        return BitStatesUtils.hasState(this.bitState,BitStatesUtils.OP_BASIC_INFO);
    }
    //判断是否已填写基本信息
    public boolean getIsBindEmail(){
        return BitStatesUtils.hasState(this.bitState,BitStatesUtils.OP_BIND_EMAIL);
    }
    //判断是否已身份认证
    public boolean getIsRealAuth(){
        return BitStatesUtils.hasState(this.bitState,BitStatesUtils.OP_REAL_AUTH);
    }
    //判断是否已视频认证
    public boolean getIsVedioAuth(){
        return BitStatesUtils.hasState(this.bitState,BitStatesUtils.OP_VIDEO_AUTH);
    }
    //判断是否已绑定银行卡
    public boolean getIsBoundBankCard(){
        return BitStatesUtils.hasState(this.bitState,BitStatesUtils.HAS_BOUND_BANKCARD);
    }
    //判断是否有正在审批的贷款
    public boolean hasBidRequestProcess(){
        return BitStatesUtils.hasState(this.bitState,BitStatesUtils.HAS_BIDREQUEST_PROCESS);
    }
    public void addState(Long state) {
        this.bitState = BitStatesUtils.addState(this.bitState,state);
    }

    public void removeState(long state) {
        this.bitState = BitStatesUtils.removeState(this.bitState,state);
    }
}
