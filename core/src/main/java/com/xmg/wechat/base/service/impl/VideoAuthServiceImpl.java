package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.domain.VideoAuth;
import com.xmg.wechat.base.mapper.VideoAuthMapper;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.VideoAuthQueryObject;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.service.IVideoAuthService;
import com.xmg.wechat.base.util.BitStatesUtils;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class VideoAuthServiceImpl implements IVideoAuthService {
	@Autowired
	private VideoAuthMapper videoAuthMapper;
	@Autowired
	private IUserinfoService userinfoService;

	@Override
	public PageResult queryPage(VideoAuthQueryObject qo) {
		int count = videoAuthMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<VideoAuth> items = videoAuthMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void audit(Long loginInfoValue, int state, String remark) {
		//获取需要视频认证的用户,判断是否已经认证
		Userinfo applyUserinfo = userinfoService.selectByPrimaryKey(loginInfoValue);
		if(applyUserinfo != null && !applyUserinfo.getIsVedioAuth()){
			//创建videoAuth对象, 设置参数, 申请人
			VideoAuth videoAuth = new VideoAuth();
			Logininfo applier = new Logininfo();
			applier.setId(loginInfoValue);
			videoAuth.setApplier(applier);//设置申请人
			videoAuth.setApplyTime(new Date());
			videoAuth.setAuditor(UserContext.getCurrent());
			videoAuth.setAuditTime(new Date());
			videoAuth.setRemark(remark);
			//判断是审核通过还是拒绝
			if(state== VideoAuth.STATE_PASS){
				//通过: 设置认证状态, 给用户信息添加视频认证位状态
				videoAuth.setState(VideoAuth.STATE_PASS);
				applyUserinfo.addState(BitStatesUtils.OP_VIDEO_AUTH);
				userinfoService.updateByPrimaryKey(applyUserinfo);
			}else {
				//拒绝: 设置认证状态
				videoAuth.setState(VideoAuth.STATE_REJECT);
			}
			//把videoAuth对象保存入库
			this.save(videoAuth);
		}
	}

	@Override
	public void save(VideoAuth videoAuth) {
		videoAuthMapper.insert(videoAuth);
	}
}
