package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.BoundBankCard;

public interface IBoundBankCardService {
    int save(BoundBankCard boundBankCard);

    int update(BoundBankCard boundBankCard);

    /**
     * 根据用户userinfoId查询绑定的银行卡
     * @param userinfoId
     * @return
     */
    BoundBankCard selectByUserinfoId(Long userinfoId);
}
