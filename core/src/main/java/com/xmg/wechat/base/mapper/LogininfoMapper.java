package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LogininfoMapper {


    int insert(Logininfo record);

    Logininfo selectByPrimaryKey(Long id);

    Long queryPageCount(QueryObject qo);

    List<Logininfo> queryPageData(QueryObject qo);

    int updateByPrimaryKey(Logininfo record);


    int getLoginByName(String username);

    Logininfo checkLogin(@Param("username") String username, @Param("password") String password);

    int selectCountByType(int userType);

    List<Logininfo> autoComplete(String keyword);

    Logininfo selectByUsername(String admin);

    void updateState(@Param("userName") String userName, @Param("state") int state);

    Logininfo selectByName(String userName);
}