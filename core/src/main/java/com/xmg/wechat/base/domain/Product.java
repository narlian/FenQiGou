package com.xmg.wechat.base.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/11/7.
 */
@Getter
@Setter
public class Product extends BaseDomain {
    private String name;//名称
    private String sn;//编号
    private BigDecimal costPrice;//成本价格
    private BigDecimal salePrice;//销售价格
    private String imagePath;//图片路径
    private String intro;//商品描述
    private String brandName;//品牌名称

    public String getJsonStr() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        map.put("name", name);
        map.put("sn", sn);
        map.put("costPrice", costPrice);
        map.put("salePrice", salePrice);
        map.put("imagePath", imagePath);
        map.put("intro", intro);
        map.put("brandName", brandName);
        return JSON.toJSONString(map);
    }
}
