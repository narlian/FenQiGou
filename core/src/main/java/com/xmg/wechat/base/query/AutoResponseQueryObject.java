package com.xmg.wechat.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Setter@Getter
public class AutoResponseQueryObject extends QueryObject {
    private String keywords;

    public String getKeyword(){
        return StringUtils.isEmpty(keywords)?null:keywords;
    }

}
