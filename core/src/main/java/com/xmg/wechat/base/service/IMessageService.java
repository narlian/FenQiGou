package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.Message;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.MessageQueryObject;

public interface IMessageService {
    int save(Message message);

    PageResult queryPage(MessageQueryObject qo);
}
