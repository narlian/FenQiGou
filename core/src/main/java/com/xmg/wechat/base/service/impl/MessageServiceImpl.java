package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.Message;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.MessageQueryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.base.mapper.MessageMapper;
import com.xmg.wechat.base.service.IMessageService;

import java.util.List;

@Service
public class MessageServiceImpl implements IMessageService {
	@Autowired
	private MessageMapper messageMapper;

	@Override
	public int save(Message message) {
		return messageMapper.insert(message);
	}

	@Override
	public PageResult queryPage(MessageQueryObject qo) {
		Long count = messageMapper.queryPageCount(qo);
		if (count <= 0){
		    return PageResult.empty(qo.getPageSize());
		}
		List data = messageMapper.queryPageData(qo);
		return new PageResult(data,count.intValue(),qo.getCurrentPage(),qo.getPageSize());
	}
}
