package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.SystemDictionaryItem;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;

import java.util.List;

public interface ISystemDictionaryItemService {
    void  save(SystemDictionaryItem systemDictionaryItem);
    int  update(SystemDictionaryItem systemDictionaryItem);
    PageResult queryPage(QueryObject qo);
    void saveOrUpdate(SystemDictionaryItem systemDictionaryItem);

    List<SystemDictionaryItem> queryListBySn(String sn);
}
