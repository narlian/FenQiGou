package com.xmg.wechat.base.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Getter@Setter
public class RealAuth extends BaseAuthDomain {
    public  static  final  int GENDER_MALE = 0;
    public  static  final  int GENDER_FEMALE = 1;

    private String realName;
    private int gender;
    private String idNumber;
    private String bornDate;
    private String address;
    private String image1;//身份证正面
    private String image2;//身份证反面


    public String getSexDisplay(){
        return gender ==GENDER_MALE?"男":"女";
    }


    public String getJsonString(){
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("id",this.id);
        param.put("username",this.applier.getUsername());
        param.put("realname",this.realName);
        param.put("idNumber",this.idNumber);
        param.put("sex",this.getSexDisplay());
        param.put("birthDate",this.bornDate);
        param.put("address",this.address);
        param.put("image1",this.image1);
        param.put("image2",this.image2);
        return JSON.toJSONString(param);
    }

    /**
     * 获取用户真实名字的隐藏字符串，只显示姓氏
     *
     * @param realName
     *            真实名字
     * @return
     */
    public String getAnonymousRealName() {
        if (StringUtils.hasLength(this.realName)) {
            int len = realName.length();
            String replace = "";
            replace += realName.charAt(0);
            for (int i = 1; i < len; i++) {
                replace += "*";
            }
            return replace;
        }
        return realName;
    }

    /**
     * 获取用户身份号码的隐藏字符串
     *
     * @param idNumber
     * @return
     */
    public String getAnonymousIdNumber() {
        if (StringUtils.hasLength(idNumber)) {
            int len = idNumber.length();
            String replace = "";
            for (int i = 0; i < len; i++) {
                if ((i > 5 && i < 10) || (i > len - 5)) {
                    replace += "*";
                } else {
                    replace += idNumber.charAt(i);
                }
            }
            return replace;
        }
        return idNumber;
    }

    /**
     * 获取用户住址的隐藏字符串
     *
     * @param currentAddress
     *            用户住址
     * @return
     */
    public String getAnonymousAddress() {
        if (StringUtils.hasLength(address) && address.length() > 4) {
            String last = address.substring(address.length() - 4,
                    address.length());
            String stars = "";
            for (int i = 0; i < address.length() - 4; i++) {
                stars += "*";
            }
            return stars + last;
        }
        return address;
    }

}
