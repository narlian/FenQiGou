package com.xmg.wechat.base.util;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.vo.VerifyCodeVo;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Helen MM on 2017/9/5.
 */
public class UserContext {
    public static final String USER_IN_SESSION="logininfo";
    public static final String VERIFYCODE_IN_SESSION="verifyCode";
    //获取当前请求对象
    public static HttpServletRequest getRequest(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return request;
    }
    //把用户信息保存到session中
    public  static void setCurrent( Logininfo logininfo){
        getRequest().getSession().setAttribute(USER_IN_SESSION,logininfo);
    }
    //获取当前登录的用户
    public  static Logininfo getCurrent(){
         return (Logininfo) getRequest().getSession().getAttribute(USER_IN_SESSION);
    }

    public static String getIp() {
        String ip = getRequest().getRemoteAddr();
        return ip;
    }

    public static void setVerifyCode(VerifyCodeVo verifyCode) {
        getRequest().getSession().setAttribute(VERIFYCODE_IN_SESSION,verifyCode);
    }

    public  static VerifyCodeVo getVerifyCode(){
        return (VerifyCodeVo) getRequest().getSession().getAttribute(VERIFYCODE_IN_SESSION);
    }

    public static String getSessionId() {
        return getRequest().getSession().getId();
    }
}
