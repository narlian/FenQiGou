package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.LogininfoQueryObject;

import java.util.List;

public interface ILogininfoSercvice {

    void register(String username, String password);

    Boolean checkUsername(String username);

    Logininfo login(String username, String password);

    void initAdmin();

    List<Logininfo> autoComplete(String keyword);

    void updateState(String fromUserName,int state);

    Logininfo selectByName(String userName);

    PageResult queryPage(LogininfoQueryObject qo);
}
