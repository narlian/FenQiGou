package com.xmg.wechat.base.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class BoundBankCard extends BaseDomain{
    private Long userinfoId;            //用户id
    private String realname;            //真实姓名
    private String idNumber;            //身份证
    private String bankName;            //银行名称
    private String branchName;          //支行名称
    private String bankCardNumber;      //银行卡号
    private String phoneNumber;         //电话号码
}
