package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.AutoResponse;
import com.xmg.wechat.base.query.QueryObject;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AutoResponseMapper {
    int insert(AutoResponse record);
    int delete(Long id);
    int updateByPrimaryKey(AutoResponse record);
	Long queryPageCount(QueryObject qo);
	List<AutoResponse> queryPageData(QueryObject qo);

    String selectByKeyword(String content);
}