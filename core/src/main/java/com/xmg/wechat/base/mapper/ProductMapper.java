package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.Product;
import com.xmg.wechat.base.query.QueryObject;

import java.util.List;

public interface ProductMapper {
    int insert(Product record);
    Product selectByPrimaryKey(Long id);
    int updateByPrimaryKey(Product record);
	Long queryPageCount(QueryObject qo);
	List<Product> queryPageData(QueryObject qo);

    int delete(Long id);

    List<Product> selectAll();

    Product selectByImagePath(String imagePath);
}