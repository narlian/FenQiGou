package com.xmg.wechat.base.service;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.business.domain.UserBankinfo;

public interface IUserinfoService {
    int save(Userinfo userinfo);
    int updateByPrimaryKey(Userinfo userinfo);
    Userinfo selectByPrimaryKey(Long id);

    void bindPhone(String phoneNumber, String verifyCode);

    //获取当前当前登录用户的userinfo
    Userinfo getCurrent();

    void bindEmail(String key);

    void basicInfoSave(Userinfo userinfo);

    int selectByRealName(String admin);

    void save(UserBankinfo userBankinfo);
}
