package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.Product;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.ProductQueryObject;

import java.math.BigDecimal;
import java.util.List;

public interface IProductService {
    int update(Product product);

    int save(Product product);

    PageResult queryPage(ProductQueryObject qo);

    void saveOrUpdate(Product product);

    void delete(Long id);

    List<Product> selectAll();

    Product get(Long id);

    BigDecimal getAverageAmount(Long productId, BigDecimal periods);

    Product selectByImagePath(String imagePath);

}
