package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.VideoAuth;
import com.xmg.wechat.base.query.QueryObject;
import java.util.List;

public interface VideoAuthMapper {
    int insert(VideoAuth record);
    VideoAuth selectByPrimaryKey(Long id);
    int updateByPrimaryKey(VideoAuth record);
	Long queryPageCount(QueryObject qo);
	List<VideoAuth> queryPageData(QueryObject qo);
}