package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.AutoResponse;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.AutoResponseQueryObject;

public interface IAutoResponseService {
    int save(AutoResponse autoResponse);
    int update(AutoResponse autoResponse);
    int delete(Long id);
    PageResult queryPage(AutoResponseQueryObject qo);

    void saveOrUpdate(AutoResponse autoResponse);

    String selectByKeyword(String content);
}
