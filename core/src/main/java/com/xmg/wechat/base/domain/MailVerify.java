package com.xmg.wechat.base.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter@Getter
public class MailVerify extends BaseDomain{
    private long userinfoId;
    private String email;
    private String uuid;
    private Date sendTime;
}
