package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.SystemDictionaryItem;
import com.xmg.wechat.base.query.QueryObject;

import java.util.List;

public interface SystemDictionaryItemMapper {
    int insert(SystemDictionaryItem record);
    SystemDictionaryItem selectByPrimaryKey(Long id);
    int updateByPrimaryKey(SystemDictionaryItem record);
	Long queryPageCount(QueryObject qo);
	List<SystemDictionaryItem> queryPageData(QueryObject qo);

    List<SystemDictionaryItem> queryListBySn(String sn);
}