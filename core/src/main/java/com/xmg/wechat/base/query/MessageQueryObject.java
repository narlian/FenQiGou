package com.xmg.wechat.base.query;

import com.xmg.wechat.base.util.DateUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;

import java.util.Date;

@Setter@Getter
public class MessageQueryObject extends QueryObject {
    private String keyword;
    private Date beginDate;
    private Date endDate;

    public String getKeyword(){
        return StringUtils.isEmpty(keyword)?null:keyword;
    }
    public Date getBeginDate() {
        return DateUtils.getStartDate(beginDate);
    }

    public Date getEndDate() {
        return DateUtils.getEndDate(endDate);
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
