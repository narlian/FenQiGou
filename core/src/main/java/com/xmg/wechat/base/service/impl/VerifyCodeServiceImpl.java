package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.service.IVerifyCodeService;
import com.xmg.wechat.base.util.DateUtils;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.base.vo.VerifyCodeVo;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class VerifyCodeServiceImpl implements IVerifyCodeService {
    @Override
    public void sendVerifyCode(String phoneNumber) {
        //校验之前是否有发送过短信90秒内
        VerifyCodeVo vo = UserContext.getVerifyCode();
        if(vo == null || DateUtils.getBetweenTime(vo.getSendTime(),new Date())>BidConst.MESSAGE_INTERVAL_TIME){

            String verifyCode = UUID.randomUUID().toString().substring(0,4);
            StringBuilder message = new StringBuilder(100);
            message.append("您的验证码为:").append(verifyCode)
                    .append(",有效期为:").append(BidConst.MESSAGE_VALID_TIME).append("分钟,请尽快使用");
            //发送短信
            System.out.println(message);
            vo = new VerifyCodeVo();
            vo.setPhoneNumber(phoneNumber);
            vo.setVerifyCode(verifyCode);
            vo.setSendTime(new Date());
            UserContext.setVerifyCode(vo);
        }else {
            throw new RuntimeException("验证码发送太频繁,请稍后再发");
        }
    }

    @Override
    public boolean validate(String phoneNumber, String verifyCode) {
        VerifyCodeVo vo = UserContext.getVerifyCode();
        if(vo != null && vo.getPhoneNumber().equals(phoneNumber)
                && vo.getVerifyCode().equals(verifyCode)
                && DateUtils.getBetweenTime(vo.getSendTime(),new Date()) <= BidConst.MESSAGE_VALID_TIME *60){
            return true;
        }
        return false;
    }
}
