package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.RealAuth;
import com.xmg.wechat.base.query.QueryObject;
import java.util.List;

public interface RealAuthMapper {
    int insert(RealAuth record);
    RealAuth selectByPrimaryKey(Long id);
    int updateByPrimaryKey(RealAuth record);
	Long queryPageCount(QueryObject qo);
	List<RealAuth> queryPageData(QueryObject qo);
}