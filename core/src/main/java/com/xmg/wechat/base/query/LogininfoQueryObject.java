package com.xmg.wechat.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;


public class LogininfoQueryObject extends QueryObject {
    private int userType;
    private String keyword;

    public String getKeyword() {
        String string = StringUtils.hasLength(keyword)?keyword:null;
        return string;

    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
