package com.xmg.wechat.base.query;

import com.xmg.wechat.base.util.DateUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter@Getter
public class BaseAuthQueryObject extends QueryObject {
    private int state = -1;
    private Date beginDate;
    private Date endDate;
    public Date getBeginDate() {
        return DateUtils.getStartDate(beginDate);
    }

    public Date getEndDate() {
        return DateUtils.getEndDate(endDate);
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
