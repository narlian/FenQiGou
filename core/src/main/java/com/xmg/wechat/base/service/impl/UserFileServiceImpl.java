package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.SystemDictionaryItem;
import com.xmg.wechat.base.domain.UserFile;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.mapper.UserFileMapper;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.UserFileQueryObject;
import com.xmg.wechat.base.service.IUserFileService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserFileServiceImpl implements IUserFileService {
	@Autowired
	private UserFileMapper userFileMapper;
	@Autowired
	private IUserinfoService userinfoService;

	@Override
	public void save(UserFile userFile) {
		userFileMapper.insert(userFile);
	}

	@Override
	public int update(UserFile userFile) {
		return userFileMapper.updateByPrimaryKey(userFile);
	}

	@Override
	public void apply(String imagePath) {
		UserFile userFile = new UserFile();
		userFile.setImage(imagePath);
		userFile.setApplier(UserContext.getCurrent());
		userFile.setApplyTime(new Date());
		userFile.setState(UserFile.STATE_NORMAL);
		this.save(userFile);
	}

	@Override
	public void choiseType(Long[] ids, Long[] fileTypes) {
		//判断ids fileTypes 都不为空且长度相等
		if(ids != null && fileTypes != null && ids.length == fileTypes.length){
			UserFile userFile = null;
			for (int i = 0; i <ids.length;i++) {
				userFile = userFileMapper.selectByPrimaryKey(ids[i]);
				SystemDictionaryItem fileType = null;
				//判断是当前登录用户和 风控材料申请人是同一个: 只能修改自己的风控材料
				if(userFile.getApplier().getId().equals(userinfoService.getCurrent().getId())){
					fileType = new SystemDictionaryItem();
					fileType.setId(fileTypes[i]);
					userFile.setFileType(fileType);
					userFileMapper.updateByPrimaryKey(userFile);
				}
			}
		}
	}

	@Override
	public List<UserFile> selectedFileTypeList(boolean selectedFile) {
		return userFileMapper.selectedFileTypeList(UserContext.getCurrent().getId(),selectedFile);
	}

	@Override
	public PageResult queryPage(UserFileQueryObject qo) {
		int count = userFileMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<UserFile> items = userFileMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void audit(Long id, int score, int state, String remark) {
//		查询出UserFile对象,  判断申请用户当前风控材料没有认证,
		UserFile userFile = userFileMapper.selectByPrimaryKey(id);
		if(userFile != null && userFile.getState() == UserFile.STATE_NORMAL){
//				设置审核人, 时间, 意见等
			userFile.setAuditor(UserContext.getCurrent());
			userFile.setApplyTime(new Date());
			userFile.setRemark(remark);
//		判断申请通过还是拒绝
			if(state == UserFile.STATE_PASS){
//		通过: 往userinfo添加score, userfile设置状态
				userFile.setScore(score);
				userFile.setState(UserFile.STATE_PASS);
				Userinfo applierUserinfo = userinfoService.selectByPrimaryKey(userFile.getApplier().getId());
				applierUserinfo.setScore(applierUserinfo.getScore() + score);
				userinfoService.updateByPrimaryKey(applierUserinfo);
			}else {
//		拒绝: userfile设置状态
				userFile.setState(UserFile.STATE_REJECT);
			}
//		保存/更新
			userFileMapper.updateByPrimaryKey(userFile);
		}

	}

	@Override
	public List<UserFile> queryAuditListByLogininfo(Long logininfoid) {
		return userFileMapper.queryAuditListByLogininfo(logininfoid,UserFile.STATE_PASS);
	}
}
