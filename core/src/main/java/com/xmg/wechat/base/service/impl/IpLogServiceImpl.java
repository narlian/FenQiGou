package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.IpLog;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.mapper.IpLogMapper;
import com.xmg.wechat.base.service.IIpLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IpLogServiceImpl implements IIpLogService {
	@Autowired
	private IpLogMapper ipLogMapper;

	@Override
	public int save(IpLog ipLog) {
		return ipLogMapper.insert(ipLog);
	}

	@Override
	public PageResult queryPage(QueryObject qo) {
		int count = ipLogMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<IpLog> ipLogList = ipLogMapper.queryPageData(qo);
		return new PageResult(ipLogList,count,qo.getCurrentPage(),qo.getPageSize());
	}
}
