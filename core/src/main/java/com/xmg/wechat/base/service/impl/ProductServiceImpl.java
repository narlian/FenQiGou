package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.Product;
import com.xmg.wechat.base.mapper.ProductMapper;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.ProductQueryObject;
import com.xmg.wechat.base.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public int update(Product product) {
        return productMapper.updateByPrimaryKey(product);
    }

    @Override
    public int save(Product product) {
        return productMapper.insert(product);
    }

    @Override
    public PageResult queryPage(ProductQueryObject qo) {
        Long count = productMapper.queryPageCount(qo);
        if (count <= 0) {
            return PageResult.empty(qo.getPageSize());
        }
        List result = productMapper.queryPageData(qo);
        return new PageResult(result, count.intValue(), qo.getCurrentPage(), qo.getPageSize());
    }

    @Override
    public void saveOrUpdate(Product product) {
        if (product.getId() != null) {
            this.update(product);
        } else {
            this.save(product);
        }
    }

    @Override
    public void delete(Long id) {
        this.productMapper.delete(id);
    }

    @Override
    public List<Product> selectAll() {
        return this.productMapper.selectAll();
    }

    @Override
    public Product get(Long id) {
        return this.productMapper.selectByPrimaryKey(id);
    }

    @Override
    public BigDecimal getAverageAmount(Long productId, BigDecimal periods) {
        Product product = this.get(productId);
        BigDecimal avgPrice = product.getSalePrice().divide(periods,2, RoundingMode.HALF_UP);
        return avgPrice;
    }

    @Override
    public Product selectByImagePath(String imagePath) {
        return productMapper.selectByImagePath(imagePath);
    }
}
