package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.SystemDictionary;
import com.xmg.wechat.base.mapper.SystemDictionaryMapper;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.service.ISystemDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemDictionaryServiceImpl implements ISystemDictionaryService {
	@Autowired
	private SystemDictionaryMapper systemDictionaryMapper;

	@Override
	public void save(SystemDictionary systemDictionary) {
		systemDictionaryMapper.insert(systemDictionary);
	}

	@Override
	public int update(SystemDictionary systemDictionary) {
		return systemDictionaryMapper.updateByPrimaryKey(systemDictionary);
	}

	@Override
	public PageResult queryPage(QueryObject qo) {
		int count = systemDictionaryMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<SystemDictionary> systemDictionaries = systemDictionaryMapper.queryPageData(qo);
		return new PageResult(systemDictionaries,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void saveOrUpdate(SystemDictionary systemDictionary) {
		if (systemDictionary.getId() == null){
			this.save(systemDictionary);
		}else {
			this.update(systemDictionary);
		}
	}

	@Override
	public List<SystemDictionary> selectAll() {
		return systemDictionaryMapper.selectAll();
	}
}
