package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.*;
import com.xmg.wechat.base.mapper.LogininfoMapper;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.LogininfoQueryObject;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.service.IIpLogService;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.MD5;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.ExpAccount;
import com.xmg.wechat.business.service.IExpAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class LogininfoServiceImpl implements ILogininfoSercvice {
    @Autowired
    private LogininfoMapper logininfoMapper;
    @Autowired
    private IUserinfoService userinfoService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IIpLogService ipLogService;
    @Autowired
    private IExpAccountService expAccountService;

    @Override
    public void register(String username, String password) {
        int count = logininfoMapper.getLoginByName(username);
        if(count > 0){
            this.updateState(username,Logininfo.STATE_NORMAL);
        }else{
            Logininfo logininfo = new Logininfo();
            logininfo.setUsername(username);
            logininfo.setPassword(MD5.encode(password));
            logininfo.setState(Logininfo.STATE_NORMAL);
            logininfo.setUserType(Logininfo.USERTYPE_WECHAT);
            logininfoMapper.insert(logininfo);

            Userinfo userinfo = new Userinfo();
            userinfo.setId(logininfo.getId());
            userinfoService.save(userinfo);
            //创建用户账户
            Account account = new Account();
            account.setId(logininfo.getId());
            accountService.save(account);
            //创建体验金账户
            ExpAccount expAccount = new ExpAccount();
            expAccount.setId(logininfo.getId());
            expAccountService.save(expAccount);
            //发体验金
            expAccountService.grantExpMoney(expAccount.getId()
                    ,new IExpAccountService.LastTime(1, IExpAccountService.LastTimeUnit.MONTH),
                            BidConst.REGISTER_GRANT_MONEY,
                            BidConst.EXPMONEY_TYPE_REGISTER);
        }
    }

    @Override
    public Boolean checkUsername(String username) {
        return logininfoMapper.getLoginByName(username)<=0;
    }

    @Override
    public Logininfo login(String username, String password) {
        Logininfo logininfo = new Logininfo();
        logininfo = logininfoMapper.checkLogin(username,MD5.encode(password));
        IpLog ipLog = new IpLog();
        ipLog.setUsername(username);
        ipLog.setLoginTime(new Date());
        ipLog.setIp(UserContext.getIp());
        if(logininfo == null){
            ipLog.setState(IpLog.LOGIN_FAILED);
            //throw new RuntimeException("账号密码不匹配");此处抛异常会导致事务回滚无法打印登录日志, 移到controller中
        }else{
            ipLog.setUserType(logininfo.getUserType());
            //把登录成功的用户放到session中
            UserContext.setCurrent(logininfo);
            ipLog.setState(IpLog.LOGIN_SUCCESS);
        }
        ipLogService.save(ipLog);
        return logininfo;
    }

    @Override
    public void initAdmin() {
        //后台查询是否已经有管理员
        int i = logininfoMapper.selectCountByType(Logininfo.USERTYPE_MANAGER);
        if(i <= 0){
            //如果没有创建一个管理员
            Logininfo logininfo = new Logininfo();
            logininfo.setUsername(BidConst.DEFAULT_ADMIN_ACCOUNT);
            logininfo.setPassword(MD5.encode(BidConst.DEFAULT_ADMIN_PASSWORD));
            logininfo.setState(Logininfo.STATE_NORMAL);
            logininfo.setUserType(Logininfo.USERTYPE_MANAGER);
            logininfoMapper.insert(logininfo);
        }
        int j = userinfoService.selectByRealName("admin");
        if(j <= 0){
            Logininfo logininfo = logininfoMapper.selectByUsername("admin");
            Userinfo userinfo = new Userinfo();
            userinfo.setRealName("admin");
            userinfo.setId(logininfo.getId());
            userinfoService.save(userinfo);
        }
    }

    @Override
    public List<Logininfo> autoComplete(String keyword) {
        return logininfoMapper.autoComplete(keyword);
    }

    @Override
    public void updateState(String fromUserName,int state) {
        logininfoMapper.updateState(fromUserName,state);
    }

    @Override
    public PageResult queryPage(LogininfoQueryObject qo) {
        Long count = logininfoMapper.queryPageCount(qo);
        if (count <= 0) {
            return PageResult.empty(qo.getPageSize());
        }
        List result = logininfoMapper.queryPageData(qo);
        return new PageResult(result,count.intValue(),qo.getCurrentPage(),qo.getPageSize());
    }

    @Override
    public Logininfo selectByName(String userName) {
        return logininfoMapper.selectByName(userName);
    }
}
