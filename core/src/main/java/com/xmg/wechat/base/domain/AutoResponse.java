package com.xmg.wechat.base.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Setter@Getter
public class AutoResponse extends BaseDomain {

    private String keyword;
    private String replyContent;

    //回显编辑页面的数据
    public String getJsonStr(){
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("id",id);
        param.put("keyword",keyword);
        param.put("replyContent",replyContent);
        return JSON.toJSONString(param);
    }
}
