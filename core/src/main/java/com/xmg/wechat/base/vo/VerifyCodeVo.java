package com.xmg.wechat.base.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter@Getter
public class VerifyCodeVo implements Serializable {
    private String phoneNumber;
    private String verifyCode;
    private Date sendTime;
}
