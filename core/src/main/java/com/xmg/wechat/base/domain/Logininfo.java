package com.xmg.wechat.base.domain;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter@Setter
public class Logininfo extends BaseDomain {
    public static final int STATE_NORMAL = 0;//已订阅状态
    public static final int STATE_LOCK = 1;//取消订阅状态
    public static final int USERTYPE_USER = 0;//普通用户
    public static final int USERTYPE_MANAGER = 1;//管理员
    public static final int USERTYPE_WECHAT = 2;//微信用户
    private String username;
    private String password;
    private int state;
    private int userType;

    public String getJsonString() {
        Map<String,Object> param = new HashMap<String, Object>();

        return JSON.toJSONString(param);
    }
}
