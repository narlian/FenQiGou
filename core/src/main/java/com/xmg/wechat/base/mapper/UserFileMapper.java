package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.UserFile;
import com.xmg.wechat.base.query.QueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserFileMapper {
    int insert(UserFile record);
    UserFile selectByPrimaryKey(Long id);
    int updateByPrimaryKey(UserFile record);
	Long queryPageCount(QueryObject qo);
	List<UserFile> queryPageData(QueryObject qo);

    List<UserFile> selectedFileTypeList(@Param("currentUserId") Long currentUserId, @Param("selectedFile") boolean selectedFile);

    List<UserFile> queryAuditListByLogininfo(@Param("logininfoId") Long logininfoId,@Param("state") int state);
}