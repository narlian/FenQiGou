package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.Message;
import com.xmg.wechat.base.query.QueryObject;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MessageMapper {
    int insert(Message record);
    Message selectByPrimaryKey(Long id);
	Long queryPageCount(QueryObject qo);
	List<Message> queryPageData(QueryObject qo);
}