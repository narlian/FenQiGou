package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.Account;

public interface AccountMapper {
    int insert(Account record);
    Account selectByPrimaryKey(Long id);
    int updateByPrimaryKey(Account record);

}