package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.IpLog;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;

public interface IIpLogService {
    int save(IpLog ipLog);
    PageResult queryPage(QueryObject qo);
}
