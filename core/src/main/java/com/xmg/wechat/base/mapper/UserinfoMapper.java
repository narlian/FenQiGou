package com.xmg.wechat.base.mapper;


import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.query.QueryObject;

import java.util.List;

public interface UserinfoMapper {
    int insert(Userinfo record);
    Userinfo selectByPrimaryKey(Long id);
    int updateByPrimaryKey(Userinfo record);
    int selectByRealName(String admin);
}