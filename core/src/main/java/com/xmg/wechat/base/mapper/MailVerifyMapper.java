package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.MailVerify;

public interface MailVerifyMapper {
    int insert(MailVerify record);
    MailVerify selectByPrimaryKey(Long id);

    MailVerify selectByUUID(String key);
}