package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.BoundBankCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.base.mapper.BoundBankCardMapper;
import com.xmg.wechat.base.service.IBoundBankCardService;

@Service
public class BoundBankCardServiceImpl implements IBoundBankCardService {
	@Autowired
	private BoundBankCardMapper boundBankCardMapper;

	@Override
	public int save(BoundBankCard boundBankCard) {
		return boundBankCardMapper.insert(boundBankCard);
	}

	@Override
	public int update(BoundBankCard boundBankCard) {
		return boundBankCardMapper.updateByPrimaryKey(boundBankCard);
	}

	@Override
	public BoundBankCard selectByUserinfoId(Long userinfoId) {
		return boundBankCardMapper.selectByUserinfoId(userinfoId);
	}
}
