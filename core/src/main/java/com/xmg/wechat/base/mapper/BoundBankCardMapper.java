package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.BoundBankCard;
import com.xmg.wechat.base.query.QueryObject;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BoundBankCardMapper {
    int insert(BoundBankCard record);
    BoundBankCard selectByPrimaryKey(Long id);
    int updateByPrimaryKey(BoundBankCard record);
	Long queryPageCount(QueryObject qo);
	List<BoundBankCard> queryPageData(QueryObject qo);

    BoundBankCard selectByUserinfoId(Long userinfoId);
}