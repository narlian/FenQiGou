package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.Account;

public interface IAccountService {
    int save(Account account);
    int updateByPrimaryKey(Account account);
    Account selectByPrimaryKey(Long id);

    Account getCurrent();

    Account get(Long bidUserId);
}
