package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.RealAuth;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.RealAuthQueryObject;

public interface IRealAuthService {
    RealAuth get(long realAuthId);

    void apply(RealAuth realAuth);

    PageResult queryPage(RealAuthQueryObject qo);

    //实名认证审核
    void audit(Long id, int state, String remark);
}
