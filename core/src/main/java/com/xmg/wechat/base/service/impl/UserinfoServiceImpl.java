package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.domain.MailVerify;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.mapper.UserinfoMapper;
import com.xmg.wechat.base.service.IMailVerifyService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.service.IVerifyCodeService;
import com.xmg.wechat.base.util.BitStatesUtils;
import com.xmg.wechat.base.util.DateUtils;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.UserBankinfo;
import com.xmg.wechat.business.service.IUserBankinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserinfoServiceImpl implements IUserinfoService {
	@Autowired
	private UserinfoMapper userinfoMapper;
	@Autowired
	private IVerifyCodeService verifyCodeService;
	@Autowired
	private IMailVerifyService mailVerifyService;
	@Autowired
	private IUserBankinfoService userBankinfoService;
	@Override
	public int save(Userinfo userinfo) {
		return userinfoMapper.insert(userinfo);
	}

	@Override
	public int updateByPrimaryKey(Userinfo userinfo) {
		int count = userinfoMapper.updateByPrimaryKey(userinfo);
		if(count <= 0){
			throw  new RuntimeException("乐观锁异常: UserinfoID:"+userinfo.getId());
		}
		return count;
	}

	@Override
	public Userinfo selectByPrimaryKey(Long id) {
		return userinfoMapper.selectByPrimaryKey(id);
	}

	@Override
	public void bindPhone(String phoneNumber, String verifyCode) {
		Userinfo userinfo = this.selectByPrimaryKey(UserContext.getCurrent().getId());
		if(userinfo.getIsBindPhone()){
			throw new RuntimeException("您已经绑定手机号码, 请不要再重复绑定");
		}
		boolean valid = verifyCodeService.validate(phoneNumber,verifyCode);
		if(valid){
			userinfo.setPhoneNumber(phoneNumber);
			userinfo.addState(BitStatesUtils.OP_BIND_PHONE);
			this.updateByPrimaryKey(userinfo);
		}else {
			throw new RuntimeException("验证码验证失效");
		}
	}

	@Override
	public Userinfo getCurrent() {
		return this.selectByPrimaryKey(UserContext.getCurrent().getId());
	}

	@Override
	public void bindEmail(String key) {
		MailVerify verify = mailVerifyService.selectByUUID(key);
		if(verify == null ){
			throw  new RuntimeException("验证地址有误,请重新发送");
		}
		if(DateUtils.getBetweenTime(verify.getSendTime(),new Date()) > BidConst.EMIAL_VALID_DAY*24*60*60){
			throw  new RuntimeException("邮箱失效,请重新发送");
		}
		//把邮箱绑定到用户信息中
		Userinfo userinfo = this.selectByPrimaryKey(verify.getUserinfoId());
		if(userinfo.getIsBindEmail()){
			throw new RuntimeException("您已绑定邮箱,请不要重复绑定");
		}
		userinfo.setEmail(verify.getEmail());
		userinfo.addState(BitStatesUtils.OP_BIND_EMAIL);
		this.updateByPrimaryKey(userinfo);
	}

	@Override
	public void save(UserBankinfo userBankinfo) {
		Userinfo userinfo = this.selectByPrimaryKey(UserContext.getCurrent().getId());
		UserBankinfo ubi;
		if (!userinfo.getIsBoundBankCard()) {
			ubi = new UserBankinfo();
			ubi.setAccountName(userBankinfo.getAccountName());
			ubi.setAccountNumber(userBankinfo.getAccountNumber());
			ubi.setBankName(userBankinfo.getBankName());
			ubi.setForkName(userBankinfo.getForkName());
			ubi.setUserinfoId(userinfo.getId());
			userBankinfoService.save(ubi);
			userinfo.addState(BitStatesUtils.HAS_BOUND_BANKCARD);
			userinfoMapper.updateByPrimaryKey(userinfo);
		} else {
			throw new RuntimeException("您已绑定银行卡,请不要重复绑定");
		}
	}

	@Override
	public void basicInfoSave(Userinfo userinfo) {
		Userinfo current = this.getCurrent();
		current.setEducationBackground(userinfo.getEducationBackground());
		current.setHouseCondition(userinfo.getHouseCondition());
		current.setIncomeGrade(userinfo.getIncomeGrade());
		current.setKidCount(userinfo.getKidCount());
		current.setMarriage(userinfo.getMarriage());
		if(!current.getIsBasicInfo()){
			current.addState(BitStatesUtils.OP_BASIC_INFO);
		}
		this.updateByPrimaryKey(current);
	}

	@Override
	public int selectByRealName(String admin) {
		return userinfoMapper.selectByRealName(admin);
	}
}
