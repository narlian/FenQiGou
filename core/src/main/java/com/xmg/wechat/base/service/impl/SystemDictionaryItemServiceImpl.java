package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.SystemDictionaryItem;
import com.xmg.wechat.base.mapper.SystemDictionaryItemMapper;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.service.ISystemDictionaryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemDictionaryItemServiceImpl implements ISystemDictionaryItemService {
	@Autowired
	private SystemDictionaryItemMapper systemDictionaryItemMapper;

	@Override
	public void save(SystemDictionaryItem systemDictionaryItem) {
		systemDictionaryItemMapper.insert(systemDictionaryItem);
	}

	@Override
	public int update(SystemDictionaryItem systemDictionaryItem) {
		return systemDictionaryItemMapper.updateByPrimaryKey(systemDictionaryItem);
	}

	@Override
	public PageResult queryPage(QueryObject qo) {
		int count = systemDictionaryItemMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<SystemDictionaryItem> items = systemDictionaryItemMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void saveOrUpdate(SystemDictionaryItem systemDictionaryItem) {
		if(systemDictionaryItem.getId() == null){
			this.save(systemDictionaryItem);
		}else {
			this.update(systemDictionaryItem);
		}
	}

	@Override
	public List<SystemDictionaryItem> queryListBySn(String sn) {

		return systemDictionaryItemMapper.queryListBySn(sn);
	}
}
