package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.AutoResponse;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.AutoResponseQueryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.base.mapper.AutoResponseMapper;
import com.xmg.wechat.base.service.IAutoResponseService;

import java.util.List;

@Service
public class AutoResponseServiceImpl implements IAutoResponseService {
	@Autowired
	private AutoResponseMapper autoResponseMapper;

	@Override
	public int save(AutoResponse autoResponse) {
		return autoResponseMapper.insert(autoResponse);
	}

	@Override
	public int update(AutoResponse autoResponse) {
		return autoResponseMapper.updateByPrimaryKey(autoResponse);
	}

	@Override
	public int delete(Long id) {
		return autoResponseMapper.delete(id);
	}

	@Override
	public PageResult queryPage(AutoResponseQueryObject qo) {
		Long count = autoResponseMapper.queryPageCount(qo);
		if (count <= 0){
		    return PageResult.empty(qo.getPageSize());
		}
		List data = autoResponseMapper.queryPageData(qo);
		return new PageResult(data,count.intValue(),qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void saveOrUpdate(AutoResponse autoResponse) {
		if(autoResponse.getId() != null){
			this.update(autoResponse);
		} else {
			this.save(autoResponse);
		}
	}

	@Override
	public String selectByKeyword(String content) {
		return autoResponseMapper.selectByKeyword(content);
	}
}
