package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.UserFile;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.UserFileQueryObject;

import java.util.List;

public interface IUserFileService {
    void save(UserFile userFile);
    int update(UserFile userFile);

    void apply(String imagePath);

    void choiseType(Long[] ids, Long[] fileTypes);

    List<UserFile> selectedFileTypeList(boolean selectedFile);

    PageResult queryPage(UserFileQueryObject qo);

    void audit(Long id, int score, int state, String remark);

    List<UserFile> queryAuditListByLogininfo(Long logininfoid);
}
