package com.xmg.wechat.base.service.impl;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.domain.MailVerify;
import com.xmg.wechat.base.mapper.MailVerifyMapper;
import com.xmg.wechat.base.service.IMailVerifyService;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class MailVerifyServiceImpl implements IMailVerifyService {
	@Autowired
	private MailVerifyMapper mailVerifyMapper;
	//http://localhost:8080/bindEmail?key=
	@Value("${email.applicationBindEmailUrl}")
	private String applicationBindEmailUrl;

	@Override
	public void sendEmail(String mail) {
		//创建UUID
		String uuid = UUID.randomUUID().toString();
		//拼接邮件内容
		StringBuilder message = new StringBuilder(100);
		message = message.append("感谢注册P2P,请点击<a href='")
				.append(applicationBindEmailUrl).append(uuid)
				.append("'>这里</a>完成验证,邮件有效期为").append(BidConst.EMIAL_VALID_DAY)
				.append("天,请尽快完成验证 ");
		//邮件发送
		System.out.println(message);
		//把邮箱认证信息存到数据库中
		MailVerify mailVerify = new MailVerify();
		mailVerify.setEmail(mail);
		mailVerify.setSendTime(new Date());
		mailVerify.setUuid(uuid);
		mailVerify.setUserinfoId(UserContext.getCurrent().getId());
		mailVerifyMapper.insert(mailVerify);
	}
	//实现邮件发送
	private void realMail(String email,String content){
		//创建邮件主题

		//创建helper

		//配置收件人

		//设置邮件的标题

		//设置内容

		//发送邮件
	}

	@Override
	public MailVerify selectByUUID(String key) {
		return mailVerifyMapper.selectByUUID(key);
	}

}
