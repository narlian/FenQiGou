package com.xmg.wechat.base.service;
import com.xmg.wechat.base.domain.VideoAuth;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.VideoAuthQueryObject;

public interface IVideoAuthService {
    PageResult queryPage(VideoAuthQueryObject qo);

    void audit(Long loginInfoValue, int state, String remark);

    void save(VideoAuth videoAuth);

}
