package com.xmg.wechat.base.service;

import com.xmg.wechat.base.domain.SystemDictionary;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;

import java.util.List;

public interface ISystemDictionaryService {
    void  save(SystemDictionary systemDictionary);
    int  update(SystemDictionary systemDictionary);
    PageResult queryPage(QueryObject qo);

    void saveOrUpdate(SystemDictionary systemDictionary);

    List<SystemDictionary> selectAll();
}
