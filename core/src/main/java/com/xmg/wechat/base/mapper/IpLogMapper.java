package com.xmg.wechat.base.mapper;

import com.xmg.wechat.base.domain.IpLog;
import com.xmg.wechat.base.query.QueryObject;

import java.util.List;

public interface IpLogMapper {
    int insert(IpLog record);
	Long queryPageCount(QueryObject qo);
	List<IpLog> queryPageData(QueryObject qo);
}