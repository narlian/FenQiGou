package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.business.domain.WeChatMenuItem;
import com.xmg.wechat.business.query.WeChatMenuItemQueryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.business.mapper.WeChatMenuItemMapper;
import com.xmg.wechat.business.service.IWeChatMenuItemService;

import java.util.List;

@Service
public class WeChatMenuItemServiceImpl implements IWeChatMenuItemService {
	@Autowired
	private WeChatMenuItemMapper weChatMenuItemMapper;

	@Override
	public int save(WeChatMenuItem weChatMenuItem) {
		return weChatMenuItemMapper.insert(weChatMenuItem);
	}

	@Override
	public int update(WeChatMenuItem weChatMenuItem) {
		return weChatMenuItemMapper.updateByPrimaryKey(weChatMenuItem);
	}

	@Override
	public List<WeChatMenuItem> selectAll() {
		return weChatMenuItemMapper.selectAll();
	}

	@Override
	public List<WeChatMenuItem> selectAllId(Long id) {
		return weChatMenuItemMapper.selectAllId(id);
	}

	@Override
	public void saveOrUpdate(WeChatMenuItem weChatMenuItem) {
		int count = this.selectAll().size();
		if (weChatMenuItem.getId()==null) {
			if (count<5) {
				this.save(weChatMenuItem);
			}
		} else {
			this.update(weChatMenuItem);
		}
	}

	@Override
	public PageResult queryPage(WeChatMenuItemQueryObject qo) {
		Long count = weChatMenuItemMapper.queryPageCount(qo);
		if (count <= 0) {
		    return PageResult.empty(qo.getPageSize());
		}
		List result = weChatMenuItemMapper.queryPageData(qo);
		return new PageResult(result,count.intValue(),qo.getCurrentPage(),qo.getPageSize());
	}
}
