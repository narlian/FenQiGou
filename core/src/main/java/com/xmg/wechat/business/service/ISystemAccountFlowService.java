package com.xmg.wechat.business.service;

import com.xmg.wechat.business.domain.SystemAccount;
import com.xmg.wechat.business.domain.SystemAccountFlow;

import java.math.BigDecimal;

public interface ISystemAccountFlowService {
    int save(SystemAccountFlow systemAccountFlow);

    void creatReceiveManagementCharge(SystemAccount account, BigDecimal amount);

    void creatReceiveInterestManageChargeFlow(SystemAccount account, BigDecimal amount);

    void createPayInterest(SystemAccount account, BigDecimal amount);

    /**
     * 收取分期还款金额
     * @param account
     * @param amount
     */
    void createReceiveAverageReturnMoneyFlow(SystemAccount account, BigDecimal amount);
}
