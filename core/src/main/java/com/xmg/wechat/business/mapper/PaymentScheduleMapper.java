package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.PaymentSchedule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PaymentScheduleMapper {
    int insert(PaymentSchedule record);
    PaymentSchedule selectByPrimaryKey(Long id);
    int updateByPrimaryKey(PaymentSchedule record);
	Long queryPageCount(QueryObject qo);
	List<PaymentSchedule> queryPageData(QueryObject qo);

    List<PaymentSchedule> queryListByBidRequestId(Long bidRequestId);

    List<PaymentSchedule> selectByLoginfoId(Long loginfoId);

    int queryByOrderBillId(@Param("orderBillId") Long orderBillId, @Param("payingState") int payingState);

    List<PaymentSchedule> selectByOrderBillId(String orderBillId);
}