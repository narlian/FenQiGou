package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.ExpAccountGrantRecord;

import java.util.List;

public interface ExpAccountGrantRecordMapper {
    int insert(ExpAccountGrantRecord record);
    ExpAccountGrantRecord selectByPrimaryKey(Long id);
    int updateByPrimaryKey(ExpAccountGrantRecord record);

	Long queryPageCount(QueryObject qo);
	List<ExpAccountGrantRecord> queryPageData(QueryObject qo);
}