package com.xmg.wechat.business.mapper;

import com.xmg.wechat.business.domain.UserBankinfo;
import org.apache.ibatis.annotations.Param;

public interface UserBankinfoMapper {
    int insert(UserBankinfo record);
    int selectByUserinfo(@Param("accountName") String accountName, @Param("accountNumber") String accountNumber);
}