package com.xmg.wechat.business.service;

import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.BidRequest;
import com.xmg.wechat.business.query.BidRequestQueryObject;

import java.math.BigDecimal;
import java.util.List;

public interface IBidRequestService {
    boolean canApplyBorrow(Userinfo userinfo);

    void apply(BidRequest bidRequest);

    int save(BidRequest bidRequest);
    int update(BidRequest bidRequest);

    PageResult queryPage(QueryObject qo);

    void audit(Long id, int state, String remark);

    BidRequest get(Long id);

    List<BidRequest> queryIndexList(BidRequestQueryObject qo);

    void bid(Long bidRequestId, BigDecimal amount);

    void fullAudit1(Long id, int state, String remark);

    void fullAudit2(Long id, int state, String remark);

    void applyExp(BidRequest bidRequest);

}
