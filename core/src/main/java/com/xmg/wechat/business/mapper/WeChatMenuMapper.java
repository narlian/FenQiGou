package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.WeChatMenu;

import java.util.List;

public interface WeChatMenuMapper {
    int insert(WeChatMenu record);
    int updateByPrimaryKey(WeChatMenu record);
	Long queryPageCount(QueryObject qo);
	List<WeChatMenu> queryPageData(QueryObject qo);

    List<WeChatMenu> selectAll();

}