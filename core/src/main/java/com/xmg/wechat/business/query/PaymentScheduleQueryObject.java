package com.xmg.wechat.business.query;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.util.DateUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter@Getter
public class PaymentScheduleQueryObject extends QueryObject {
    private Long borrowUserId; //查询当前登录用户的还款明细
    private int state = -1;
    private Date beginDate;
    private Date endDate;
    private Integer bidRequestType;

    public Date getBeginDate() {
        return DateUtils.getStartDate(beginDate);
    }

    public Date getEndDate() {
        return DateUtils.getEndDate(endDate);
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
