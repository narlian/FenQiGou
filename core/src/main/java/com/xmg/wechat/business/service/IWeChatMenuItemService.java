package com.xmg.wechat.business.service;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.business.domain.WeChatMenuItem;
import com.xmg.wechat.business.query.WeChatMenuItemQueryObject;

import java.util.List;

public interface IWeChatMenuItemService {
    int save(WeChatMenuItem weChatMenuItem);
    int update(WeChatMenuItem weChatMenuItem);
    PageResult queryPage(WeChatMenuItemQueryObject qo);

    void saveOrUpdate(WeChatMenuItem weChatMenuItem);

    List<WeChatMenuItem> selectAll();
    List<WeChatMenuItem> selectAllId(Long id);
}
