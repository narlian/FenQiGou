package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.business.domain.WeChatMenu;
import com.xmg.wechat.business.domain.WeChatMenuItem;
import com.xmg.wechat.business.mapper.WeChatMenuMapper;
import com.xmg.wechat.business.query.WeChatMenuQueryObject;
import com.xmg.wechat.business.service.IWeChatMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeChatMenuServiceImpl implements IWeChatMenuService {
	@Autowired
	private WeChatMenuMapper weChatMenuMapper;

    @Override
    public int save(WeChatMenu weChatMenu) {
        return weChatMenuMapper.insert(weChatMenu);
    }

    @Override
    public int update(WeChatMenu weChatMenu) {
        return weChatMenuMapper.updateByPrimaryKey(weChatMenu);
    }

    @Override
    public List<WeChatMenu> selectAll() {
        return weChatMenuMapper.selectAll();
    }

    @Override
    public PageResult queryPage(WeChatMenuQueryObject qo) {
        Long count = weChatMenuMapper.queryPageCount(qo);
        if (count <= 0) {
            return PageResult.empty(qo.getPageSize());
        }
        List result = weChatMenuMapper.queryPageData(qo);
        return new PageResult(result,count.intValue(),qo.getCurrentPage(),qo.getPageSize());
    }

    @Override
    public void saveOrUpdate(WeChatMenu weChatMenu) {
        int count = this.selectAll().size();
        if (weChatMenu.getId() == null){
            if (count<3) {
                this.save(weChatMenu);
            }
        }else {
            this.update(weChatMenu);
        }
    }
}
