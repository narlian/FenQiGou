package com.xmg.wechat.business.query;

import com.xmg.wechat.base.query.QueryObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Setter@Getter
public class WeChatMenuItemQueryObject extends QueryObject {
    private Long parentId;
    private String keyword;

    public String getKeyword() {
        return StringUtils.isEmpty(keyword)?keyword:null;
    }
}
