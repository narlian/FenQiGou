package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.util.CalculatetUtil;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.*;
import com.xmg.wechat.business.mapper.PaymentScheduleMapper;
import com.xmg.wechat.business.query.PaymentScheduleQueryObject;
import com.xmg.wechat.business.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PaymentScheduleServiceImpl implements IPaymentScheduleService {
	@Autowired
	private PaymentScheduleMapper paymentScheduleMapper;
	@Autowired
	private IAccountService accountService;
	@Autowired
	private IAccountFlowService accountFlowService;
	@Autowired
	private IPaymentScheduleDetailService paymentScheduleDetailService;
	@Autowired
	private ISystemAccountService systemAccountService;
	@Autowired
	private ISystemAccountFlowService systemAccountFlowService;
	@Autowired
	private IBidRequestService bidRequestService;
	@Autowired
	private IBidService bidService;
	@Autowired
	private IExpAccountService expAccountService;
	@Autowired
	private IExpAccountFlowService expAccountFlowService;
	@Autowired
	private IOrderBillService orderBillService;
	@Override
	public int save(PaymentSchedule paymentSchedule) {
		return paymentScheduleMapper.insert(paymentSchedule);
	}

	@Override
	public int update(PaymentSchedule paymentSchedule) {
		return paymentScheduleMapper.updateByPrimaryKey(paymentSchedule);
	}

	@Override
	public PageResult queryPage(PaymentScheduleQueryObject qo) {
		int count = paymentScheduleMapper.queryPageCount(qo).intValue();
				if(count <=0 ){
					return PageResult.empty(qo.getPageSize());
				}
				List<PaymentSchedule> items = paymentScheduleMapper.queryPageData(qo);
				return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public List<PaymentSchedule> selectItemsByOrderBillId(Long orderBillId) {
		return paymentScheduleMapper.queryListByBidRequestId(orderBillId);
	}

	@Override
	public PaymentSchedule get(Long id) {
		return paymentScheduleMapper.selectByPrimaryKey(id);
	}

	@Override
	public void returnMoney(Long id) {
		//判断还款对象是待还状态, 当前用户账户余额 >= 该期还款金额, 当前用户是该期还款人
		PaymentSchedule ps = this.get(id);
		Account borrowUserAccount = accountService.getCurrent();
		SystemAccount systemAccount = systemAccountService.selectCurrent();
		if(ps!=null && ps.getState() == BidConst.PAYMENT_STATE_NORMAL && //待还状态
				(( ps.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL &&
					UserContext.getCurrent().getId().equals(ps.getBorrowUser().getId()) &&
					borrowUserAccount.getUsableAmount().compareTo(ps.getTotalAmount()) >=0)
				||
				(ps.getBidRequestType() == BidConst.BIDREQUEST_TYPE_EXP &&
					systemAccount.getUsableAmount().compareTo(ps.getInterest())>=0))
				){
			//还款对象和还款明细的变化
			//设置还款对象还款日期
			ps.setPayDate(new Date());
			//设置还款对象状态 -- > 还清
			ps.setState(BidConst.PAYMENT_STATE_DONE);
			this.update(ps);
			//设置还款明细的还款日期
			paymentScheduleDetailService.updateState(ps.getId(),ps.getPayDate());
			//借款人账户变化
			if(ps.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL ){
				//借款人可用金额减少, 待还本息减少 剩余授信额度增加(待还本金),生成还款成功流水
				borrowUserAccount.setUsableAmount(borrowUserAccount.getUsableAmount().subtract(ps.getTotalAmount()));
				borrowUserAccount.setUnReturnAmount(borrowUserAccount.getUnReturnAmount().subtract(ps.getTotalAmount()));
				borrowUserAccount.setBorrowLimit(borrowUserAccount.getRemainBorrowLimit().add(ps.getPrincipal()));
				accountService.updateByPrimaryKey(borrowUserAccount);
				accountFlowService.creatReturnMoneyFlow(borrowUserAccount,ps.getTotalAmount());
			}
			//投资人账户变化
			//获取该借款对象所有投资人, 查询投资人账户, 可用金额增加, 待收本金,利息减少, 生成回款流水
			Map<Long,Account> accountMap = new HashMap<Long, Account>();
			Map<Long,ExpAccount> expAccountMap = new HashMap<Long, ExpAccount>();
			Long bidUserId;
			Account bidUserAccount;
			ExpAccount bidUserExpAccount;
			for(PaymentScheduleDetail psd :ps.getDetails()){
				bidUserId = psd.getInvestorId();
				bidUserAccount = accountMap.get(bidUserId);
				bidUserExpAccount = expAccountMap.get(bidUserId);
				if(bidUserAccount == null){
					bidUserAccount = accountService.get(bidUserId);
					accountMap.put(bidUserId,bidUserAccount);
					bidUserExpAccount = expAccountService.get(bidUserId);
					expAccountMap.put(bidUserId,bidUserExpAccount);
				}
				if(ps.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
					bidUserAccount.setUsableAmount(bidUserAccount.getUsableAmount().add(psd.getTotalAmount()));
					bidUserAccount.setUnReceivePrincipal(bidUserAccount.getUnReceivePrincipal().subtract(psd.getPrincipal()));
					accountFlowService.creatGainReturnMoneyFlow(bidUserAccount,psd.getTotalAmount());
				}else {
					//体验标账户余额增加
					bidUserExpAccount.setUsableAmount(bidUserExpAccount.getUsableAmount().add(psd.getPrincipal()));
					expAccountFlowService.createReceiveFlow(bidUserExpAccount,psd.getPrincipal());
					//系统账户支付体验标利息 生成流水
					systemAccount.setUsableAmount(systemAccount.getUsableAmount().subtract(psd.getInterest()));
					systemAccountFlowService.createPayInterest(systemAccount,psd.getInterest());
					//用户账户收取利息
					bidUserAccount.setUsableAmount(bidUserAccount.getUsableAmount().add(psd.getInterest()));
					accountFlowService.createReceiveInterestFlow(bidUserAccount,psd.getInterest());
				}
				//账户待收利息减少
				bidUserAccount.setUnReceiveInterest(bidUserAccount.getUnReceiveInterest().subtract(psd.getInterest()));
				//支付平台利息管理费, 投资人账户金额减少, 生成支付信息管理费流水
				BigDecimal interestManageCharge = CalculatetUtil.calInterestManagerCharge(psd.getInterest());
				bidUserAccount.setUsableAmount(bidUserAccount.getUsableAmount().subtract(interestManageCharge));
				accountFlowService.creatInterestManageChargeFlow(bidUserAccount,interestManageCharge);
				//系统账号收取利息管理费, 可用金额增加, 生成利息管理费的流水
				systemAccount.setUsableAmount(systemAccount.getUsableAmount().add(interestManageCharge));
				systemAccountFlowService.creatReceiveInterestManageChargeFlow(systemAccount,interestManageCharge);
			}
			//对投资账户统一更新,
			for(Account buAcount:accountMap.values()){
				accountService.updateByPrimaryKey(buAcount);
			}
			for(ExpAccount expAccount : expAccountMap.values()){
				expAccountService.update(expAccount);
			}
			// 对系统账户更新
			systemAccountService.update(systemAccount);

			//判断这次借款是否全部还清
			//通过还款对象 获取借款对象id  查询属于该借款对象的所有还款对象集合
			Long bidRequestId = ps.getBidRequestId();
			List<PaymentSchedule> paymentScheduleList = queryListByBidRequestId(bidRequestId);
			//遍历这个集合, 判断每一个还款是否都属于还清的状态
			boolean isAllReturn = true;
			for (PaymentSchedule paymentSchedule : paymentScheduleList) {
				if(paymentSchedule.getState() != BidConst.PAYMENT_STATE_DONE){
					isAllReturn = false;
				}
			}
			//如果所有都还清了:
			if(isAllReturn){
				//修改借款对象的状态 --> 已完成
				BidRequest bidRequest = bidRequestService.get(bidRequestId);
				bidRequest.setBidRequestState(BidConst.BIDREQUEST_STATE_COMPLETE_PAY_BACK);
				bidRequestService.update(bidRequest);
				//修改投标对象的状态 --> 已完成
				bidService.updateState(bidRequestId,BidConst.BIDREQUEST_STATE_COMPLETE_PAY_BACK);
			}
		}
	}

	private List<PaymentSchedule> queryListByBidRequestId(Long bidRequestId) {
		return  paymentScheduleMapper.queryListByBidRequestId(bidRequestId);
	}

	@Override
	public List<PaymentSchedule> selectByLoginfoId(Long id) {
		return paymentScheduleMapper.selectByLoginfoId(id);
	}

	@Override
	public void wechatReturnMoney(Long id) {
		PaymentSchedule ps = paymentScheduleMapper.selectByPrimaryKey(id);
		OrderBill ob = orderBillService.selectById(ps.getOrderBillId());
		if(ps != null && ps.getState() == OrderBill.PAYMENT_STATE_PAYING_BACK){
			//获取购买人的账户
			Account account = accountService.selectByPrimaryKey(ob.getPurchaser().getId());

			if(UserContext.getCurrent().getId().equals(ob.getPurchaser().getId()) //判断当前登录用户和还款人是否同一个
					&& account.getUsableAmount().compareTo(ps.getTotalAmount()) >= 0//还款人账户的余额大于本期还款金额
					){
				//还款人账户可用金额减少，待还款金额减少，生成还款流水
				account.setUsableAmount(account.getUsableAmount().subtract(ps.getTotalAmount()));
				account.setUnReturnAmount(account.getUnReturnAmount().subtract(ps.getTotalAmount()));
				accountFlowService.createReturnMoneyFlow(account,ps.getTotalAmount());
				accountService.updateByPrimaryKey(account);
				//系统账户可用金额增加，生成系统账户流水
				SystemAccount systemAccount = systemAccountService.selectCurrent();
				systemAccount.setUsableAmount(systemAccount.getUsableAmount().add(ps.getTotalAmount()));
				systemAccountFlowService.createReceiveAverageReturnMoneyFlow(systemAccount,ps.getTotalAmount());
				systemAccountService.update(systemAccount);
				//设置还款明细参数
				ps.setPayDate(new Date());
				ps.setState(OrderBill.PAYMENT_STATE_COMPLETE_PAY_BACK);
				paymentScheduleMapper.updateByPrimaryKey(ps);
				int i = paymentScheduleMapper.queryByOrderBillId(ps.getOrderBillId(),OrderBill.PAYMENT_STATE_PAYING_BACK);
				//如果这个订单还款中的订单明细为0
				if(i <= 0){
					ob.setState(OrderBill.PAYMENT_STATE_COMPLETE_PAY_BACK);
					orderBillService.update(ob);
				}
			}else{
				throw new RuntimeException("账户余额不足");
			}
		}else{
			throw new RuntimeException("订单出错，请联系管理员进行修复");
		}
	}

	//根据订单号查询对应的还款明细
	@Override
	public List<PaymentSchedule> selectByOrderBillId(String orderBillId) {
		return paymentScheduleMapper.selectByOrderBillId(orderBillId);
	}
}
