package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.service.IProductService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.OrderBill;
import com.xmg.wechat.business.domain.OrderBillItem;
import com.xmg.wechat.business.mapper.OrderBillItemMapper;
import com.xmg.wechat.business.query.OrderBillItemQueryObject;
import com.xmg.wechat.business.service.IAccountFlowService;
import com.xmg.wechat.business.service.IOrderBillItemService;
import com.xmg.wechat.business.service.IOrderBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class OrderBillItemServiceImpl implements IOrderBillItemService {
	@Autowired
	private OrderBillItemMapper orderBillItemMapper;
	@Autowired
	private IProductService productService;
	@Autowired
	private IOrderBillService orderBillService;
	@Autowired
	private IAccountService accountService;
	@Autowired
	private IAccountFlowService accountFlowService;
	@Autowired
	private IUserinfoService userinfoService;

	@Override
	public int save(OrderBillItem orderBillItem) {
		return orderBillItemMapper.insert(orderBillItem);
	}

	@Override
	public int update(OrderBillItem orderBillItem) {
		return orderBillItemMapper.updateByPrimaryKey(orderBillItem);
	}

	@Override
	public PageResult queryPage(QueryObject qo) {
		int count = orderBillItemMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<OrderBillItem> items = orderBillItemMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void add(Long productId) {
		Long currentId = UserContext.getCurrent().getId();
		//查询该商品是否在当前用户的购物车中已存在, 如果是 数量加一, 如果不是, 新加一条
		OrderBillItem item = orderBillItemMapper.selectByProductId(productId,currentId);
		if(item == null){
			item = new OrderBillItem();
			item.setBuyerId(currentId);
			item.setNumber(1);
			item.setProduct(productService.get(productId));
			item.setPrice(productService.get(productId).getSalePrice());
			this.save(item);
		}else{
			item.setNumber(item.getNumber() + 1);
			this.update(item);
		}
	}
	//查看购物车
	@Override
	public List<OrderBillItem> queryList() {
		return orderBillItemMapper.getItemByBuyerId(UserContext.getCurrent().getId());
	}
	//普通订单
	@Override
	public void normalOrder() {
		//获取购物车中的商品
		List<OrderBillItem> orderBillItems = orderBillItemMapper.getItemByBuyerId(UserContext.getCurrent().getId());
		//创建订单
		OrderBill orderBill = new OrderBill();
		orderBill.setBuyType(OrderBill.BUYTYPE_ALLPAYMENT);
		orderBill.setBuyTime(new Date());
		orderBill.setOrderBillItems(orderBillItems);
		orderBill.setPurchaser(userinfoService.getCurrent());
		orderBillService.save(orderBill);
		BigDecimal totalAmount = BigDecimal.ZERO;
		Integer count = 0;
		for (OrderBillItem orderBillItem : orderBillItems) {
			orderBillItem.setOrderBillId(orderBill.getId());
			this.update(orderBillItem);
			totalAmount.add(orderBillItem.getAmount());
			count += orderBillItem.getNumber();
		}
		orderBill.setTotalAmount(totalAmount);
		orderBill.setCount(count);
		orderBillService.update(orderBill);
		//从账户中扣取金额
		Account current = accountService.getCurrent();
		if(current.getUsableAmount().compareTo(totalAmount)>=0){
			//从账户余额中扣除对应金额
			current.setUsableAmount(current.getUsableAmount().subtract(totalAmount));
			accountService.updateByPrimaryKey(current);
			//生成流水
			accountFlowService.creatBidSuccessFlow(current,totalAmount);
		}
	}

    @Override
    public List<OrderBillItem> selectItemsByOrderBillId(Long orderBillId) {
        return orderBillItemMapper.selectByOrderBillId(orderBillId);
    }
	//减少购物车商品
	@Override
	public void subItem(Long id) {
		//判断该商品是否最后一个 是则删除 不是则数量减一
		Long currentId = UserContext.getCurrent().getId();
		OrderBillItem orderBillItem = orderBillItemMapper.selectByPrimaryKey(id);
		if(orderBillItem != null && orderBillItem.getNumber() > 1){
			orderBillItemMapper.subItemById(id);
		}else{
			this.deleteItem(id);
		}
	}

	//删除购物车中的商品
	@Override
	public void deleteItem(Long id) {
		orderBillItemMapper.deleteItem(id);
	}
	//购物车中添加商品
	@Override
	public void addItem(Long itemId) {
		OrderBillItem orderBillItem = orderBillItemMapper.selectByPrimaryKey(itemId);
		orderBillItem.setNumber(orderBillItem.getNumber()+1);
		this.update(orderBillItem);
	}

	//根据订单id查询当前用户的普通订单明细

	@Override
	public List<OrderBillItem> queryListByORderBillId(OrderBillItemQueryObject qo) {
		return orderBillItemMapper.queryPageData(qo);
	}
}
