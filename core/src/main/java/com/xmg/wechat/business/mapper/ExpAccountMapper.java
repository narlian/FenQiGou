package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.ExpAccount;

import java.util.List;

public interface ExpAccountMapper {
    int insert(ExpAccount record);
    ExpAccount selectByPrimaryKey(Long id);
    int updateByPrimaryKey(ExpAccount record);
	Long queryPageCount(QueryObject qo);
	List<ExpAccount> queryPageData(QueryObject qo);
}