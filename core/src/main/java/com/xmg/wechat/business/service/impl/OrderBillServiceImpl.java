package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.domain.Product;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.service.IProductService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.OrderBill;
import com.xmg.wechat.business.domain.PaymentSchedule;
import com.xmg.wechat.business.mapper.OrderBillMapper;
import com.xmg.wechat.business.query.OrderBillQueryObject;
import com.xmg.wechat.business.service.IOrderBillService;
import com.xmg.wechat.business.service.IPaymentScheduleService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class OrderBillServiceImpl implements IOrderBillService {
	@Autowired
	private OrderBillMapper orderBillMapper;
	@Autowired
	private IProductService productService;
	@Autowired
	private IUserinfoService userinfoService;
	@Autowired
	private IPaymentScheduleService paymentScheduleService;
	@Autowired
	private IAccountService accountService;
	@Override
	public int save(OrderBill orderBill) {
		return orderBillMapper.insert(orderBill);
	}

	@Override
	public int update(OrderBill orderBill) {
		return orderBillMapper.updateByPrimaryKey(orderBill);
	}

	@Override
	public OrderBill selectById(Long id) {
		return orderBillMapper.selectByPrimaryKey(id);
	}

	@Override
	public PageResult query(OrderBillQueryObject qo) {
		Long count = orderBillMapper.queryPageCount(qo);
		if(count <= 0){
		    return new PageResult(Collections.EMPTY_LIST,0,1,1);
		}
		List data = orderBillMapper.queryPageData(qo);
		return new PageResult(data,count.intValue(),qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void averagePayment(Long id,int repaymentNumber,String address) {
		//根据产品id查询出产品对象
		Product product = productService.get(id);
		//判断该用户是否已经绑定银行卡
		Userinfo userinfo = userinfoService.selectByPrimaryKey(UserContext.getCurrent().getId());
		if(userinfo.getIsBoundBankCard()){
			//创建分期付款订单设置对象
			OrderBill apob = new OrderBill();
			apob.setProduct(product);
			apob.setCount(1);
			apob.setTotalAmount(product.getSalePrice());
			apob.setRepaymentNumber(repaymentNumber);
			apob.setState(OrderBill.PAYMENT_STATE_AUDITING);
			apob.setPurchaser(userinfo);
			apob.setBuyType(OrderBill.BUYTYPE_AVERAGEPAYMENT);
			apob.setApplyTime(new Date());
			apob.setAddress(address);
			orderBillMapper.insert(apob);
		}else{
			throw new RuntimeException("您还没有绑定过银行卡哦，请先绑定银行卡再申请分期付款！");
		}
	}





	// 根据OrderBillItem 表中的 orderBillId 查询OrderBill 此时的OrderBill product_id 没有值 不能关联产品表
	@Override
	public OrderBill getById(Long orderBillId) {
		return orderBillMapper.getById(orderBillId);
	}




	@Override
	public List<OrderBill> queryBidOrderList() {
		return orderBillMapper.queryBidOrderList(UserContext.getCurrent().getId(),OrderBill.BUYTYPE_AVERAGEPAYMENT);
	}

	@Override
	public List<OrderBill> queryNormalOrderList() {
		return orderBillMapper.queryNormalOrderList(UserContext.getCurrent().getId(),OrderBill.BUYTYPE_ALLPAYMENT);
	}




	@Override
	public void audit(Long id, int state) {
		OrderBill orderBill = orderBillMapper.selectByPrimaryKey(id);
		if(orderBill != null
				&& orderBill.getPurchaser().getIsBoundBankCard()
				&& orderBill.getState() == OrderBill.PAYMENT_STATE_AUDITING){
				Userinfo auditUserinfo = userinfoService.selectByPrimaryKey(UserContext.getCurrent().getId());
				orderBill.setAuditor(auditUserinfo);
				orderBill.setBuyTime(new Date());
				Logininfo logininfo = new Logininfo();
				logininfo.setId(orderBill.getPurchaser().getId());
			if(state == OrderBill.STATE_PASS){
				//审核成功
				orderBill.setState(OrderBill.PAYMENT_STATE_PAYING_BACK);
				//账户待还款金额增加
				Account account = accountService.selectByPrimaryKey(orderBill.getPurchaser().getId());
				account.setUnReturnAmount(account.getUnReturnAmount().add(orderBill.getTotalAmount()).add(new BigDecimal(100)));
				accountService.updateByPrimaryKey(account);
				//生成还款明细
				ArrayList<PaymentSchedule> paymentSchedules = new ArrayList<PaymentSchedule>();
				BigDecimal totalAmountTemp = BigDecimal.ZERO;
				for (int i = 0; i < orderBill.getRepaymentNumber(); i++) {
					PaymentSchedule ps = new PaymentSchedule();
					ps.setPayDate(DateUtils.addMonths(orderBill.getBuyTime(),i + 1));
					ps.setDeadLine(DateUtils.addMonths(orderBill.getBuyTime(),i + 2));
					ps.setMonthIndex(i + 1);
					ps.setState(OrderBill.PAYMENT_STATE_PAYING_BACK);
					ps.setOrderBillId(orderBill.getId());
					ps.setBorrowUser(logininfo);
					if(i < orderBill.getRepaymentNumber() - 1){
						ps.setTotalAmount(orderBill.getTotalAmount().add(new BigDecimal(100)).divide(new BigDecimal(orderBill.getRepaymentNumber()),2, RoundingMode.HALF_UP));
						totalAmountTemp = totalAmountTemp.add(ps.getTotalAmount());
					}else{
						ps.setTotalAmount(orderBill.getTotalAmount().add(new BigDecimal(100)).subtract(totalAmountTemp));
					}
					paymentScheduleService.save(ps);
					paymentSchedules.add(ps);
				}
				orderBill.setPaymentSchedule(paymentSchedules);
			}else if(state == OrderBill.STATE_REJECT){
				//审核失败
				orderBill.setState(OrderBill.STATE_REJECT);
			}
			orderBillMapper.updateByPrimaryKey(orderBill);
		}else{
			throw new RuntimeException("未绑定银行卡");
		}
	}

	@Override
	public OrderBill selectByPurchaserId(Long purchaserId) {
		return orderBillMapper.selectByPurchaserId(purchaserId,OrderBill.PAYMENT_STATE_PAYING_BACK);
	}
}

