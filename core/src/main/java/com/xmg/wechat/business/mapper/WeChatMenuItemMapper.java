package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.WeChatMenuItem;

import java.util.List;

public interface WeChatMenuItemMapper {
    int insert(WeChatMenuItem record);
    WeChatMenuItem selectByPrimaryKey(Long id);
    void delete(Long id);
    int updateByPrimaryKey(WeChatMenuItem record);
	Long queryPageCount(QueryObject qo);
	List<WeChatMenuItem> queryPageData(QueryObject qo);

    List<WeChatMenuItem> selectAll();

    List<WeChatMenuItem> selectAllId(Long id);
}