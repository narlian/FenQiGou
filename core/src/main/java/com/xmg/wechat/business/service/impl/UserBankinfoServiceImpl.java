package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.UserBankinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.business.mapper.UserBankinfoMapper;
import com.xmg.wechat.business.service.IUserBankinfoService;

@Service
public class UserBankinfoServiceImpl implements IUserBankinfoService {
	@Autowired
	private UserBankinfoMapper userBankinfoMapper;

	@Override
	public int save(UserBankinfo userBankinfo) {
		return userBankinfoMapper.insert(userBankinfo);
	}
}
