package com.xmg.wechat.business.domain;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.xmg.wechat.base.domain.BaseDomain;
import com.xmg.wechat.base.domain.Product;
import com.xmg.wechat.base.domain.Userinfo;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

//订单对象
@Setter@Getter
public class OrderBill extends BaseDomain {
    public static final int PAYMENT_STATE_AUDITING = 0;             //待审核
    public static final int PAYMENT_STATE_PAYING_BACK = 1;          //还款中
    public static final int PAYMENT_STATE_COMPLETE_PAY_BACK = 2;    //已还清
    public static final int STATE_PASS = 3;                         //审核通过
    public static final int STATE_REJECT = 4;                       //审核拒绝

    public static final int BUYTYPE_ALLPAYMENT = 0;                 //全额付款
    public static final int BUYTYPE_AVERAGEPAYMENT = 1;             //分期付款

    private Product product;                        //购买商品
    private int count;                              //购买数量  商品数量
    private BigDecimal totalAmount;                 //总金额
    private int repaymentNumber;                    //还款期数
    private int state;                              //订单状态
    private Userinfo purchaser;                     //购买人
    private int buyType;                            //购买类型
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date buyTime;                           //购买时间
    private Userinfo auditor;                       //审核人
    private Date applyTime;                         //申请时间
    private String address;                         //收货地址

    private List<PaymentSchedule> paymentSchedule;  //还款期数
    private List<OrderBillItem> orderBillItems;     //订单明细  购物车明细

    //不是分期购的商品总额
    public BigDecimal getTotalSum(){
        BigDecimal totalsum = BigDecimal.ZERO;
        for (OrderBillItem orderBillItem : orderBillItems) {
            totalsum = totalsum.add(orderBillItem.getAmount());
        }
        return totalsum;
    }

    public String getStateDisplay(){
        switch (this.state){
            case PAYMENT_STATE_AUDITING : return "待审核";
            case PAYMENT_STATE_PAYING_BACK : return "还款中";
            case PAYMENT_STATE_COMPLETE_PAY_BACK : return "已还清";
            default : return "已付款";
        }
    }

    public BigDecimal getAverageReturnMoney(){
        return this.totalAmount.add(new BigDecimal(100)).divide(new BigDecimal(this.repaymentNumber),2, RoundingMode.HALF_UP);
    }

    public String getBuyTypeDisplay(){
        return this.buyType == BUYTYPE_ALLPAYMENT ? "全额付款":"分期付款";
    }

    public String getJsonString(){
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("id",id);
        return JSON.toJSONString(map);
    }
}
