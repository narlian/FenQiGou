package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.business.domain.Bid;
import com.xmg.wechat.business.mapper.BidMapper;
import com.xmg.wechat.business.service.IBidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BidServiceImpl implements IBidService {
	@Autowired
	private BidMapper bidMapper;

	@Override
	public int save(Bid bid) {
		return bidMapper.insert(bid);
	}

	@Override
	public int updateState(Long bidRequestId, int state) {
		return bidMapper.updateState(bidRequestId,state);
	}
}
