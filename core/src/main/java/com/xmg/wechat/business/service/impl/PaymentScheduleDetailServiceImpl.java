package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.business.domain.PaymentScheduleDetail;
import com.xmg.wechat.business.mapper.PaymentScheduleDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.business.service.IPaymentScheduleDetailService;

import java.util.Date;

@Service
public class PaymentScheduleDetailServiceImpl implements IPaymentScheduleDetailService {
	@Autowired
	private PaymentScheduleDetailMapper paymentScheduleDetailMapper;

	@Override
	public int save(PaymentScheduleDetail paymentScheduleDetail) {
		return paymentScheduleDetailMapper.insert(paymentScheduleDetail);
	}

	@Override
	public int update(PaymentScheduleDetail paymentScheduleDetail) {
		return paymentScheduleDetailMapper.updateByPrimaryKey(paymentScheduleDetail);
	}

	@Override
	public void updateState(Long id, Date payDate) {
		paymentScheduleDetailMapper.updateState(id,payDate);
	}
}
