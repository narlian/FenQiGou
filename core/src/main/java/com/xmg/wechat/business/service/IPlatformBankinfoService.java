package com.xmg.wechat.business.service;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.PlatformBankinfo;

import java.util.List;

public interface IPlatformBankinfoService {
    int save(PlatformBankinfo platformBankinfo);
    int update(PlatformBankinfo platformBankinfo);
    PageResult queryPage(QueryObject qo);

    void saveOrUpdate(PlatformBankinfo platformBankinfo);

    List<PlatformBankinfo> sellectAll();

}
