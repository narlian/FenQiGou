package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.business.domain.ExpAccount;
import com.xmg.wechat.business.domain.ExpAccountFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.business.mapper.ExpAccountFlowMapper;
import com.xmg.wechat.business.service.IExpAccountFlowService;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 */
@Service
public class ExpAccountFlowServiceImpl implements IExpAccountFlowService {
	@Autowired
	private ExpAccountFlowMapper expAccountFlowMapper;

	@Override
	public int save(ExpAccountFlow expAccountFlow) {
		return expAccountFlowMapper.insert(expAccountFlow);
	}

	@Override
	public void createBidFlow(ExpAccount expAccount, BigDecimal amount) {
		ExpAccountFlow flow = createFlow(expAccount);
		flow.setActionType(BidConst.EXPMONEY_FLOW_BID);
		flow.setAmount(amount);
		flow.setNote("体验金投标冻结" + amount);
		this.save(flow);
	}

	@Override
	public void createBidFailFlow(ExpAccount expAccount, BigDecimal amount) {
		ExpAccountFlow flow = createFlow(expAccount);
		flow.setActionType(BidConst.EXPMONEY_FLOW_BID_FAIL);
		flow.setAmount(amount);
		flow.setNote("体验金投标失败,退回金额" + amount);
		this.save(flow);
	}

	@Override
	public void createBidSuccessFlow(ExpAccount expAccount, BigDecimal amount) {
		ExpAccountFlow flow = createFlow(expAccount);
		flow.setActionType(BidConst.EXPMONEY_FLOW_BID_SUCCESS);
		flow.setAmount(amount);
		flow.setNote("体验金投标成功,冻结金额减少" + amount);
		this.save(flow);
	}

	@Override
	public void createReceiveFlow(ExpAccount expAccount, BigDecimal amount) {
		ExpAccountFlow flow = createFlow(expAccount);
		flow.setActionType(BidConst.EXPMONEY_CALLBACK);
		flow.setAmount(amount);
		flow.setNote("体验金回款,金额增加" + amount);
		this.save(flow);
	}

	public ExpAccountFlow createFlow(ExpAccount expAccount){
		ExpAccountFlow flow = new ExpAccountFlow();
		flow.setActionTime(new Date());
		flow.setExpAccountId(expAccount.getId());
		flow.setUsableAmount(expAccount.getUsableAmount());
		flow.setFreezedAmount(expAccount.getFreezedAmount());
		return flow;
	}

}
