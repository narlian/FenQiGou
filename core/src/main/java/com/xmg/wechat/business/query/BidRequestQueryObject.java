package com.xmg.wechat.business.query;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.util.DateUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter@Getter
public class BidRequestQueryObject extends QueryObject {
    private int bidRequestState = -1;
    private int[] states;
    private String  orderByType;

    private int bidRequestType = -1;

    private Date beginDate;
    private Date endDate;
    public Date getBeginDate() {
        return DateUtils.getStartDate(beginDate);
    }

    public Date getEndDate() {
        return DateUtils.getEndDate(endDate);
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
