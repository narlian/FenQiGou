package com.xmg.wechat.business.domain;

import com.xmg.wechat.base.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

//用户银行信息
@Setter@Getter
public class UserBankinfo extends BaseDomain {
    private String bankName;//银行名
    private String accountNumber;//卡号
    private String forkName;//支行名
    private String accountName;//开户人姓名
    private Long userinfoId;    //用户id

}
