package com.xmg.wechat.business.service;

import com.xmg.wechat.business.domain.SystemAccount;

public interface ISystemAccountService {
    int save(SystemAccount systemAccount);
    int update(SystemAccount systemAccount);
    SystemAccount initAccount();
    SystemAccount selectCurrent();
}
