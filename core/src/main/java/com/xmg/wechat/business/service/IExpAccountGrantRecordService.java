package com.xmg.wechat.business.service;
import com.xmg.wechat.business.domain.ExpAccountGrantRecord;

public interface IExpAccountGrantRecordService {
    void save(ExpAccountGrantRecord record);
}
