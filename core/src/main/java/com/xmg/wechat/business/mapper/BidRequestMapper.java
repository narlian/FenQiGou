package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.BidRequest;

import java.util.List;

public interface BidRequestMapper {
    int insert(BidRequest record);
    BidRequest selectByPrimaryKey(Long id);
    int updateByPrimaryKey(BidRequest record);
	Long queryPageCount(QueryObject qo);
	List<BidRequest> queryPageData(QueryObject qo);

}