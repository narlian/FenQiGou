package com.xmg.wechat.business.query;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.util.DateUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter@Getter
public class OrderBillQueryObject extends QueryObject {
    private int buyType = -1;
    private Date beginDate;
    private Date endDate;
    private int state = -1;
    private Date beginApplyTime;
    private Date endApplyTime;
    public Date getBeginDate() {
        return DateUtils.getStartDate(beginDate);
    }

    public Date getEndDate() {
        return DateUtils.getEndDate(endDate);
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getBeginApplyTime() {
        return beginApplyTime;
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setBeginApplyTime(Date beginApplyTime) {
        this.beginApplyTime = beginApplyTime;
    }

    public Date getEndApplyTime() {
        return endApplyTime;
    }
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public void setEndApplyTime(Date endApplyTime) {
        this.endApplyTime = endApplyTime;
    }
}
