package com.xmg.wechat.business.service;

import com.xmg.wechat.business.domain.PaymentScheduleDetail;

import java.util.Date;

public interface IPaymentScheduleDetailService {
    int save(PaymentScheduleDetail paymentScheduleDetail);
    int update(PaymentScheduleDetail paymentScheduleDetail);

    void updateState(Long id, Date payDate);
}
