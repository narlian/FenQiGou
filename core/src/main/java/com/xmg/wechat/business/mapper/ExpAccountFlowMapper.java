package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.ExpAccountFlow;

import java.util.List;

public interface ExpAccountFlowMapper {
    int insert(ExpAccountFlow record);
    ExpAccountFlow selectByPrimaryKey(Long id);

	Long queryPageCount(QueryObject qo);
	List<ExpAccountFlow> queryPageData(QueryObject qo);
}