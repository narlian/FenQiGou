package com.xmg.wechat.business.service;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.business.domain.WeChatMenu;
import com.xmg.wechat.business.domain.WeChatMenuItem;
import com.xmg.wechat.business.query.WeChatMenuQueryObject;

import java.util.List;

public interface IWeChatMenuService {
    int save(WeChatMenu weChatMenu);
    int update(WeChatMenu weChatMenu);
    PageResult queryPage(WeChatMenuQueryObject qo);

    void saveOrUpdate(WeChatMenu weChatMenu);

    List<WeChatMenu> selectAll();
}
