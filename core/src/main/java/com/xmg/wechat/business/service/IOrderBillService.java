package com.xmg.wechat.business.service;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.business.domain.OrderBill;
import com.xmg.wechat.business.query.OrderBillQueryObject;

import java.util.List;

public interface IOrderBillService {
    int save(OrderBill orderBill);

    int update(OrderBill orderBill);

    OrderBill selectById(Long id);

    PageResult query(OrderBillQueryObject qo);

    /**
     * 分期付款
     * @param id
     */
    void averagePayment(Long id,int repaymentNumber,String address);


    /**
     * 根据OrderBillItem 表中的 orderBillId 查询OrderBill
     * 此时的OrderBill product_id 没有值 不能关联产品表
     * @param orderBillId
     * @return
     */
    OrderBill getById(Long orderBillId);

    /**
     * 根据当前登录用户id查询 用户分期购列表
     * @return
     */
    List<OrderBill> queryBidOrderList();
    /**
     * 根据当前登录用户id查询 用户普通订单列表
     * @return
     */
    List<OrderBill> queryNormalOrderList();

    /**
     * 分期付款审核
     * @param id
     * @param state
     */
    void audit(Long id, int state);

    /**
     * 查询当前用户的未还款明细
     * @param id
     * @return
     */
    OrderBill selectByPurchaserId(Long id);
}
