package com.xmg.wechat.business.query;

import com.xmg.wechat.base.query.BaseAuthQueryObject;
import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class RechargeOfflineQueryObject extends BaseAuthQueryObject {
    private String tradeCode;

}
