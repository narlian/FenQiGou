package com.xmg.wechat.business.service;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.business.domain.RechargeOffline;
import com.xmg.wechat.business.query.RechargeOfflineQueryObject;

public interface IRechargeOfflineService {
    int save(RechargeOffline rechargeOffline);

    void applyCharge(RechargeOffline rechargeOffline);

    PageResult queryPage(RechargeOfflineQueryObject qo);

    void audit(Long id, int state, String remark);
}
