package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.business.domain.BidRequestAuditHistory;
import com.xmg.wechat.business.mapper.BidRequestAuditHistoryMapper;
import com.xmg.wechat.business.service.IBidRequestAuditHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BidRequestAuditHistoryServiceImpl implements IBidRequestAuditHistoryService {
	@Autowired
	private BidRequestAuditHistoryMapper bidRequestAuditHistoryMapper;

	@Override
	public int save(BidRequestAuditHistory bidRequestAuditHistory) {
		return bidRequestAuditHistoryMapper.insert(bidRequestAuditHistory);
	}

	@Override
	public List<BidRequestAuditHistory> getListByBidRequestId(Long bidRequestId) {
		return bidRequestAuditHistoryMapper.getListByBidRequestId(bidRequestId);
	}
}
