package com.xmg.wechat.business.service;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.business.domain.AccountFlow;

import java.math.BigDecimal;

public interface IAccountFlowService {
    int save(AccountFlow accountFlow);

    /**
     * 创建流水
     * @param account
     * @param amount
     */
    void createRechargeFlow(Account account, BigDecimal amount);

    void creatBidFlow(Account account, BigDecimal amount);

    void creatBidFailedFlow(Account account, BigDecimal amount);

    void creatBorrowSuccessFlow(Account account, BigDecimal amount);

    void managementChargeFlow(Account account, BigDecimal amount);

    void creatBidSuccessFlow(Account account, BigDecimal amount);

    void creatReturnMoneyFlow(Account account, BigDecimal amount);

    void creatGainReturnMoneyFlow(Account account, BigDecimal amount);

    void creatInterestManageChargeFlow(Account account, BigDecimal amount);

    void createReceiveInterestFlow(Account account, BigDecimal amount);

    /**
     * 创建分期还款流水
     * @param account
     * @param amount
     */
    void createReturnMoneyFlow(Account account, BigDecimal amount);
}
