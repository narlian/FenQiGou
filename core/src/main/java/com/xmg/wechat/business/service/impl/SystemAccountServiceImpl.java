package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.business.domain.SystemAccount;
import com.xmg.wechat.business.mapper.SystemAccountMapper;
import com.xmg.wechat.business.service.ISystemAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemAccountServiceImpl implements ISystemAccountService {
	@Autowired
	private SystemAccountMapper systemAccountMapper;

	@Override
	public int save(SystemAccount systemAccount) {
		return systemAccountMapper.insert(systemAccount);
	}

	@Override
	public int update(SystemAccount systemAccount) {
		int count = systemAccountMapper.updateByPrimaryKey(systemAccount);
		if(count <= 0){
			throw new RuntimeException("操作失败,请重试");
		}
		return count;
	}

	@Override
	public SystemAccount initAccount() {
		SystemAccount systemAccount = new SystemAccount();
		this.save(systemAccount);
		return systemAccount;
	}

	@Override
	public SystemAccount selectCurrent() {
		SystemAccount systemAccount = systemAccountMapper.selectCurrent();
		if(systemAccount == null){
			systemAccount = this.initAccount();
		}
		return systemAccount;
	}
}
