package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.OrderBill;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderBillMapper {
    int insert(OrderBill record);
    OrderBill selectByPrimaryKey(Long id);
    int updateByPrimaryKey(OrderBill record);
	Long queryPageCount(QueryObject qo);
	List<OrderBill> queryPageData(QueryObject qo);

    /**
     *  根据OrderBillItem 表中的 orderBillId 查询OrderBill 此时的OrderBill product_id 没有值 不能关联产品表
     * @param orderBillId
     * @return
     */
    OrderBill getById(Long orderBillId);

    List<OrderBill> queryBidOrderList(@Param("id") Long id, @Param("buyType") int buyType);

    List<OrderBill> queryNormalOrderList(@Param("id") Long id, @Param("buyType") int buyType);

    OrderBill selectByPurchaserId(@Param("purchaserId") Long purchaserId, @Param("unPayAll") int unPayAll);
}