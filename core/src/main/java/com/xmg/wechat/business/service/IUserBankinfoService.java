package com.xmg.wechat.business.service;

import com.xmg.wechat.business.domain.UserBankinfo;

public interface IUserBankinfoService {
    int save(UserBankinfo userBankinfo);
}
