package com.xmg.wechat.business.domain;

import com.xmg.wechat.base.domain.BaseDomain;
import com.xmg.wechat.base.domain.BidConst;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter@Getter
public class SystemAccount extends BaseDomain {
    private int version;
    private BigDecimal usableAmount = BidConst.ZERO;//账户可用余额
    private BigDecimal freezedAmount = BidConst.ZERO;//账户冻结金额
}
