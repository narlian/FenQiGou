package com.xmg.wechat.business.mapper;

import com.xmg.wechat.business.domain.SystemAccount;

public interface SystemAccountMapper {
    int insert(SystemAccount record);
    SystemAccount selectCurrent();
    int updateByPrimaryKey(SystemAccount record);
}