package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.RechargeOffline;

import java.util.List;

public interface RechargeOfflineMapper {
    int insert(RechargeOffline record);
    RechargeOffline selectByPrimaryKey(Long id);
    int updateByPrimaryKey(RechargeOffline record);
	Long queryPageCount(QueryObject qo);
	List<RechargeOffline> queryPageData(QueryObject qo);
}