package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.Bid;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BidMapper {
    int insert(Bid record);
    Bid selectByPrimaryKey(Long id);
    int updateByPrimaryKey(Bid record);
	Long queryPageCount(QueryObject qo);
	List<Bid> queryPageData(QueryObject qo);
	List<Bid> selectByBidRequestId(Long bidRequestId);

    int updateState(@Param("bidRequestId") Long bidRequestId, @Param("state") int state);
}