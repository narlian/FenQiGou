package com.xmg.wechat.business.service;

import com.xmg.wechat.business.domain.BidRequestAuditHistory;

import java.util.List;

public interface IBidRequestAuditHistoryService {
    int save(BidRequestAuditHistory bidRequestAuditHistory);

    List<BidRequestAuditHistory> getListByBidRequestId(Long bidRequestId);
}
