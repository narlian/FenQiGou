package com.xmg.wechat.business.query;

import com.xmg.wechat.base.query.QueryObject;
import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class OrderBillItemQueryObject extends QueryObject {
    private Long orderBillId;
    private int state = -1;
    private Long currentUserId;
}
