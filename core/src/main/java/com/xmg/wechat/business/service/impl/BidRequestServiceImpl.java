package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.BitStatesUtils;
import com.xmg.wechat.base.util.CalculatetUtil;
import com.xmg.wechat.base.util.DecimalFormatUtil;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.*;
import com.xmg.wechat.business.mapper.BidRequestMapper;
import com.xmg.wechat.business.query.BidRequestQueryObject;
import com.xmg.wechat.business.service.*;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class BidRequestServiceImpl implements IBidRequestService {
	@Autowired
	private BidRequestMapper bidRequestMapper;
	@Autowired
	private IUserinfoService userinfoService;
	@Autowired
	private IAccountService accountService;
	@Autowired
	private IBidRequestAuditHistoryService bidRequestAuditHistoryService;
	@Autowired
	private IBidService bidService;
	@Autowired
	private IAccountFlowService accountFlowService;
	@Autowired
	private ISystemAccountFlowService systemAccountFlowService;
	@Autowired
	private ISystemAccountService systemAccountService;
	@Autowired
	private IPaymentScheduleService paymentScheduleService;
	@Autowired
	private IPaymentScheduleDetailService paymentScheduleDetailService;
	@Autowired
	private IExpAccountService expAccountService;
	@Autowired
	private IExpAccountFlowService expAccountFlowService;

	@Override
	public boolean canApplyBorrow(Userinfo userinfo) {
		if(userinfo.getIsBasicInfo() && //基本信息
				userinfo.getIsVedioAuth() && //视频认证
				userinfo.getIsRealAuth() && //实名认证
				userinfo.getScore() >= BidConst.CREDIT_BORROW_SCORE //材料认证分数达到30分
				){
			return true;
		}
		return false;
	}

	@Override
	public int save(BidRequest bidRequest) {
		return bidRequestMapper.insert(bidRequest);
	}

	@Override
	public int update(BidRequest bidRequest) {
		int count = bidRequestMapper.updateByPrimaryKey(bidRequest);
		if(count <= 0){
			throw new RuntimeException("操作失败, 请重试");
		}
		return count;
	}

	@Override
	public PageResult queryPage(QueryObject qo) {
		int count = bidRequestMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<BidRequest> items = bidRequestMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}


	//发标前审核
	@Override
	public void audit(Long id, int state, String remark) {
		//1. 根据id获取BidRequest对象, 判断是否处于待发布状态
		BidRequest bidRequest = bidRequestMapper.selectByPrimaryKey(id);
		if(bidRequest.getBidRequestState() == BidConst.BIDREQUEST_STATE_PUBLISH_PENDING){
			//2 创建审核历史对象,设置相关属性
			BidRequestAuditHistory brah = new BidRequestAuditHistory();
			brah.setRemark(remark);
			brah.setApplyTime(bidRequest.getApplyTime());
			brah.setApplier(bidRequest.getCreateUser());
			brah.setAuditTime(new Date());
			brah.setAuditor(UserContext.getCurrent());
			brah.setBidRequestId(bidRequest.getId());
			brah.setAuditType(BidRequestAuditHistory.PUBLISH_AUDIT);
			//3 判断审核状态: 成功
			if(state == BidRequestAuditHistory.STATE_PASS){
				//设置审核对象状态
				brah.setState(BidRequestAuditHistory.STATE_PASS);
				//设置借款对象, 状态 = 招标中, 截止时间 = 当前时间 + 招标天数  发布时间 = 当前时间  风控意见
				bidRequest.setBidRequestState(BidConst.BIDREQUEST_STATE_BIDDING);
				bidRequest.setDisableDate(DateUtils.addDays(new Date(),bidRequest.getDisableDays()));
				bidRequest.setPublishTime(new Date());
			}else{
				//4 审核状态:拒绝
				//设置审核对象状态
				brah.setState(BidRequestAuditHistory.STATE_REJECT);
				bidRequest.setBidRequestState(BidConst.BIDREQUEST_STATE_PUBLISH_REFUSE);//发标审核拒绝
				//判断信用标 才需要更新借款人信息
				if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
					//移除 Userinfo中正在借款的状态码, 更新借款人信息
					Userinfo userinfo = userinfoService.selectByPrimaryKey(bidRequest.getCreateUser().getId());
					userinfo.removeState(BitStatesUtils.HAS_BIDREQUEST_PROCESS);
					userinfoService.updateByPrimaryKey(userinfo);
				}
			}
			bidRequest.setNote(remark);
			bidRequestAuditHistoryService.save(brah);
			this.update(bidRequest);
		}
	}

	@Override
	public BidRequest get(Long id) {
		return bidRequestMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<BidRequest> queryIndexList(BidRequestQueryObject qo) {
		return bidRequestMapper.queryPageData(qo);
	}

	@Override
	public void apply(BidRequest bidRequest) {
		// 符合利息范围
		//用户没有正在执行的贷款申请
		Userinfo userinfo = userinfoService.getCurrent();
		if(this.canApplyBorrow(userinfo) &&// 判断申请人是否有贷款资格
				!userinfo.hasBidRequestProcess() && //没有审核中的贷款
				bidRequest.getBidRequestAmount().compareTo(BidConst.SMALLEST_BIDREQUEST_AMOUNT) >= 0 && // 申请金额  <= 剩余申请额度
				bidRequest.getBidRequestAmount().compareTo(accountService.getCurrent().getRemainBorrowLimit()) <=0 &&
				bidRequest.getCurrentRate().compareTo(BidConst.SMALLEST_CURRENT_RATE) >= 0 && //申请利息 > = 系统最利息
				bidRequest.getCurrentRate().compareTo(BidConst.MAX_CURRENT_RATE) <= 0 && //申请利息 < = 系统最大利息
				bidRequest.getMinBidAmount().compareTo(BidConst.SMALLEST_BID_AMOUNT ) >= 0 // 最小投标 > = 系统最小投标
				){
			//给借款对象设置相关信息
			BidRequest request = new BidRequest();
			//request.setReturnType(bidRequest.getReturnType());
			request.setReturnType(BidConst.RETURN_TYPE_MONTH_INTEREST_PRINCIPAL);//还款类型(等额本息)
			request.setBidRequestType(BidConst.BIDREQUEST_TYPE_NORMAL);//借款类型(信用标)
			request.setBidRequestState(BidConst.BIDREQUEST_STATE_PUBLISH_PENDING);//借款状态待发布
			request.setMinBidAmount(bidRequest.getMinBidAmount());// 最小投標金额
			request.setMonthes2Return(bidRequest.getMonthes2Return());
			request.setTitle(bidRequest.getTitle());// 借款标题
			request.setDescription(bidRequest.getDescription());// 借款描述
			request.setDisableDays(bidRequest.getDisableDays());// 招标天数
			request.setApplyTime(new Date());// 申请时间
			request.setCurrentRate(bidRequest.getCurrentRate());
			request.setCreateUser(UserContext.getCurrent());// 借款人
			request.setBidRequestAmount(bidRequest.getBidRequestAmount());
			request.setTotalRewardAmount(CalculatetUtil
					.calTotalInterest(request.getReturnType(),request.getBidRequestAmount(),request.getCurrentRate(),request.getMonthes2Return()));
			this.save(request);
			//修改申请人状态, userinfo
			userinfo.addState(BitStatesUtils.HAS_BIDREQUEST_PROCESS);
			userinfoService.updateByPrimaryKey(userinfo);
		}
	}
	//发放系统体验标
	@Override
	public void applyExp(BidRequest bidRequest) {
		//判断最小投标金额和最小借款金额
		if(bidRequest.getMinBidAmount().compareTo(BidConst.SMALLEST_BID_AMOUNT ) >= 0 &&// 最小投标 > = 系统最小投标
				bidRequest.getBidRequestAmount().compareTo(BidConst.SMALLEST_BIDREQUEST_AMOUNT) >= 0 // 借款金额 > = 系统最小借款金额
				){
			//给借款对象设置相关信息
			BidRequest request = new BidRequest();
			request.setReturnType(BidConst.RETURN_TYPE_MONTH_INTEREST_PRINCIPAL);//还款类型(等额本息)
			request.setBidRequestType(BidConst.BIDREQUEST_TYPE_EXP);//借款类型(体验标)
			request.setBidRequestState(BidConst.BIDREQUEST_STATE_PUBLISH_PENDING);//借款状态待发布
			request.setMinBidAmount(bidRequest.getMinBidAmount());// 最小投標金额
			request.setMonthes2Return(bidRequest.getMonthes2Return());
			request.setTitle(bidRequest.getTitle());// 借款标题
			request.setDisableDays(bidRequest.getDisableDays());// 招标天数
			request.setApplyTime(new Date());// 申请时间
			request.setCreateUser(UserContext.getCurrent());
			request.setCurrentRate(bidRequest.getCurrentRate());
			request.setBidRequestAmount(bidRequest.getBidRequestAmount());
			request.setTotalRewardAmount(CalculatetUtil
					.calTotalInterest(request.getReturnType(),request.getBidRequestAmount(),request.getCurrentRate(),request.getMonthes2Return()));
			this.save(request);
		}
	}

	@Override
	public void bid(Long bidRequestId, BigDecimal amount) {
		//1 根据bidRequestId 查询bidRequest对象, 判断是否为null, 是否处于招标中的状态
		//2 投标金额 > 最小投标金额
		//3 投标金额  < = min(用户可用金额, 该标还需要的金额)
		//4 判断当前投资的用户不是借款人
		BidRequest bidRequest = bidRequestMapper.selectByPrimaryKey(bidRequestId);
		Account bidUserAccount = accountService.getCurrent();//投资人账户
		ExpAccount expAccount = expAccountService.get(bidUserAccount.getId());//投资人体验标账户
		//判断分为信用标投资和 体验标投资分别满足的条件
		if(bidRequest!=null && bidRequest.getBidRequestState() == BidConst.BIDREQUEST_STATE_BIDDING &&
				amount.compareTo(bidRequest.getMinBidAmount()) >=0 &&
				((bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL &&
				amount.compareTo(bidUserAccount.getUsableAmount().min(bidRequest.getRemainAmount())) <=0 &&
				!UserContext.getCurrent().getId().equals(bidRequest.getCreateUser().getId()))
				||
				(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_EXP && //体验标
				amount.compareTo(expAccount.getUsableAmount()
						.min(bidRequest.getRemainAmount())) <=0))//体验标投标金额 <=min(体验标账户金额, 体验标还需的金额)
				){
			//对于借款对象 , BidCount 投资数量 +1 , 投资金额 + amount
			bidRequest.setBidCount(bidRequest.getBidCount()+1);
			bidRequest.setCurrentSum(bidRequest.getCurrentSum().add(amount));
			//对于投标对象 创建投标对象id
			Bid bid = new Bid();
			bid.setActualRate(bidRequest.getCurrentRate());
			bid.setAvailableAmount(amount);
			bid.setBidRequestId(bidRequestId);
			bid.setBidRequestState(bidRequest.getBidRequestState());
			bid.setBidTime(new Date());
			bid.setBidRequestTitle(bidRequest.getTitle());
			bid.setBidUser(UserContext.getCurrent());
			bidService.save(bid);
			//对于投资人  判断投标类型
			if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
				//可用金额减少, 冻结金额增加, 生成投标冻结的流水
				bidUserAccount.setUsableAmount(bidUserAccount.getUsableAmount().subtract(amount));
				bidUserAccount.setFreezedAmount(bidUserAccount.getFreezedAmount().add(amount));
				accountService.updateByPrimaryKey(bidUserAccount);
				accountFlowService.creatBidFlow(bidUserAccount,amount);
			}else if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_EXP){
				//用户体验金金额减少, 冻结金额增加, 生成体验金流水
				expAccount.setUsableAmount(expAccount.getUsableAmount().subtract(amount));
				expAccount.setFreezedAmount(expAccount.getFreezedAmount().add(amount));
				expAccountService.update(expAccount);
				expAccountFlowService.createBidFlow(expAccount,amount);
			}
			//判断是否已投满
			if(bidRequest.getCurrentSum().compareTo(bidRequest.getBidRequestAmount()) ==0){
				//判断投标类型  信用标: 借款对象状态设置为满标一审  体验标:状态设置为满标二审
				int state = bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL
						? BidConst.BIDREQUEST_STATE_APPROVE_PENDING_1 : BidConst.BIDREQUEST_STATE_APPROVE_PENDING_2;
				bidRequest.setBidRequestState(state);
				bidService.updateState(bidRequestId,state);
			}
			this.update(bidRequest);
		}

	}
	//满标一审
	@Override
	public void fullAudit1(Long id, int state, String remark) {
		//1根据id获取借款对象, 判断是否为满标一审的状态
		BidRequest bidRequest = this.get(id);
		if(bidRequest!=null && bidRequest.getBidRequestState()==BidConst.BIDREQUEST_STATE_APPROVE_PENDING_1){
			//2 创建审核历史对象 设置相关属性
			creatBidRequestAuditHistory(bidRequest,remark,state,BidRequestAuditHistory.FULL_AUDIT_1);
			//判断审核通过
			if(state == BidRequestAuditHistory.STATE_PASS){
				//进入满标二审的状态, 修改借款状态, 标的状态
				bidRequest.setBidRequestState(BidConst.BIDREQUEST_STATE_APPROVE_PENDING_2);
				bidService.updateState(bidRequest.getId(),BidConst.BIDREQUEST_STATE_APPROVE_PENDING_2);
			}else {
				auditReject(bidRequest);
			}
			this.update(bidRequest);
		}
	}
	//满标二审
	@Override
	public void fullAudit2(Long id, int state, String remark) {
		//判断借款状态
		BidRequest bidRequest = bidRequestMapper.selectByPrimaryKey(id);
		if(bidRequest!=null && bidRequest.getBidRequestState()==BidConst.BIDREQUEST_STATE_APPROVE_PENDING_2){
			//创建审核历史对象,设置相关属性
			creatBidRequestAuditHistory(bidRequest,remark,state,BidConst.BIDREQUEST_STATE_APPROVE_PENDING_2);
			//审核通过
			if(state == BidRequestAuditHistory.STATE_PASS){
				//1 借款人状态和投标的状态(还款中)
				bidRequest.setBidRequestState(BidConst.BIDREQUEST_STATE_PAYING_BACK);
				bidService.updateState(id,BidConst.BIDREQUEST_STATE_PAYING_BACK );
				//2 借款人
				//判断投标类型 信用标借款人的变化
				if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
					//2.1 借款人账号可用金额增加, 生成借款成功流水
					Account creatUserAccount = accountService.selectByPrimaryKey(bidRequest.getCreateUser().getId());
					creatUserAccount.setUsableAmount(creatUserAccount.getUsableAmount().add(bidRequest.getBidRequestAmount()));
					accountFlowService.creatBorrowSuccessFlow(creatUserAccount,bidRequest.getBidRequestAmount());
					//2.2 借款人的待还本息增加 加上这次借款金额和利息
					creatUserAccount.setUnReturnAmount(creatUserAccount.getUnReturnAmount()
							.add(bidRequest.getBidRequestAmount().add(bidRequest.getTotalRewardAmount())));
					//2.3 借款人授信额度减少
					creatUserAccount.setRemainBorrowLimit(creatUserAccount.getRemainBorrowLimit().subtract(bidRequest.getBidRequestAmount()));
					//2.4 移除借款人的正在申请借贷的状态码
					Userinfo userinfo = userinfoService.selectByPrimaryKey(bidRequest.getCreateUser().getId());
					userinfo.removeState(BitStatesUtils.HAS_BIDREQUEST_PROCESS);
					userinfoService.updateByPrimaryKey(userinfo);
					//2.5 支付平台手续费, 可用金额减少, 生成支付平台手续费的流水
					BigDecimal managementCharge = CalculatetUtil.calAccountManagementCharge(bidRequest.getBidRequestAmount());
					creatUserAccount.setUsableAmount(creatUserAccount.getUsableAmount().subtract(managementCharge));
					accountService.updateByPrimaryKey(creatUserAccount);
					accountFlowService.managementChargeFlow(creatUserAccount,managementCharge);
					//2.6 系统账户收取用户的借款手续费,系统账户可用金额增加, 生成收费流水
					SystemAccount systemAccount = systemAccountService.selectCurrent();
					systemAccount.setUsableAmount(managementCharge);
					systemAccountFlowService.creatReceiveManagementCharge(systemAccount,managementCharge);
					systemAccountService.update(systemAccount);
				}

				//4 还款(根据借款期数)
				//5 还款明细(根据投资人个数定的)
				List<PaymentSchedule> 	paymentSchedules = creatPayScheduleList(bidRequest);
				//3 投资人
				Map<Long,Account> accountMap = new HashMap<Long,Account>();
				Map<Long,ExpAccount> expAccountMap = new HashMap<Long,ExpAccount>();
				List<Bid> bids = bidRequest.getBids();
				Account bidUserAccount;
				Long bidUserId;
				//3.1 遍历投标集合, 获取每一个投资人的账户
				for (Bid bid : bids) {
					bidUserId = bid.getBidUser().getId();
					if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
						bidUserAccount = accountMap.get(bidUserId);
						if (bidUserAccount == null) {
							bidUserAccount = accountService.selectByPrimaryKey(bidUserId);
							accountMap.put(bidUserId, bidUserAccount);
						}
						//3.2 投资人账户冻结金额减少,待收本金, 待收利息增加, 生成投标成功流水
						bidUserAccount.setFreezedAmount(bidUserAccount.getFreezedAmount().subtract(bid.getAvailableAmount()));
						accountFlowService.creatBidSuccessFlow(bidUserAccount, bid.getAvailableAmount());
					}else {
						ExpAccount bidExpAccount = expAccountMap.get(bidUserId);
						bidUserAccount = accountMap.get(bidUserId);
						if (bidUserAccount == null) {
							bidUserAccount = accountService.selectByPrimaryKey(bidUserId);
							accountMap.put(bidUserId, bidUserAccount);
							bidExpAccount = expAccountService.get(bidUserId);
							expAccountMap.put(bidUserId,bidExpAccount);
						}
						bidExpAccount.setFreezedAmount(bidExpAccount.getFreezedAmount().subtract(bid.getAvailableAmount()));
						expAccountFlowService.createBidSuccessFlow(bidExpAccount,bid.getAvailableAmount());
					}
				}
					//待收利息增加(根据还款明细求和)
					for (PaymentSchedule ps : paymentSchedules) {
						for(PaymentScheduleDetail psd : ps.getDetails()){
							//找到对应的用户,添加上该还款明细的本金和利息
							bidUserAccount = accountMap.get(psd.getInvestorId());
							bidUserAccount.setUnReceiveInterest(bidUserAccount.getUnReceiveInterest().add(psd.getInterest()));
							if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
								bidUserAccount.setUnReceivePrincipal(bidUserAccount.getUnReceivePrincipal().add(psd.getPrincipal()));
							}
						}
					}
					//统一更新用户Account
					for(Account account:accountMap.values()){
						accountService.updateByPrimaryKey(account);
					}
					if( expAccountMap.values().size() > 0){
						for (ExpAccount expAccount : expAccountMap.values()){
							expAccountService.update(expAccount);
						}
					}
			}else{
				//审核拒绝
				//借款状态和投标状态
				//借款人
				//投资人
				auditReject(bidRequest);
			}
			//更新借款状态
			this.update(bidRequest);
		}
	}
	//还款
	private List<PaymentSchedule> creatPayScheduleList(BidRequest bidRequest) {
		List<PaymentSchedule> 	paymentSchedules = new ArrayList<PaymentSchedule>();
		PaymentSchedule paymentSchedule;
		BigDecimal principalTemp = BigDecimal.ZERO; //n -1 期的本金总和
		BigDecimal interestTemp = BigDecimal.ZERO; // n - 1 期的利息总和
		for(int i = 0; i < bidRequest.getMonthes2Return(); i ++){
			paymentSchedule = new PaymentSchedule();
			paymentSchedule.setBidRequestId(bidRequest.getId());
			paymentSchedule.setBidRequestTitle(bidRequest.getTitle());
			paymentSchedule.setBidRequestType(bidRequest.getBidRequestType());
			paymentSchedule.setBorrowUser(bidRequest.getCreateUser());
			paymentSchedule.setDeadLine(DateUtils.addMonths(bidRequest.getPublishTime(),i+1));//还款时间: 标的发布时间 + 按月分期的第几个月
			paymentSchedule.setMonthIndex(i+1);
			paymentSchedule.setReturnType(bidRequest.getReturnType());
			//判断是否为最后一期还款
			if(i<bidRequest.getMonthes2Return() -1){
				//还款金额 计算出来的金额
				paymentSchedule.setTotalAmount(CalculatetUtil.calMonthToReturnMoney(bidRequest.getReturnType(),
						bidRequest.getBidRequestAmount(),bidRequest.getCurrentRate(),i+1,bidRequest.getMonthes2Return()));
				//利息
				paymentSchedule.setInterest(CalculatetUtil.calMonthlyInterest(bidRequest.getReturnType()
						, bidRequest.getBidRequestAmount(),bidRequest.getCurrentRate(),i+1,bidRequest.getMonthes2Return()));
				//本金 = 该期还款金额 - 改期利息
				paymentSchedule.setPrincipal(paymentSchedule.getTotalAmount().subtract(paymentSchedule.getInterest()));
				principalTemp = principalTemp.add(paymentSchedule.getPrincipal());
				interestTemp = interestTemp.add(paymentSchedule.getInterest());
			}else{
				//最后一期
				//利息 = 借款总利息 - 前N-1的利息之和
				paymentSchedule.setInterest(bidRequest.getTotalRewardAmount().subtract(interestTemp));
				//本金 = 借款总金额 - 前N-1 的本金之和
				paymentSchedule.setPrincipal(bidRequest.getBidRequestAmount().subtract(principalTemp));
				//还款金额 = 利息 + 本金
				paymentSchedule.setTotalAmount(paymentSchedule.getInterest().add(paymentSchedule.getPrincipal()));
			}
			//创建还款明细对象
			paymentScheduleService.save(paymentSchedule);
			creatPayScheduleDetail(paymentSchedule,bidRequest);
			paymentSchedules.add(paymentSchedule);
		}
		return paymentSchedules;
	}
	//创建还款明细对象
	private void creatPayScheduleDetail(PaymentSchedule paymentSchedule, BidRequest bidRequest) {
		PaymentScheduleDetail psd;
		Bid bid;
		BigDecimal principalTemp = BigDecimal.ZERO; //n -1 期的本金总和
		BigDecimal interestTemp = BigDecimal.ZERO; // n - 1 期的利息总和
		for(int i = 0; i < bidRequest.getBids().size(); i ++) {
			bid = bidRequest.getBids().get(i);
			psd = new PaymentScheduleDetail();
			psd.setBidAmount(bid.getAvailableAmount());
			psd.setBidId(bid.getId());
			psd.setBidRequestId(bidRequest.getId());
			psd.setBorrowUser(bidRequest.getCreateUser());
			psd.setDeadLine(paymentSchedule.getDeadLine());
			psd.setInvestorId(bid.getBidUser().getId());
			psd.setMonthIndex(paymentSchedule.getMonthIndex());
			psd.setPaymentScheduleId(paymentSchedule.getId());
			psd.setReturnType(bidRequest.getReturnType());
			if (i < bidRequest.getBids().size() - 1) {
				//不是最后一个投资人
				//对应投资人投资比例
				BigDecimal rate = bid.getAvailableAmount().divide(bidRequest.getBidRequestAmount(),
						BidConst.CAL_SCALE, RoundingMode.HALF_UP);
				//该还款明细对象的利息 = 投资比例 * 该期的利息  保留4位小数, 因为保存到数据库中也是4位小数
				//psd.setInterest(rate.multiply(paymentSchedule.getInterest()));
				psd.setInterest(DecimalFormatUtil.formatBigDecimal(paymentSchedule.getInterest().multiply(rate),BidConst.STORE_SCALE));
				//该还款明细对象的本金 = 投资比例 * 该期的本金
				//psd.setPrincipal(rate.multiply(paymentSchedule.getPrincipal()));
				psd.setPrincipal(DecimalFormatUtil.formatBigDecimal(paymentSchedule.getPrincipal().multiply(rate),BidConst.STORE_SCALE));
				//该还款明细对象的还款金额 = 该还款明细对象的利息 + 该还款明细对象的本金
				psd.setTotalAmount(psd.getInterest().add(psd.getPrincipal()));
				principalTemp = principalTemp.add(psd.getPrincipal());
				interestTemp = interestTemp.add(psd.getInterest());
			}else{
				//最后一个投资人
				//该还款明细对象的利息 = 该还款对象的利息 - 前n-1个投资人利息之和
				psd.setInterest(paymentSchedule.getInterest().subtract(interestTemp));
				//该还款明细对象的本金 = 该还款对象的本金 - 前n-1个投资人本金之和
				psd.setPrincipal(paymentSchedule.getPrincipal().subtract(principalTemp));
				//该还款明细对象的还款金额 = 该还款明细对象的利息 + 该还款明细对象的本金
				psd.setTotalAmount(psd.getInterest().add(psd.getPrincipal()));
			}
			paymentScheduleDetailService.save(psd);
			paymentSchedule.getDetails().add(psd);//把明细对象设置到还款对象中
		}

	}

	//创建审核历史对象的方法
	private void creatBidRequestAuditHistory(BidRequest bidRequest,String remark,int state, int actionType){
		BidRequestAuditHistory brah = new BidRequestAuditHistory();
		brah.setApplier(bidRequest.getCreateUser());
		brah.setApplyTime(new Date());
		brah.setAuditor(UserContext.getCurrent());
		brah.setAuditTime(new Date());
		brah.setBidRequestId(bidRequest.getId());
		brah.setRemark(remark);
		brah.setAuditType(actionType);
		if(state == BidRequestAuditHistory.STATE_PASS){
			brah.setState(BidRequestAuditHistory.STATE_PASS);
		}else {
			brah.setState(BidRequestAuditHistory.STATE_REJECT);
		}
		bidRequestAuditHistoryService.save(brah);
	}

	//抽取审核拒绝的操作
	private void  auditReject(BidRequest bidRequest){
		//判断审核拒绝
		//进入满标审核拒绝状态,修改借款状态, 标的状态
		bidRequest.setBidRequestState(BidConst.BIDREQUEST_STATE_PUBLISH_REFUSE);
		bidService.updateState(bidRequest.getId(),BidConst.BIDREQUEST_STATE_PUBLISH_REFUSE);
		//获取所有投资人账号, 冻结金额减少, 可用金额增加, 生成流标流水
		List<Bid> bids = bidRequest.getBids();
		Map<Long,Account> accountMap = new HashMap<Long,Account>();
		Account bidUserAcount;
		Long bidUserId;
		for (Bid bid : bids) {
			bidUserId = bid.getBidUser().getId();
			//判断投标类型
			if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
				bidUserAcount = accountMap.get(bidUserId);
				if(bidUserAcount == null){
					bidUserAcount = accountService.selectByPrimaryKey(bidUserId);
					accountMap.put(bidUserId,bidUserAcount);
				}
				bidUserAcount.setFreezedAmount(bidUserAcount.getFreezedAmount().subtract(bid.getAvailableAmount()));//当前冻结的金额减去投标金额
				bidUserAcount.setUsableAmount(bidUserAcount.getUsableAmount().add(bid.getAvailableAmount()));
				accountFlowService.creatBidFailedFlow(bidUserAcount,bid.getAvailableAmount());
			}else {
				//体验标 金额增加, 冻结金额减少
				ExpAccount expAccount = expAccountService.get(bidUserId);
				expAccount.setUsableAmount(expAccount.getUsableAmount().add(bid.getAvailableAmount()));
				expAccount.setFreezedAmount(expAccount.getFreezedAmount().subtract(bid.getAvailableAmount()));
				expAccountService.update(expAccount);//懒得用Map封装, 直接单条更新
				expAccountFlowService.createBidFailFlow(expAccount,bid.getAvailableAmount());
			}
		}
		if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
			for(Account account : accountMap.values()){
				accountService.updateByPrimaryKey(account);
			}
			//取消借款人正在借款的状态码
			Userinfo bidUserUserinfo = userinfoService.selectByPrimaryKey(bidRequest.getCreateUser().getId());
			bidUserUserinfo.removeState(BitStatesUtils.HAS_BIDREQUEST_PROCESS);
			userinfoService.updateByPrimaryKey(bidUserUserinfo);
		}
	}

}
