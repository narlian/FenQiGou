package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.AccountFlow;

import java.util.List;

public interface AccountFlowMapper {
    int insert(AccountFlow record);
    AccountFlow selectByPrimaryKey(Long id);
	Long queryPageCount(QueryObject qo);
	List<AccountFlow> queryPageData(QueryObject qo);
}