package com.xmg.wechat.business.domain;

import com.xmg.wechat.base.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 体验金账户
 * 
 * @author stef
 *
 */
@Setter
@Getter
public class ExpAccount extends BaseDomain {

	private int version;
	private BigDecimal usableAmount = BigDecimal.ZERO;// 体验金账户余额
	private BigDecimal freezedAmount = BigDecimal.ZERO;// 体验金冻结金额
	private BigDecimal unReturnExpAmount = BigDecimal.ZERO;// 临时垫收体验金

}
