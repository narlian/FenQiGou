package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.BidRequestAuditHistory;

import java.util.List;

public interface BidRequestAuditHistoryMapper {
    int insert(BidRequestAuditHistory record);
    BidRequestAuditHistory selectByPrimaryKey(Long id);
	Long queryPageCount(QueryObject qo);
	List<BidRequestAuditHistory> queryPageData(QueryObject qo);

    List<BidRequestAuditHistory> getListByBidRequestId(Long bidRequestId);
}