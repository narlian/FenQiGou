package com.xmg.wechat.business.domain;

import com.alibaba.fastjson.JSON;
import com.xmg.wechat.base.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter@Setter
public class WeChatMenu extends BaseDomain {

    private String name;
    private String type;
    private String key;
    private String url;

    //回显编辑页面的数据
    public String getJsonStr(){
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("id",id);
        param.put("name",name);
        param.put("type",type);
        param.put("key",key);
        param.put("url",url);
        return JSON.toJSONString(param);
    }
}
