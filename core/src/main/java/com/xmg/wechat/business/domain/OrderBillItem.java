package com.xmg.wechat.business.domain;

import com.xmg.wechat.base.domain.BaseDomain;
import com.xmg.wechat.base.domain.Product;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter@Setter
public class OrderBillItem extends BaseDomain {
    private BigDecimal price;  //商品价格
    private Integer number;  //商品数量
    private BigDecimal amount;  //单条商品明细的金额 小计
    private Product product;    //商品
    private Long orderBillId;//关联的订单id, 不必用对象封装
    private Long buyerId; //添加商品到购物车时 记录该用户的id

    //计算商品小计
    public BigDecimal getAmount(){
        return price.multiply(new BigDecimal(number));
    }
}
