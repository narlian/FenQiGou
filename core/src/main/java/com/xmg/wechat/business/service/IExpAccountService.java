package com.xmg.wechat.business.service;

import com.xmg.wechat.business.domain.ExpAccount;
import lombok.Getter;
import org.apache.commons.lang3.time.DateUtils;

import java.math.BigDecimal;
import java.util.Date;

public interface IExpAccountService {
    int save(ExpAccount expAccount);
    int update(ExpAccount expAccount);
    ExpAccount get(Long id );

    /**
     * 发放注册体验金
     * @param id  体验金账户id/登录用户id
     * @param lastTime 有效期
     * @param grantMoney 体验金金额
     * @param expmoneyType 体验金类型
     */
    void grantExpMoney(Long id, LastTime lastTime, BigDecimal grantMoney, int expmoneyType);

    /**
     * 获取当前用户的体验金账号
     * @return
     */
    ExpAccount getCurrent();

    /**
     * 有效期 静态类
     */
        @Getter
    class LastTime{
        private  int amount;//持续时间
        private  LastTimeUnit unit;//持续时间单位

        public LastTime(int amount, LastTimeUnit unit) {
            this.amount = amount;
            this.unit = unit;
        }

        public Date getReturnDate(Date date) {
            switch (this.unit){
                case DAY:return DateUtils.addDays(date,this.amount);
                case MONTH:return DateUtils.addMonths(date,this.amount);
                case YEAR:return DateUtils.addYears(date,this.amount);
                default:return date;
            }
        }
    }

    /**
     * 有效期持续时间单位
     */
    enum LastTimeUnit{
            DAY,MONTH,YEAR
    }
}
