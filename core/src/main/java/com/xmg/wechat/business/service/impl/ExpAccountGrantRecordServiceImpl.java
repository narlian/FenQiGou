package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.business.domain.ExpAccountGrantRecord;
import com.xmg.wechat.business.mapper.ExpAccountGrantRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.business.service.IExpAccountGrantRecordService;

@Service
public class ExpAccountGrantRecordServiceImpl implements IExpAccountGrantRecordService {
	@Autowired
	private ExpAccountGrantRecordMapper expAccountGrantRecordMapper;

	@Override
	public void save(ExpAccountGrantRecord record) {
		expAccountGrantRecordMapper.insert(record);
	}
}
