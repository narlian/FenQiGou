package com.xmg.wechat.business.service;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.OrderBillItem;
import com.xmg.wechat.business.query.OrderBillItemQueryObject;

import java.util.List;

public interface IOrderBillItemService {
    int save(OrderBillItem orderBillItem);
    int update(OrderBillItem orderBillItem);

    PageResult queryPage(QueryObject qo);

    void add(Long id);

    /**
     * 查看购物车
     * @return
     */
    List<OrderBillItem> queryList();

    void normalOrder();

    /**
     * 根据订单id查询订单明细
     * @param orderBillId
     * @return
     */
    List<OrderBillItem> selectItemsByOrderBillId(Long orderBillId);

    /**
     * 减少购物车中的商品
     * @param id
     */
    void subItem(Long id);

    /**
     * 删除购物车中的商品
     * @param id
     */
    void deleteItem(Long id);

    /**
     * 往购物车中添加商品
     * @param id
     */
    void addItem(Long itemId);

    /**
     * 根据订单id查询当前用户的普通订单明细
     * @param qo
     * @return
     */
    List<OrderBillItem> queryListByORderBillId(OrderBillItemQueryObject qo);
}

