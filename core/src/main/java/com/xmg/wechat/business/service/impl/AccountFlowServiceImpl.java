package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.business.domain.AccountFlow;
import com.xmg.wechat.business.mapper.AccountFlowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xmg.wechat.business.service.IAccountFlowService;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class AccountFlowServiceImpl implements IAccountFlowService {
	@Autowired
	private AccountFlowMapper accountFlowMapper;

	@Override
	public int save(AccountFlow accountFlow) {

		return accountFlowMapper.insert(accountFlow);
	}

	@Override
	public void createRechargeFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount, BidConst.ACCOUNT_ACTIONTYPE_RECHARGE_OFFLINE,"线下充值" + amount +"元");

	}

	@Override
	public void creatBidFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_WITHDRAW_FREEZED,"投标冻结" + amount +"元");
	}

	@Override
	public void creatBidFailedFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_WITHDRAW_UNFREEZED,"流标回退" + amount +"元");
	}

	@Override
	public void creatBorrowSuccessFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_BIDREQUEST_SUCCESSFUL,"借款成功,可用金额增加" + amount +"元");
	}

	@Override
	public void managementChargeFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_CHARGE,"缴纳平台管理费,可用金额减少" + amount +"元");
	}

	@Override
	public void creatBidSuccessFlow(Account account, BigDecimal amount) {
		//creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_BID_SUCCESSFUL,"投标成功,冻结金额减少" + amount +"元");
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_BID_SUCCESSFUL,"下单成功,可用金额减少" + amount +"元");
	}

	@Override
	public void creatReturnMoneyFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_RETURN_MONEY,"还款成功,可用金额减少" + amount +"元");
	}

	@Override
	public void creatGainReturnMoneyFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_CALLBACK_MONEY,"收到回款,可用金额增加" + amount +"元");
	}

	@Override
	public void creatInterestManageChargeFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_INTEREST_SHARE,"支付利息管理费,可用金额减少" + amount +"元");
	}

	@Override
	public void createReceiveInterestFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_CALLBACK_INTEREST_MONEY,"体验金利息回款,可用金额增加" + amount +"元");
	}

	@Override
	public void createReturnMoneyFlow(Account account, BigDecimal amount) {
		creatFlow(account,amount,BidConst.ACCOUNT_ACTIONTYPE_RETURN_MONEY,"本期还款成功，还款金额" + amount +"元，可用金额减少"+ amount +"元");
	}

	private void creatFlow(Account account, BigDecimal amount, int actionType, String remark) {
		AccountFlow flow =new AccountFlow();
		flow.setAccountId(account.getId());
		flow.setActionType(actionType);
		flow.setUsableAmount(account.getUsableAmount());
		flow.setAmount(amount);
		flow.setFreezedAmount(account.getFreezedAmount());
		flow.setTradeTime(new Date());
		flow.setRemark(remark);
		this.save(flow);
	}

}
