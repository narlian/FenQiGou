package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.SystemAccountFlow;

import java.util.List;

public interface SystemAccountFlowMapper {
    int insert(SystemAccountFlow record);
	Long queryPageCount(QueryObject qo);
	List<SystemAccountFlow> queryPageData(QueryObject qo);
}