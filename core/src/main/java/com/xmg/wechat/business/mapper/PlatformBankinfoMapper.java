package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.PlatformBankinfo;

import java.util.List;

public interface PlatformBankinfoMapper {
    int insert(PlatformBankinfo record);
    PlatformBankinfo selectByPrimaryKey(Long id);
    int updateByPrimaryKey(PlatformBankinfo record);
	Long queryPageCount(QueryObject qo);
	List<PlatformBankinfo> queryPageData(QueryObject qo);

    List<PlatformBankinfo> selectAll();
}