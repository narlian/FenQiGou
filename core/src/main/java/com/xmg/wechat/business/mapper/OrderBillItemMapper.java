package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.OrderBillItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderBillItemMapper {
    int insert(OrderBillItem record);
    OrderBillItem selectByPrimaryKey(Long id);
    int updateByPrimaryKey(OrderBillItem record);

    List<OrderBillItem> selectByOrderBillId(Long orderBillId);

	Long queryPageCount(QueryObject qo);
	List<OrderBillItem> queryPageData(QueryObject qo);

    OrderBillItem selectByProductId(@Param("productId") Long productId, @Param("currentId")Long currentId);

    List<OrderBillItem> getItemByBuyerId(Long id);

    void subItemById(Long itemId);

    void deleteItem(Long itemId);
}