package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.business.domain.SystemAccount;
import com.xmg.wechat.business.domain.SystemAccountFlow;
import com.xmg.wechat.business.mapper.SystemAccountFlowMapper;
import com.xmg.wechat.business.service.ISystemAccountFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class SystemAccountFlowServiceImpl implements ISystemAccountFlowService {
	@Autowired
	private SystemAccountFlowMapper systemAccountFlowMapper;

	@Override
	public int save(SystemAccountFlow systemAccountFlow) {
		return systemAccountFlowMapper.insert(systemAccountFlow);
	}

	@Override
	public void creatReceiveManagementCharge(SystemAccount account, BigDecimal amount) {
		crestFlow(account,amount, BidConst.SYSTEM_ACCOUNT_ACTIONTYPE_MANAGE_CHARGE,"收到账户管理费" + amount +"元");
	}

	@Override
	public void creatReceiveInterestManageChargeFlow(SystemAccount account, BigDecimal amount) {
		crestFlow(account,amount, BidConst.SYSTEM_ACCOUNT_ACTIONTYPE_INTREST_MANAGE_CHARGE,"收到信息管理费" + amount +"元");
	}

	@Override
	public void createPayInterest(SystemAccount account, BigDecimal amount) {
		crestFlow(account,amount, BidConst.SYSTEM_ACCOUNT_PAY_INTEREST,"支付体验标利息" + amount +"元");
	}

	@Override
	public void createReceiveAverageReturnMoneyFlow(SystemAccount account, BigDecimal amount) {
		crestFlow(account,amount, BidConst.SYSTEM_ACCOUNT_ACTIONTYPE_RECEIVE_AVERAGE_RETURNMONEY,"收到还款金额" + amount +"元");
	}

	private void crestFlow(SystemAccount account, BigDecimal amount, int actionType, String remark){
		SystemAccountFlow systemAccountFlow = new SystemAccountFlow();
		systemAccountFlow.setActionTime(new Date());
		systemAccountFlow.setActionType(actionType);
		systemAccountFlow.setAmount(amount);
		systemAccountFlow.setFreezedAmount(account.getFreezedAmount());
		systemAccountFlow.setUsableAmount(account.getUsableAmount());
		systemAccountFlow.setRemark(remark);
		this.save(systemAccountFlow);
	}
}
