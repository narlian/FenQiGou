package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.ExpAccount;
import com.xmg.wechat.business.domain.ExpAccountFlow;
import com.xmg.wechat.business.domain.ExpAccountGrantRecord;
import com.xmg.wechat.business.mapper.ExpAccountMapper;
import com.xmg.wechat.business.service.IExpAccountFlowService;
import com.xmg.wechat.business.service.IExpAccountGrantRecordService;
import com.xmg.wechat.business.service.IExpAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class ExpAccountServiceImpl implements IExpAccountService {
	@Autowired
	private ExpAccountMapper expAccountMapper;
	@Autowired
	private IExpAccountGrantRecordService expAccountGrantRecordService;
	@Autowired
	private IExpAccountFlowService expAccountFlowService;

	@Override
	public int save(ExpAccount expAccount) {
		return expAccountMapper.insert(expAccount);
	}

	@Override
	public int update(ExpAccount expAccount) {
		int count = expAccountMapper.updateByPrimaryKey(expAccount);
		if(count <= 0 ){
			throw  new RuntimeException("乐观锁异常,体验金更新id:"+expAccount.getId());
		}
		return count;
	}

	@Override
	public ExpAccount get(Long id) {
		return expAccountMapper.selectByPrimaryKey(id);
	}

	@Override
	public ExpAccount getCurrent() {
		return expAccountMapper.selectByPrimaryKey(UserContext.getCurrent().getId());
	}

	@Override
	public void grantExpMoney(Long id, LastTime lastTime, BigDecimal grantMoney, int expmoneyType) {
		//创建体验金发放回收记录
		ExpAccountGrantRecord record = new ExpAccountGrantRecord();
		record.setGrantUserId(id);
		record.setAmount(grantMoney);
		record.setGrantDate(new Date());
		record.setGrantType(expmoneyType);
		record.setNote("注册发放体验金");
		record.setReturnDate(lastTime.getReturnDate(new Date()));
		record.setState(ExpAccountGrantRecord.STATE_NORMAL);
		expAccountGrantRecordService.save(record);
		//修改体验金账户
		ExpAccount expAccount = this.get(id );
		expAccount.setUsableAmount(expAccount.getUsableAmount().add(grantMoney));
		this.update(expAccount);
		//添加一条体验金发放流水
		ExpAccountFlow flow = new ExpAccountFlow();
		flow.setActionTime(record.getGrantDate());
		flow.setActionType(expmoneyType);
		flow.setAmount(grantMoney);
		flow.setExpAccountId(id);
		flow.setNote("注册发放体验金");
		flow.setUsableAmount(expAccount.getUsableAmount());
		flow.setFreezedAmount(expAccount.getFreezedAmount());
		expAccountFlowService.save(flow);
	}
}
