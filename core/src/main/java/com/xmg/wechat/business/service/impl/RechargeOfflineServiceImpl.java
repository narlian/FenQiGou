package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.RechargeOffline;
import com.xmg.wechat.business.mapper.RechargeOfflineMapper;
import com.xmg.wechat.business.query.RechargeOfflineQueryObject;
import com.xmg.wechat.business.service.IAccountFlowService;
import com.xmg.wechat.business.service.IRechargeOfflineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RechargeOfflineServiceImpl implements IRechargeOfflineService {
	@Autowired
	private RechargeOfflineMapper rechargeOfflineMapper;
	@Autowired
	private IAccountService accountService;
	@Autowired
	private IAccountFlowService accountFlowService;


	@Override
	public int save(RechargeOffline rechargeOffline) {
		return rechargeOfflineMapper.insert(rechargeOffline);
	}

	@Override
	public void applyCharge(RechargeOffline rechargeOffline) {
		RechargeOffline ro = new RechargeOffline();
		ro.setAmount(rechargeOffline.getAmount());
		ro.setApplier(UserContext.getCurrent());
		ro.setApplyTime(new Date());
		ro.setNote(rechargeOffline.getNote());
		ro.setTradeCode(rechargeOffline.getTradeCode());
		ro.setTradeTime(rechargeOffline.getTradeTime());
		ro.setBankInfo(rechargeOffline.getBankInfo());
		this.save(ro);
	}

	@Override
	public void audit(Long id, int state, String remark) {
		RechargeOffline ro = rechargeOfflineMapper.selectByPrimaryKey(id);
		if(ro!=null &&ro.getState() == RechargeOffline.STATE_NORMAL){
			ro.setAuditor(UserContext.getCurrent());
			ro.setAuditTime(new Date());
			ro.setRemark(remark);
			if(state == RechargeOffline.STATE_PASS){
				ro.setState(RechargeOffline.STATE_PASS);
				Account applierAccount = accountService.selectByPrimaryKey(ro.getApplier().getId());
				applierAccount.setUsableAmount(applierAccount.getUsableAmount().add(ro.getAmount()));
				accountService.updateByPrimaryKey(applierAccount);
				accountFlowService.createRechargeFlow(applierAccount,ro.getAmount());
			}else{
				ro.setState(RechargeOffline.STATE_REJECT);
			}
			rechargeOfflineMapper.updateByPrimaryKey(ro);
		}
	}

	@Override
	public PageResult queryPage(RechargeOfflineQueryObject qo) {
		int count = rechargeOfflineMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<RechargeOffline> items = rechargeOfflineMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());

	}
}
