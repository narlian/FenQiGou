package com.xmg.wechat.business.service;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.business.domain.PaymentSchedule;
import com.xmg.wechat.business.query.PaymentScheduleQueryObject;

import java.util.List;

public interface IPaymentScheduleService {
    int save(PaymentSchedule paymentSchedule);
    int update(PaymentSchedule paymentSchedule);

    PageResult queryPage(PaymentScheduleQueryObject qo);

    void returnMoney(Long id);
    PaymentSchedule get(Long id);

    /**
     * 根据订单id查询还款明细
     * @param
     * @return
     */
    List<PaymentSchedule> selectItemsByOrderBillId(Long orderBillId);

    List<PaymentSchedule> selectByLoginfoId(Long id);

    /**
     * 还款
     * @param id
     */
    void wechatReturnMoney(Long id);

    /**
     * 根据订单号查询对应的还款明细
     * @param orderBillId
     * @return
     */
    List<PaymentSchedule> selectByOrderBillId(String orderBillId);
}
