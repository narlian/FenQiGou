package com.xmg.wechat.business.mapper;

import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.PaymentScheduleDetail;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface PaymentScheduleDetailMapper {
    int insert(PaymentScheduleDetail record);
    PaymentScheduleDetail selectByPrimaryKey(Long id);
    int updateByPrimaryKey(PaymentScheduleDetail record);
	Long queryPageCount(QueryObject qo);
	List<PaymentScheduleDetail> queryPageData(QueryObject qo);
    List<PaymentScheduleDetail> selectByPaymentScheduleId(Long paymentScheduleId);

    void updateState(@Param("id") Long id, @Param("payDate") Date payDate);
}