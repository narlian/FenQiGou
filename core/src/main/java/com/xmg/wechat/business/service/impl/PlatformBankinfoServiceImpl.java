package com.xmg.wechat.business.service.impl;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.QueryObject;
import com.xmg.wechat.business.domain.PlatformBankinfo;
import com.xmg.wechat.business.mapper.PlatformBankinfoMapper;
import com.xmg.wechat.business.service.IPlatformBankinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlatformBankinfoServiceImpl implements IPlatformBankinfoService {
	@Autowired
	private PlatformBankinfoMapper platformBankinfoMapper;

	@Override
	public int save(PlatformBankinfo platformBankinfo) {
		return platformBankinfoMapper.insert(platformBankinfo);
	}

	@Override
	public int update(PlatformBankinfo platformBankinfo) {
		return platformBankinfoMapper.updateByPrimaryKey(platformBankinfo);
	}

	@Override
	public PageResult queryPage(QueryObject qo) {
		int count = platformBankinfoMapper.queryPageCount(qo).intValue();
		if(count <=0 ){
			return PageResult.empty(qo.getPageSize());
		}
		List<PlatformBankinfo> items = platformBankinfoMapper.queryPageData(qo);
		return new PageResult(items,count,qo.getCurrentPage(),qo.getPageSize());
	}

	@Override
	public void saveOrUpdate(PlatformBankinfo platformBankinfo) {
		if(platformBankinfo.getId() == null){
			this.save(platformBankinfo);
		}else{
			this.update(platformBankinfo);
		}
	}

	@Override
	public List<PlatformBankinfo> sellectAll() {
		return platformBankinfoMapper.selectAll();
	}

}
