package com.xmg.wechat.business.service;

import com.xmg.wechat.business.domain.ExpAccount;
import com.xmg.wechat.business.domain.ExpAccountFlow;

import java.math.BigDecimal;

public interface IExpAccountFlowService {
    int save(ExpAccountFlow expAccountFlow);

    void createBidFlow(ExpAccount expAccount, BigDecimal amount);

    void createBidFailFlow(ExpAccount expAccount, BigDecimal amount);

    /**
     * 体验标投标成功流水
     * @param expAccount
     * @param amount
     */
    void createBidSuccessFlow(ExpAccount expAccount, BigDecimal amount);

    void createReceiveFlow(ExpAccount expAccount, BigDecimal amount);
}
