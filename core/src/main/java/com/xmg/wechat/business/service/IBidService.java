package com.xmg.wechat.business.service;
import com.xmg.wechat.business.domain.Bid;

public interface IBidService {
    int save(Bid bid);

    int updateState(Long bidRequestId, int bidrequestStateApprovePending1);
}
