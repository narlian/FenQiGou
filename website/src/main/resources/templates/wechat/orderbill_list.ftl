<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description"
          content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <link rel="stylesheet" href="/css/weui.min.css">
    <link rel="stylesheet" href="/css/jquery-weui.min.css">
    <link rel="stylesheet" href="/css/demos.css">

    <title>订单页面</title>
<#include "../common/links-tpl.ftl" />
    <link type="text/css" rel="stylesheet" href="/css/account.css"/>
    <script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="/js/plugins-override.js"></script>
    <script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>

    <link rel="stylesheet" href="/css/bank.css">
    <script type="text/javascript" src="/js/bank.js"></script>
    <script type="text/javascript">
        $(function () {
           /* //查看按钮
            $(".btn-info").click(function () {
                $(".form-horizontal").submit();
            });*/
            //购物车商品数量
            var productSum;
            $.get("/getProductsum",function (data) {
                $("#productSum").text(data);
            },'json');
            //
        })
    </script>
</head>
<body>

<div class="weui-tab">
    <div class="weui-tab__bd">
        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
            <!-- 功能页面 -->
            <div class="col-sm-9">
                <form action="/borrowBidReturn_list" name="searchForm" id="searchForm" class="form-inline"
                      method="post">
                    <input type="hidden" id="currentPage" name="currentPage" value=""/>
                    <div class="form-group">
                        <label>订单状态</label>
                        <select class="form-control" name="state">
                            <option value="-1">全部</option>
                            <option value="0">待还款</option>
                            <option value="1">已还款</option>
                            <option value="2">逾期</option>
                        </select>
                        <script type="text/javascript">
                            $("[name=state] option[value='${(qo.state)!''}']").attr("selected", "selected");
                        </script>
                    </div>
                </form>

                <div class="panel el-panel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="pull-left" style="line-height: 35px;">我的订单</span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-title">
							<span class="pull-left">
								分期购订单
							</span>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>订单号</th>
                                <th>商品名称</th>
                                <th>总还款金额</th>
                                <th>还款期数</th>
                                <th>订单状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if bidOrders?size &gt; 0 >
                                <#list bidOrders as data>
                                <tr>
                                    <td>${data.id}</td>
                                    <td><a href="#">${data.product.name}</a></td>
                                    <td>${data.totalAmount}元</td>
                                    <td>${data.repaymentNumber}期</td>
                                    <td>${data.stateDisplay}</td>
                                    <td><a class="btn btn-danger btn-sm" href="/wechatReturnMonryList?id=${data.id}">订单明细</a>
                                    </td>
                                </tr>
                                </#list>
                            <#else>
                            <tr>
                                <td colspan="7" align="center">
                                    <p class="text-danger">目前暂时没有分期购记录</p>
                                </td>
                            </tr>
                            </#if>
                            </tbody>
                        </table>
                    </div>

                    <div class="panel el-panel">
                        <div class="panel-title">
							<span class="pull-left">
								普通订单
							</span>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>订单号</th>
                                <th>购买时间</th>
                                <th>订单总价格</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if normalOrders?size &gt; 0 >
                                <#list normalOrders as data>
                                <tr>
                                    <td>${data.id}</td>
                                    <td>${data.buyTime?string("yyyy-MM-dd")}</td>
                                    <td>${data.totalSum}元</td>
                                    <td><a class="btn btn-danger btn-sm"
                                           href="/wcOrderBillItem_list?orderBillId=${data.id}">订单明细</a></td>
                                </tr>
                                </#list>
                            <#else>
                            <tr>
                                <td colspan="7" align="center">
                                    <p class="text-danger">目前暂时没有购买记录</p>
                                </td>
                            </tr>
                            </#if>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
<#--weUI底部导航-->
    <#include "../common/tabbar-tpl.ftl"/>
</div>
<!-- body 最后 -->
<script src="/js/jquery.min.js"></script>
<script src="/js/fastclick.js"></script>
<script>
    $(function () {
        FastClick.attach(document.body);
    });
</script>
<script src="/js/jquery-weui.min.js"></script>
</body>
</html>