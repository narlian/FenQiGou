<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <link rel="stylesheet" href="/css/weui.min.css">
    <link rel="stylesheet" href="/css/jquery-weui.min.css">
    <link rel="stylesheet" href="/css/demos.css">
    <title>还款明细页面</title>

<#include "../common/links-tpl.ftl" />
    <link type="text/css" rel="stylesheet" href="/css/account.css"/>
    <script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="/js/plugins-override.js"></script>
    <script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript">
        $(function () {

            $(".return_money").click(function () {
                $.ajax({
                    dataType:"json",
                    type:"POST",
                    url:"/wechatReturnMoney",
                    data:{id:$(this).data("rid")},
                    success:function(data){
                        if(data.success){
                            $.alert("还款成功",function(){
                                window.location.reload();
                            });
                        }else{
                            $.alert(data.msg);
                        }
                    }
                });
            });
            //购物车商品数量
            var productSum;
            $.get("/getProductsum",function (data) {
                $("#productSum").text(data);
            },'json');
        })
    </script>
</head>
<body>

<div class="weui-tab">
    <div class="weui-tab__bd">
        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
            <h3>还款列表</h3>
            <div>
                <table class="table">
                    <ul class="thumbnails">
                        <tr>
                            <td>还款日期</td>
                            <td>本期还款截止日期</td>
                            <td>本期还款金额</td>
                            <td>第几期</td>
                            <td>本期还款状态</td>
                        </tr>
                    <#list paymentSchedules as paymentSchedule>

                        <tr>
                            <td>${paymentSchedule.payDate?string('yyyy-MM-dd')}</td>
                            <td>${paymentSchedule.deadLine?string('yyyy-MM-dd')}</td>
                            <td>${paymentSchedule.totalAmount}</td>
                            <td>${paymentSchedule.monthIndex}</td>
                            <td>
                                <#if paymentSchedule.state=1>
                                    <a class="btn btn-success return_money" data-returnmoney="${paymentSchedule.totalAmount?string('0.##')}" data-rid="${paymentSchedule.id}">立刻还款</a>
                                <#else>
                                ${paymentSchedule.stateDis}
                                </#if>
                            </td>
                        </tr>
                    </#list>
                    </ul>
                </table>
            </div>
        </div>
    </div>


    <#--weUI底部导航-->
    <#include "../common/tabbar-tpl.ftl"/>
</div>

<!-- body 最后 -->
<script src="/js/jquery.min.js"></script>
<script src="/js/fastclick.js"></script>
<script>
    $(function() {
        FastClick.attach(document.body);
    });
</script>
<script src="/js/jquery-weui.min.js"></script>
</body>
</html>