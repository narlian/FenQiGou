<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
        <link rel="stylesheet" href="/css/demos.css">
        <title>购物车</title>
		<#include "../common/links-tpl.ftl" />
		<link type="text/css" rel="stylesheet" href="/css/account.css" />
		<link rel="stylesheet" href="/css/bank.css">

        <link rel="stylesheet" href="/css/weui.min.css">
        <link rel="stylesheet" href="/css/jquery-weui.min.css">

        <script src="/js/jquery/jquery.min.js"></script>
        <script src="/js/jquery/jquery-weui.min.js"></script>
        <script src="/js/jquery/swiper.min.js"></script>
        <script type="text/javascript" src="/js/city-picker.js" charset="utf-8"></script>

		<script type="text/javascript">
            $(function () {
                $("#normalOrder").click(function () {
                    $.login({
                        title: '支付订单',
                        text: '确认支付',
                        username: '招商银行信用卡',  // 默认用户名
                        password: '',  // 默认密码
                        onOK: function (username, password) {
                            //点击确认
                            $.get("/normalOrder", function (data) {
                                if (data.success) {
                                    $.confirm("支付成功,回到首页", function () {
                                        window.location.href = "/product_list";
                                    });
                                } else {
                                    $.alert(data.msg);
                                }
                            }, 'json')
                        },
                        onCancel: function () {
                            //点击取消
                        }
                    });
                });
                var productSum = $("input[name=totalnumber]").val();
                 $("#productSum").text(productSum);

                //收获地址
                    $("#city-picker").cityPicker({
                        title: "请选择收货地址"
                    });

                    $("#addressBt").click(function () {
                        $.ajax({
                            dataType:"json",
                            type: "POST",
                            url:"/allPayment",
                            data:$('#addressForm').serialize(),// 你的formid
                            success:function (data) {
                                if(data.success){
                                    $.alert(data.msg);
                                    location.href = "/product_list";
                                }else {
                                    $.alert(data.msg);
                                }
                            }
                        })
                    })
                });
            //函数
            function subItemNum(itemId){
                $.get("/subOrderBillItem?id="+itemId, function (data) {
                    if(data.success){
                        window.location.reload();
                    }
                },'json');
            }
            function addItemNum(itemId){
                $.get("/addOrderBillItem?id="+itemId, function (data) {
                    if(data.success){
                        window.location.reload();
                    }
                },'json');
            }
            function delItemNum(itemId){
                $.get("/deleteOrderBillItem?id="+itemId, function (data) {
                    if(data.success){
                        window.location.reload();
                    }
                },'json');
            }

	 </script>
	</head>
	<body>

        <div class="weui-tab">
            <div class="weui-tab__bd">
                <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
                    <div class="container">
                        <div class="row">
                            <!-- 功能页面 -->
                            <div class="col-sm-9">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span class="pull-left" style="line-height: 35px;">购物车</span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>商品名称</th>
                                            <th>商品价格</th>
                                            <th>商品数量</th>
                                            <th>小计</th>
                                            <th>操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>
										<#list orderBillItems as data>
                                        <tr>
                                            <td><a href="#">${data.product.name}</a></td>
                                            <td>${data.price}元</td>
                                            <td>
                                                <a href="javascript:subItemNum(${data.id})" ><span class="glyphicon glyphicon-minus" aria-hidden="true"/></a>
                                            ${data.number}
                                                <a href="javascript:addItemNum(${data.id})"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></font></a>
                                            </td>
                                            <td>${data.amount}元</td>
                                            <td>
                                                <a href="javascript:delItemNum(${data.id})">删除</a>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="totalnumber" value="${orderBillItems?size}"/>
										</#list>
                                        </tbody>
                                    </table>

                                    <div class="totalNum"  align="right">
                                        <button  class="btn btn-danger">
                                            <span class=''>合计</span>
                                            <span class='moneySym'>￥</span>
                                            <span class='total-sum'>${(totalSum)!'0.00'}</span>
                                        </button>
                                    </div>

                                <#--地址-->
                                    <form id="addressForm" action="/allPayment" method="post">
                                    <#--<input type="hidden" name="id" value="${id}">
                                    <input type="hidden" name="repaymentNumber" value="${repaymentNumber}">-->
                                        <div align="left">
                                            <span class="" style="color: #1aad19">请填写收货地址</span>
                                        </div>
                                        <div class="weui-cells weui-cells_form">
                                            <div class="weui-cell">
                                                <div class="weui-cell__hd">
                                                    <label class="weui-label" for="home-city">城市</label>
                                                </div>
                                                <div class="weui-cell__bd">
                                                    <input id='city-picker' name="city" class="weui-input" readonly="" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="weui-cells weui-cells_form">
                                            <div class="weui-cell">
                                                <div class="weui-cell__hd">
                                                    <label class="weui-label" for="home-city">详细地址</label>
                                                </div>
                                                <div class="weui-cell__bd">
                                                    <input class="weui-input" name="address" type="text">
                                                </div>
                                            </div>
                                        </div>
                                       <#-- <a id="addressBt" type="button" class="weui-btn weui-btn_primary" style="width: 50%">提交</a>-->
                                    </form>

                                    <form id="normalOrderForm" action="/normalOrder"	>
                                        <div class="totalNum"  align="center">
                                            <button type="button" id="normalOrder" class="weui-btn weui-btn_primary"  style="width: 50%">
                                                <span class=''>提交订单</span>
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                </div>
            </div>


		<#--weUI底部导航-->
            <#include "../common/tabbar-tpl.ftl"/>
        </div>

        <!-- body 最后 -->

        <script src="/js/fastclick.js"></script>
        <script>
            $(function() {
                FastClick.attach(document.body);
            });
        </script>

	</body>
</html>