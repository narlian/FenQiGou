<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <link rel="stylesheet" href="/css/weui.min.css">
    <link rel="stylesheet" href="/css/jquery-weui.min.css">
    <link rel="stylesheet" href="/css/demos.css">
    <title>普通订单详情</title>

<#include "../common/links-tpl.ftl" />
    <link type="text/css" rel="stylesheet" href="/css/account.css"/>
    <script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="/js/plugins-override.js"></script>
    <script type="text/javascript">
        $(function () {
            //购物车商品数量
            var productSum;
            $.get("/getProductsum",function (data) {
                $("#productSum").text(data);
            },'json');

        })
    </script>
</head>
<body>

<div class="weui-tab">
    <div class="weui-tab__bd">
        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
            <!-- 功能页面 -->
            <div class="col-sm-9">
                <form action="/borrowBidReturn_list" name="searchForm" id="searchForm" class="form-inline" method="post">
                    <input type="hidden" id="currentPage" name="currentPage" value="" />
                    <div class="form-group">
                        <label>订单状态</label>
                        <select class="form-control" name="state">
                            <option value="-1">全部</option>
                            <option value="0">待还款</option>
                            <option value="1">已还款</option>
                            <option value="2">逾期</option>
                        </select>
                        <script type="text/javascript">
                            $("[name=state] option[value='${(qo.state)!''}']").attr("selected","selected");
                        </script>
                    </div>
                    <div class="form-group">
                        <button id="query" class="btn btn-success"><i class="icon-search"></i> 查询</button>
                    </div>
                </form>
                <div class="form-group">
                    <label>订单号</label><span>${(qo.orderBillId)!''}</span>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="pull-left" style="line-height: 35px;">订单明细</span>
                        <div class="clearfix"></div>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>商品名称</th>
                            <th>商品价格</th>
                            <th>商品数量</th>
                            <th>小计</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list pageResult as data>
                        <tr>
                            <td><a href="#">${data.product.name}</a></td>
                            <td>${data.price}元</td>
                            <td>${data.number}个</td>
                            <td>${data.amount}元</td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>

                    <div class="totalNum"  align="right">
                        <button  class="btn btn-danger">
                            <span class=''>合计</span>
                            <span class='moneySym'>￥</span>
                            <span class='total-sum'>${(totalSum)!'0.00'}</span>
                        </button>
                    </div>
        </div>
    </div>

    <#--weUI底部导航-->
    <#include "../common/tabbar-tpl.ftl"/>
</div>

<!-- body 最后 -->
<script src="/js/jquery.min.js"></script>
<script src="/js/fastclick.js"></script>
<script>
    $(function() {
        FastClick.attach(document.body);
    });
</script>
<script src="/js/jquery-weui.min.js"></script>
</body>
</html>