<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="/css/weui.min.css">
    <link rel="stylesheet" href="/css/jquery-weui.min.css">

    <script src="/js/jquery/jquery.min.js"></script>
    <script src="/js/jquery/jquery-weui.min.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
    <script src="/js/jquery/swiper.min.js"></script>
    <script type="text/javascript" src="/js/city-picker.js" charset="utf-8"></script>


    <!-- 如果使用了某些拓展插件还需要额外的JS -->
</head>
<script type="text/javascript">
    $(function () {
        $("#city-picker").cityPicker({
            title: "请选择收货地址"
        });

        $("#addressBt").click(function () {
            $.ajax({
                dataType:"json",
                type: "POST",
                url:"/allPayment",
                data:$('#addressForm').serialize(),// 你的formid
                success:function (data) {
                    if(data.success){
                        $.alert(data.msg);
                        location.href = "/product_list";
                    }else {
                        $.alert(data.msg);
                    }
                }
            })
           /* $.post("/allPayment",function (data) {
                if(data.success){
                    $.alert(data.msg);
                    location.href = "/product_list";
                }else{
                    $.alert(data.msg);
                }
            })*/
        })
    })

</script>
<body>
<form id="addressForm" action="/allPayment" method="post">
    <input type="hidden" name="id" value="${id}">
    <input type="hidden" name="repaymentNumber" value="${repaymentNumber}">
    <div align="center">
        <h2 class="demos-title" style="color: #1aad19">请填写收货地址</h2>
    </div>
    <div class="weui-cells weui-cells_form">
        <div class="weui-cell">
            <div class="weui-cell__hd">
                <label class="weui-label" for="home-city">城市</label>
            </div>
            <div class="weui-cell__bd">
                <input id='city-picker' name="city" class="weui-input" readonly="" type="text">
            </div>
        </div>
    </div>
    <div class="weui-cells weui-cells_form">
        <div class="weui-cell">
            <div class="weui-cell__hd">
                <label class="weui-label" for="home-city">详细地址</label>
            </div>
            <div class="weui-cell__bd">
                <input class="weui-input" name="address" type="text">
            </div>
        </div>
    </div>
    <a id="addressBt" type="button" class="weui-btn weui-btn_primary" style="width: 50%">提交</a>
</form>
</body>
</html>