<div class="weui-tabbar">
    <a href="/product_list" class="weui-tabbar__item weui-bar__item--on">
        <div class="weui-tabbar__icon">
            <img src="/images/icon_nav_button.png" alt="">
        </div>
        <p class="weui-tabbar__label">商城</p>
    </a>
    <a href="/orderBillItem" class="weui-tabbar__item">
        <span id="productSum" class="weui-badge" style="position: absolute;top: -.4em;right: 1em;"></span>
        <div class="weui-tabbar__icon">
            <img src="/images/icon_nav_shopcart.png" alt="">
        </div>
        <p class="weui-tabbar__label">购物车</p>
    </a>
    <a href="/WcOrderBill_list" class="weui-tabbar__item">
        <div class="weui-tabbar__icon">
            <img src="/images/icon_nav_article.png" alt="">
        </div>
        <p class="weui-tabbar__label">订单</p>
    </a>
    <a href="/wcPersonal" class="weui-tabbar__item">
        <div class="weui-tabbar__icon">
            <img src="/images/icon_nav_cell.png" alt="">
        </div>
        <p class="weui-tabbar__label">个人中心</p>
    </a>
</div>