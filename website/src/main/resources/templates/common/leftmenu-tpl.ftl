<ul id="menu" class="list-group">
    <li class="list-group-item">
        <a href="javascript:;" class="text-title"><span>我的分期购</span></a>
        <ul class="in">
            <li name="orderBillList"><a href="/orderBill_list"><span>我的订单</span></a></li>
            <li name="borrowBidReturnList"><a href="/borrowBidReturn_list"><span>我的还款</span></a></li>
        </ul>
    </li>
	<li class="list-group-item">
		<a href="#"><span class="text-title">我的账户</span></a>
		<ul class="in">
			<li name="personal"><a href="/personal">账户信息</a></li>
			<li name="bankInfo"><a href="/bankInfo">银行卡管理</a></li>
			<li name="ipLog"><a href="/ipLog">登录记录</a></li>
			<li name="userInfo"><a href="/basicInfo"> <span>个人资料</span></a></li>
		</ul>
	</li>
	<li class="list-group-item">
		<a href="#"><span>资产详情</span></a>
		<ul class="in">
			<li name="accountFlow_list"><a href="">账户流水</a></li>
			<li name="recharge"><a href="/recharge_list">充值明细</a></li>
			<li name="moneyWithdraw"><a href="/moneyWithdraw">提现申请</a></li>
		</ul>
	</li>
</ul>
<script>
        $("li[name=${currentMenu}]").addClass("active");
</script>