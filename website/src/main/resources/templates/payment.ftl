<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>支付页面</title>
    <link rel="stylesheet" href="/js/bootstrap-3.3.2-dist/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/css/core.css" type="text/css" />
    <script type="text/javascript" src="/js/jquery/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="/js/bootstrap-3.3.2-dist/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/jquery.bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-validation/localization/messages_zh.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
    <style type="text/css">
        .el-login-form{
            width:600px;
            margin-left:auto;
            margin-right:auto;
            margin-top: 20px;
        }
        .el-login-form .form-control{
            width: 220px;
            display: inline;
        }
    </style>
</head>
<body>
        <div class="container">
            <form id="loginForm" class="form-horizontal el-login-form" action="#" method="post" >
                <p class="h4" style="margin: 10px 10px 20px 110px;color:#999;">本次购买的商品详情</p>
                <div class="form-group">
                    <div class="col-sm-10">
                        商品名称:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" autocomplete="off" name="username" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        商品数量:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" autocomplete="off" name="" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        商品价格:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" autocomplete="off" name="" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">优惠价格：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;元</div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        还款期数:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" autocomplete="off" name="" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        每期还款金额：&nbsp;&nbsp;<input type="password" autocomplete="off" name="" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        总还款金额：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" autocomplete="off" name="" class="form-control"/>
                    </div>
                </div>
            </form>
        </div>
</body>
</html>