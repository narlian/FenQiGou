<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>商品展示页面</title>
<#include "../common/links-tpl.ftl" />
    <link type="text/css" rel="stylesheet" href="/css/account.css" />
    <script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="/js/plugins-override.js"></script>
    <script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript">
        $(function () {
            //查看按钮
            $(".btn-info").click(function () {
                $(".form-horizontal").submit();
            });
            //分页
            $("#pagination").twbsPagination({
                        totalPages:${pageResult.totalPage} | 1,
                    startPage:${pageResult.currentPage},
            visiblePages:3,
                    onPageClick: function (event, page) {
                $("#currentPage").val(page);
                $("#searchForm").submit();
            }
        }) ;
            //
        })
    </script>
</head>
<body>
<h3>商品列表</h3>
<!-- 功能页面 -->
<div class="col-sm-9">
    <form action="/product_list3" name="searchForm" id="searchForm" class="form-inline" method="post">
        <input type="hidden" id="currentPage" name="currentPage" value="${pageResult.currentPage}" />
    </form>
</div>
<ul class="thumbnails">
<#list pageResult.listData as product>
    <form class="form-horizontal" method="post" action="/product_pay">
        <input type="hidden" name="id" value="${product.id}"/>
        <li class="span4">
            <div class="thumbnail" align="center">
                <img src="${product.imagePath}" alt="...">
                <h3>${product.name}</h3>
                <p>${product.intro}</p>
                <p>惊爆价:${product.salePrice}</p>
                <input type="submit" value="查看详情">
            </div>
        </li>
    </form>
</#list>
</ul>
<div style="text-align: center;">
    <ul id="pagination" class="pagination"></ul>
</div>
</body>
</html>