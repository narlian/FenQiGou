<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <link rel="stylesheet" href="/css/weui.min.css">
    <link rel="stylesheet" href="/css/jquery-weui.min.css">
    <link rel="stylesheet" href="/css/demos.css">
    <title>商品展示页面</title>

<#include "../common/links-tpl.ftl" />
    <link type="text/css" rel="stylesheet" href="/css/account.css"/>
    <script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="/js/plugins-override.js"></script>
    <script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript">
        $(function () {
            //查看按钮
            $(".btn-info").click(function () {
                $(".form-horizontal").submit();
            });
            //购物车商品数量
            var productSum;
            $.get("/getProductsum",function (data) {
                $("#productSum").text(data);
            },'json');
            //
            $(".swiper-slide").click(function (){
                var _this = $(this);
                var imagePath = _this.find("img").prop("src");
                location.href="/activeProduct?imagePath="+imagePath;
            });
            //
        })
    </script>
</head>
<body>

<div class="weui-tab">
    <div class="weui-tab__bd">
        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
            <h3 style="color: #30A9DE">商品列表</h3>
            <div>
                <ul class="thumbnails">
                    <li class="span4">
                        <div class="thumbnail" align="center">
                        <#--1-->
                            <div class="swiper-container">
                                <!-- Additional required wrapper -->
                                <div class="swiper-wrapper" align="center">
                                    <!-- Slides -->
                                <#--<div class="swiper-slide"><img src="/upload/26c924f4-60ac-4d2d-b428-65ecdf9c25cb_small.jpg" style="width: 250px;height: :200px" /></div>-->
                                <#--<div class="swiper-slide"><img src="/upload/774daaf8-86e6-4d0b-b0a1-8044244f0220_small.jpg" style="width: 250px;height: :200px"/></div>-->
                                <#--<div class="swiper-slide"><img src="/upload/848b9edb-1172-4abb-a6e2-284267475d01_small.jpg" style="width: 250px;height: :200px"/></div>-->
                                <#list productList as p>
                                    <div class="swiper-slide"><img src="${p.imagePath}" style="width: 250px;height: :200px" /></div>
                                </#list>
                                </div>
                                <!-- If we need pagination -->
                                <div class="swiper-pagination"></div>
                            </div>

                            <script src="/js/swiper.min.js"></script>

                            <script>
                                $(".swiper-container").swiper({
                                    loop: true,
                                    autoplay: 2000
                                });
                            </script>
                        <#--0-->
                        </div>
                    </li>
                <#list productList as product>
                    <form class="form-horizontal" method="post" action="/product_pay">
                        <input type="hidden" name="id" value="${product.id}"/>
                        <li class="span4">
                            <div class="thumbnail" align="center">
                                <img src="${product.imagePath}" style="width: 200px;height: :200px" alt="...">
                                <h3 style="color: #30A9DE">${product.name}</h3>
                                <p style="color: #EFDC05">${product.intro}</p>
                                <p style="color: #E53A40">原价:<del>${product.salePrice?c+0}</del></p>
                                <p style="color: #090707">惊爆价:${product.salePrice}</p>
                                <input type="submit" value="查看详情">
                            </div>
                        </li>
                    </form>
                </#list>
                </ul>
            </div>
            <div><h2>占个位,半小时有惊喜</h2></div>
        </div>
    </div>


    <#--weUI底部导航-->
    <#include "../common/tabbar-tpl.ftl"/>
</div>

<!-- body 最后 -->
<script src="/js/jquery.min.js"></script>
<script src="/js/fastclick.js"></script>
<script>
    $(function() {
        FastClick.attach(document.body);
    });
</script>
<script src="/js/jquery-weui.min.js"></script>
</body>
</html>