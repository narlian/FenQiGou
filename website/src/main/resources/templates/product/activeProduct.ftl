<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description"
          content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <link rel="stylesheet" href="/css/weui.min.css">
    <link rel="stylesheet" href="/css/jquery-weui.min.css">
    <link rel="stylesheet" href="/css/demos.css">
    <title>分期购买界面</title>
<#include "../common/links-tpl.ftl" />
    <script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
    <link type="text/css" rel="stylesheet" href="/css/account.css"/>
    <script type="text/javascript">
        $(function () {
            //分期按钮
            $(".btn-default").click(function () {
                var _this = $(this);
                $(".btn-default").removeClass("periodsClass");
                _this.addClass("periodsClass");
                var productId = $("input[name=productId]").val();
                var salePrice = $("input[name=salePrice]").val();
                /* $("#tab_form").ajaxSubmit({
                     url: '/getAverageAmount',
                     dataType: 'json',
                     data: {productId: productId, periods: _this.val()},
                     success: function (data) {
                         $("#avgPrice").html(data);
                     }
                 });*/
                var number = (salePrice) / _this.val() + 100 / _this.val();
                $("#avgPrice").html(number.toFixed(2));
            });
            //分期购买
            $(".btn-info").click(function () {
                //期数
                var periods = $(".periodsClass").val();
                if (periods) {
                    var productId = $("input[name=productId]").val();
                    $.get("/averagePayment?id=" + productId + "&repaymentNumber=" + periods, function (data) {
                        $.alert(data.msg);
                    }, 'json')
                } else {
                    $.alert("请选择期数");
                }
            });
            //购物车商品数量
            var productSumObj =  $.get("/getProductsum",function (data) {
                $("#productSum").text(data);
            },'json');
            //加入购物车
            $(".btn-success").click(function () {
                //期数
                var productId = $("input[name=productId]").val();
                $.get("/add_product?id=" + productId, function (data) {
                    $("#productSum").text((productSumObj.responseJSON + 1));
                    $.alert("添加成功,详情请看购物车");
                }, 'json')
            });
            //
        })
    </script>
</head>
<body>
<div class="weui-tab">
    <div class="weui-tab__bd">
        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
            <div class="row">
                <div class="col-md-11"><h3 style="color: #d6ecfa">商品详情</h3></div>
                <div class="col-md-1"><a href="/product_list"><h3 style="color: #FFEEE4">首页</h3></a></div>
            </div>
            <form id="tab_form" method="post">
                <input type="hidden" name="productId" value="${product.id}"/>
                <input type="hidden" name="salePrice" value="${product.salePrice?c}"/>
                <div>
                    <ul class="thumbnails">
                        <li class="span4">
                            <div class="thumbnail" align="center">
                                <img src="${product.imagePath}" style="width: 200px;height: :200px" alt="...">
                                <div class="caption">
                                    <h3 style="color: #67D5B5">${product.name}</h3>
                                    <p style="color: #75D701">${product.intro}</p>
                                    <p style="color: #a5dff9">原价:<del>${product.salePrice?c+0}</del></p>
                                    <p style="color: #ef5285">现价:${product.salePrice}</p>
                                    <p style="color: #60c5ba">手续费:100</p>
                                    <p style="color: #feee7d">分期:
                                        <button type="button" class="btn btn-default" value="3">3期</button>
                                        <button type="button" class="btn btn-default" value="6">6期</button>
                                        <button type="button" class="btn btn-default" value="9">9期</button>
                                        <button type="button" class="btn btn-default" value="12">12期</button>
                                    </p>
                                    <p style="color: #6C49B8">
                                        每期应付:<em id="avgPrice"></em>
                                        <#--<h3 id="avgPrice"></h3>-->
                                    </p>
                                    <div>
                                        <button type="button" class="btn btn-info">分期购买</button>
                                        <button type="button" class="btn btn-success">加入购物车</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                <#--<div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <img src="${product.imagePath}" alt="...">
                            <div class="caption">
                                <h3>${product.name}</h3>
                                <p>${product.intro}</p>
                                <p>价格:${product.salePrice}</p>
                                <p>分期利息:100</p>
                                <p>分期:
                                    <button type="button" class="btn btn-default" value="3">3期</button>
                                    <button type="button" class="btn btn-default" value="6">6期</button>
                                    <button type="button" class="btn btn-default" value="9">9期</button>
                                    <button type="button" class="btn btn-default" value="12">12期</button>
                                </p>
                                <p>
                                &lt;#&ndash;每期支付:<input type="text" id="avgPrice" value=""/>&ndash;&gt;
                                    每期应付:
                                <h3 id="avgPrice"></h3>
                                </p>
                                <button type="button" class="btn btn-info">分期购买</button>
                                <button type="button" class="btn btn-success">加入购物车</button>
                            </div>
                        </div>
                    </div>
                </div>-->
                </div>
            </form>
            <div><h4>占个位,半小时有惊喜</h4></div>
        </div>
    <#--weUI底部导航-->
        <#include "../common/tabbar-tpl.ftl"/>
    </div>
</div>
<!-- body 最后 -->
<script src="/js/jquery.min.js"></script>
<script src="/js/fastclick.js"></script>
<script>
    $(function () {
        FastClick.attach(document.body);
    });
</script>
<script src="/js/jquery-weui.min.js"></script>
</body>
</html>