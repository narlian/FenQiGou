<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>个人中心</title>
		<!-- 包含一个模板文件,模板文件的路径是从当前路径开始找 -->
		<#include "common/links-tpl.ftl" />
		<script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
        <script type="text/javascript" src="/js/bank.js"></script>
		<link type="text/css" rel="stylesheet" href="/css/account.css" />
		
		<script type="text/javascript">
			$(function () {
				$("#showBindPhoneModal").click(function () {
					//弹出模态框
					$("#bindPhoneModal").modal('show');
                });
				$("#sendVerifyCode").click(function () {
				    //获取手机号码
					var phoneNumber =$("#phoneNumber").val();
                    //让按钮变灰
					var _this = $(this);
					_this.prop("disabled",true);
					$.ajax({
						url:"sendVerifyCode",
						data:{phoneNumber:phoneNumber},
                        dataType:"json",
                        success:function(data){
						    if(data.success){
								//提示发送成功
								$.messager.popup(data.msg);
								//修改按钮文本为倒计时
								var count = 90;
								var timer=window.setInterval(function () {
                                    count --;
                                    if(count > 0){
                                        $("#sendVerifyCode").text(count +"s后重新发送");
                                    }else{
                                        window.clearInterval(timer);
                                        $("#sendVerifyCode").text("重新发送");
                                        _this.prop("disabled",false);
                                    }
                                },1000);
							}else{
                                $.messager.popup(data.msg);
                                _this.prop("disabled",false);
							}
						}
					});
                });
				//保存按钮
				$("#bindPhone").click(function () {
					$("#bindPhoneForm").ajaxSubmit({
						dataType:'json',
						success:function (data) {
							if(data.success){
							    window.location.reload();
							}else{
                                $.messager.popup(data.msg);
							}
                        }
					});
                });
				//绑定邮箱按钮
				$("#showBindEmailModal").click(function () {
					$("#bindEmailModal").modal('show');
                });
				//邮箱认证按钮
				$("#bindEmail").click(function () {
					//提交表单
					$("#bindEmailForm").ajaxSubmit({
						dataType:'json',
						success:function (data) {
							if(data.success){
							    $.messager.popup(data.msg);
                                $("#bindEmailModal").modal('hide');
							}else{
                                $.messager.popup(data.msg);
							}
                        }
					});
                });
                //绑定银行卡按钮
                $("#showBankInfoModal").click(function () {
                    $("#bankInfoModal").modal('show');
                });
                //银行卡认证按钮
                $("#bankInfo").click(function () {
                    //提交表单
                    $("#bankInfoForm").ajaxSubmit({
                        dataType:'json',
                        success:function (data) {
                            if(data.success){
                                $.messager.popup(data.msg);
                                $("#bankInfoModal").modal('hide');
                            }else{
                                $.messager.popup(data.msg);
                            }
                        }
                    });
                });

                for(var k in SITE_BANK_TYPE_NAME_MAP){
                    var v=SITE_BANK_TYPE_NAME_MAP[k];
                    $("<option value='"+k+"'>"+v+"</option>").appendTo($("#bankType"));
                }
            })
		</script>
	</head>
	<body>
		<!-- 网页顶部导航 -->
		<#include "common/head-tpl.ftl" />
		<!-- 网页导航 -->
		<!-- 在当前的freemarker的上下文中,添加一个变量,变量的名字叫nav,变量的值叫personal -->
		<#assign currentNav="personal">
		<#include "common/navbar-tpl.ftl" />
		
		<div class="container">
			<div class="row">
				<!--导航菜单-->
				<div class="col-sm-3">
					<#assign currentMenu="personal">
					<#include "common/leftmenu-tpl.ftl" />
				</div>
				<!-- 功能页面 -->
				<div class="col-sm-9">
					<div class="panel panel-default">
						<div class="panel-body el-account">
							<div class="el-account-info">
								<div class="pull-left el-head-img">
									<img class="icon" width="150px" height="200px" src="/images/Imagephoto.png" />
								</div>
								<div class="pull-left el-head">
									<p>用户名：${logininfo.username}</p>
									<p>最后登录时间：2017-08-25 15:30:10</p>
								</div>
								<div class="pull-left" style="text-align: center;width: 400px;margin:30px auto 0px auto;">
									<a class="btn btn-primary btn-lg" href="/recharge">账户充值</a>
									<a class="btn btn-danger btn-lg" href="/moneyWithdraw">账户提现</a>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<div class="row h4 account-info">
								<div class="col-sm-4">
									账户余额：<span class="text-primary">${account.totalAmount?string("0.00")}元</span>
								</div>
                                <div class="col-sm-4">
                                    分期待还金额：<span class="text-primary">${account.unReturnAmount?string("0.00")}元</span>
                                </div>
							</div>

                            <#--<div class="row h4 account-info">
                                <div class="col-sm-4">
                                    体验金总额：<span class="text-primary">${expAccount.usableAmount?string("0.00")}元</span>
                                </div>
                                <div class="col-sm-4">
                                    体验金冻结金额：<span class="text-primary">${expAccount.freezedAmount?string("0.00")}元</span>
                                </div>
                                <div class="col-sm-4">
                                    体验金待回收金额：<span class="text-primary">${expAccount.unReturnExpAmount?string("0.00")}元</span>
                                </div>
                            </div>-->
							
							<div class="el-account-info top-margin">
								<div class="row">
									<div class="col-sm-4">
										<div class="el-accoun-auth">
											<div class="el-accoun-auth-left">
												<img src="images/shouji.jpg" />
											</div>
											<div class="el-accoun-auth-right">
												<h5>手机认证</h5>
											<#if !userinfo.isBindPhone>
												<p>
													未认证
													<a href="javascript:;" id="showBindPhoneModal">立刻绑定</a>
												</p>
											<#else >
                                                <p>
                                                    已认证
                                                    <a href="javascript:;">查看</a>
                                                </p>
											</#if>
											</div>
											<div class="clearfix"></div>
											<p class="info">手机认证后才能分期购买</p>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="el-accoun-auth">
											<div class="el-accoun-auth-left">
												<img src="images/youxiang.jpg" />
											</div>
											<div class="el-accoun-auth-right">
												<h5>邮箱认证</h5>
											<#if !userinfo.isBindEmail >
												<p>
													未绑定
													<a href="javascript:;" id="showBindEmailModal">去绑定</a>
												</p>
											<#else>
                                                <p>
                                                    已认证
                                                    <a href="javascript:;">查看</a>
                                                </p>
											</#if>
											</div>
											<div class="clearfix"></div>
											<p class="info">您可以设置邮箱来接收重要信息</p>
										</div>
									</div>
                                    <div class="col-sm-4">
                                        <div class="el-accoun-auth">
                                            <div class="el-accoun-auth-left">
                                                <img src="images/baozhan.jpg" />
                                            </div>
                                            <div class="el-accoun-auth-right">
                                                <h5>VIP会员</h5>
                                                <p>
                                                    普通用户
                                                    <a href="#">查看</a>
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="info">VIP会员，让你更快捷的投资</p>
                                        </div>
                                    </div>
								</div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="el-accoun-auth">
                                            <div class="el-accoun-auth-left">
                                                <img src="images/shiming.png" />
                                            </div>
                                            <div class="el-accoun-auth-right">
                                                <h5>银行卡认证</h5>
											<#if !userinfo.isBoundBankCard>
                                                <p>
                                                    未认证
                                                    <a href="javascript:;" id="showBankInfoModal">立刻绑定</a>
                                                </p>
											<#else >
                                                <p>
                                                    已认证
                                                    <a href="javascript:;">查看</a>
                                                </p>
											</#if>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="info">银行卡认证后才能分期购买</p>
                                        </div>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<#if !userinfo.isBindPhone>
		<div class="modal fade" id="bindPhoneModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="exampleModalLabel">绑定手机</h4>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" id="bindPhoneForm" method="post" action="/bindPhone">
							<div class="form-group">
								<label for="phoneNumber" class="col-sm-2 control-label">手机号:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="phoneNumber" name="phoneNumber" />
									<button id="sendVerifyCode" class="btn btn-primary" type="button" autocomplate="off">发送验证码</button>
								</div>
							</div>
							<div class="form-group">
								<label for="verifyCode" class="col-sm-2 control-label">验证码:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="verifyCode" name="verifyCode" />
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
						<button type="button" class="btn btn-primary" id="bindPhone">保存</button>
					</div>
				</div>
			</div>
		</div>
		</#if>

		<#if !userinfo.isBindPhone>
		<div class="modal fade" id="bankInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">绑定银行卡</h4>
                    </div>
                    <div class="modal-body">
                        <form id="bankInfoForm" class="form-horizontal" method="post" action="/bankInfo_save">
							<div class="form-group">
								<label class="col-sm-3 control-label">选择银行</label>
								<div class="col-sm-6">
									<select id="bankType" class="form-control" autocomplete="off" name="bankName">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">开户人</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="accountName" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">账号</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="accountNumber" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">开户行</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="forkName" />
								</div>
							</div>
						</form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="button" class="btn btn-primary" id="bankInfo">保存</button>
                    </div>
				</div>
			</div>
		</div>
		</#if>

		<#if !userinfo.isBindEmail>
        <div class="modal fade" id="bindEmailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">绑定邮箱</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="bindEmailForm" method="post" action="/sendEmail">
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="email" name="email" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="button" class="btn btn-primary" id="bindEmail">保存</button>
                    </div>
                </div>
            </div>
        </div>
		</#if>
		<#include "common/footer-tpl.ftl" />
	</body>
</html>