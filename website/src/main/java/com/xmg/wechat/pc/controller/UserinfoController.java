package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserinfoController {
    @Autowired
    private IUserinfoService userinfoService;

    @RequestMapping("/bindPhone")
    @ResponseBody
    public AjaxResult bindPhone(String phoneNumber,String verifyCode){
         AjaxResult result = null;
                 try {
                     userinfoService.bindPhone(phoneNumber,verifyCode);
                     result = new AjaxResult(true,"操作成功");
                 } catch (Exception e) {
                     e.printStackTrace();
                     result = new AjaxResult(false,e.getMessage());
                 }
                 return result;
    }
}
