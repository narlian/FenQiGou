package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {
    @Autowired
    private ILogininfoSercvice logininfoSercvice;

    @RequestMapping("/userLogin")
    @ResponseBody
    public AjaxResult userLogin(String username, String password){
        AjaxResult result = null;
        Logininfo logininfo = logininfoSercvice.login(username,password);
            if(logininfo != null){
                result = new AjaxResult("登录成功");
            }else{
                result = new AjaxResult(false,"账号密码不匹配");
            }
        return result;
    }
}
