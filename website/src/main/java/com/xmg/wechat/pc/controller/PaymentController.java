package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.service.IProductService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.business.service.IOrderBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PaymentController {
    @Autowired
    private IOrderBillService orderBillService;
    @RequestMapping("/recevingAddress")
    public String allPayment(){
        return "allPayment";
    }

    @RequestMapping("/averagePayment")
    public String averagePayment(Long id,int repaymentNumber,Model model){
            model.addAttribute("id",id);
            model.addAttribute("repaymentNumber",repaymentNumber);
        return "receving_address";
    }

    @RequestMapping("/allPayment")
    @ResponseBody
    public AjaxResult recevingAddress(Long id,int repaymentNumber,String city,String address){
        AjaxResult result = null;
        try {
            orderBillService.averagePayment(id,repaymentNumber,city + address);
            result = new AjaxResult("提交成功，请等待后台工作审核");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,"失败，"+e.getMessage());
        }
        return result;
    }
}
