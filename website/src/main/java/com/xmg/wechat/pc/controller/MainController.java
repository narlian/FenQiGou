package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.business.query.BidRequestQueryObject;
import com.xmg.wechat.business.service.IBidRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
    @Autowired
    private IBidRequestService bidRequestService;
    @RequestMapping("/index")
    public String mainPage(BidRequestQueryObject qo, Model model){
        qo.setStates(new int[]{BidConst.BIDREQUEST_STATE_BIDDING,BidConst.BIDREQUEST_STATE_PAYING_BACK,BidConst.BIDREQUEST_STATE_COMPLETE_PAY_BACK});
        qo.setPageSize(5);
        qo.setCurrentPage(1);
        qo.setBidRequestType(BidConst.BIDREQUEST_TYPE_NORMAL);
        model.addAttribute("bidRequests",bidRequestService.queryIndexList(qo));
        //最近5个体验标
        qo.setBidRequestType(BidConst.BIDREQUEST_TYPE_EXP);
        model.addAttribute("expBidRequests",bidRequestService.queryIndexList(qo));
        return "main";
    }

    @RequestMapping("/invest")
    public String invest(){
        return "invest";
    }

    @RequestMapping("/invest_list")
    public String investList(BidRequestQueryObject qo, Model model){
        qo.setStates(new int[]{BidConst.BIDREQUEST_STATE_BIDDING,BidConst.BIDREQUEST_STATE_PAYING_BACK,BidConst.BIDREQUEST_STATE_COMPLETE_PAY_BACK});
        qo.setOrderByType(" br.bidRequestState asc");
         model.addAttribute("pageResult",bidRequestService.queryPage(qo));
        return "invest_list";
    }
}
