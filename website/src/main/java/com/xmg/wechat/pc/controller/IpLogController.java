package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.IpLogQueryObject;
import com.xmg.wechat.base.service.IIpLogService;
import com.xmg.wechat.base.util.RequiredPermission;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IpLogController {
    @Autowired
    private IIpLogService ipLogService;

    @RequiredPermission
    @RequestMapping("/ipLog")
    public String ipLogList(@ModelAttribute("qo")IpLogQueryObject qo, Model model){
        qo.setUsername(UserContext.getCurrent().getUsername());
        PageResult pageResult = ipLogService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "ipLog_list";
    }

}
