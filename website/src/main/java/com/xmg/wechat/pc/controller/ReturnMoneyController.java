package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.query.PaymentScheduleQueryObject;
import com.xmg.wechat.business.service.IPaymentScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ReturnMoneyController {
    @Autowired
    private IPaymentScheduleService paymentScheduleService;
    @Autowired
    private IAccountService accountService;

    @RequestMapping("/borrowBidReturn_list")
    public String returnList(@ModelAttribute("qo") PaymentScheduleQueryObject qo,String orderBillId, Model model){
        qo.setBorrowUserId(UserContext.getCurrent().getId());
        model.addAttribute("pageResult",paymentScheduleService.queryPage(qo));
        model.addAttribute("account", accountService.getCurrent());
        if(orderBillId != null){
            model.addAttribute("paymentSchedules",paymentScheduleService.selectByOrderBillId(orderBillId));
        }else{
            model.addAttribute("paymentSchedules",paymentScheduleService.selectByLoginfoId(UserContext.getCurrent().getId()));
        }
        return "returnmoney_list";
    }
    @RequestMapping("/returnMoney")
    @ResponseBody
    public AjaxResult returnMoney(Long id){
         AjaxResult result = null;
                 try {
                    paymentScheduleService.returnMoney(id);
                     result = new AjaxResult(true,"操作成功");
                 } catch (Exception e) {
                     e.printStackTrace();
                     result = new AjaxResult(false,e.getMessage());
                 }
                 return result;
    }

}
