package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.service.IVerifyCodeService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class VerifyCodeController {
    @Autowired
    private IVerifyCodeService verifyCodeService;
    @RequestMapping("/sendVerifyCode")
    @ResponseBody
    public AjaxResult sendVerifyCode(String phoneNumber){
         AjaxResult result = null;
                 try {
                    verifyCodeService.sendVerifyCode(phoneNumber);
                     result = new AjaxResult(true,"短信发送成功");
                 } catch (Exception e) {
                     e.printStackTrace();
                     result = new AjaxResult(false,e.getMessage());
                 }
                 return result;
    }

}
