package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.service.ISystemDictionaryItemService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BasicInfoController {
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;
    @Autowired
    private IUserinfoService userinfoService;

    @RequestMapping("/basicInfo")
    public String basicInfo(Model model){
        Userinfo current = userinfoService.getCurrent();
        model.addAttribute("userinfo",current);
        model.addAttribute("educationBackgrounds",systemDictionaryItemService.queryListBySn("educationBackground"));
        model.addAttribute("incomeGrades",systemDictionaryItemService.queryListBySn("incomeGrade"));
        model.addAttribute("marriages",systemDictionaryItemService.queryListBySn("marriage"));
        model.addAttribute("kidCounts",systemDictionaryItemService.queryListBySn("kidCount"));
        model.addAttribute("houseConditions",systemDictionaryItemService.queryListBySn("houseCondition"));
        return "userinfo";
    }

    @RequiredPermission
    @RequestMapping("/basicInfo_save")
    @ResponseBody
    public AjaxResult save(Userinfo userinfo){
         AjaxResult result = null;
                 try {
                     userinfoService.basicInfoSave(userinfo);
                     result = new AjaxResult(true,"操作成功");
                 } catch (Exception e) {
                     e.printStackTrace();
                     result = new AjaxResult(false,e.getMessage());
                 }
                 return result;
    }
}
