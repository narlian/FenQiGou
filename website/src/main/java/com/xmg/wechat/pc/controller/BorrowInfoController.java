package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.service.IRealAuthService;
import com.xmg.wechat.base.service.IUserFileService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.BidRequest;
import com.xmg.wechat.business.service.IBidRequestService;
import com.xmg.wechat.business.service.IExpAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

@Controller
public class BorrowInfoController {
    @Autowired
    private IBidRequestService bidRequestService;
    @Autowired
    private IUserinfoService userinfoService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IRealAuthService realAuthService;
    @Autowired
    private IUserFileService userFileService;
    @Autowired
    private IExpAccountService expAccountService;

    @RequestMapping("/borrowInfo")
    public String borrowInfoPage(Model model){
        //判断符合借款条件
        Userinfo userinfo = userinfoService.getCurrent();
        if(bidRequestService.canApplyBorrow(userinfo)){
            //判断用户是否有正在申请贷款
            if(userinfo.hasBidRequestProcess()){
                return "borrow_apply_result";
            }else{
                model.addAttribute("minBidRequestAmount", BidConst.SMALLEST_BIDREQUEST_AMOUNT);
                model.addAttribute("minBidAmount",BidConst.SMALLEST_BID_AMOUNT);
                model.addAttribute("account",accountService.getCurrent());
                return "borrow_apply";
            }
        }else {
            return "redirect:/borrowIndex";
        }
    }

    @RequestMapping("/borrow_apply")
    public String apply(BidRequest bidRequest){
        bidRequestService.apply(bidRequest);
        return "redirect:/borrowInfo";
    }

    @RequestMapping("/borrow_info")
    public String borrowInfo(Long id,Model model){
        BidRequest bidRequest = bidRequestService.get(id);
        String returnPage ="";
        if(bidRequest != null){
            //判断有没有登录
            Logininfo current = UserContext.getCurrent();
            model.addAttribute("bidRequest",bidRequest);
            //判断信用标 需要的值
            if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
                Userinfo createUser = userinfoService.selectByPrimaryKey(bidRequest.getCreateUser().getId());
                model.addAttribute("userInfo",createUser);
                model.addAttribute("realAuth",realAuthService.get(createUser.getRealAuthId()));
                model.addAttribute("userFiles",userFileService.queryAuditListByLogininfo(bidRequest.getCreateUser().getId()));

                if(current != null){
                    //判断当前用户是否为借款创建人, self 设值
                    if(!current.getId().equals(bidRequest.getCreateUser().getId())){
                        model.addAttribute("account",accountService.getCurrent());
                        model.addAttribute("self",false);
                    }else{
                        model.addAttribute("self",true);
                    }
                }else {
                    model.addAttribute("self",false);
                }
                returnPage = "borrow_info";
            }else if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_EXP){
                if(current != null){
                    model.addAttribute("expAccount",expAccountService.getCurrent());
                }
                returnPage = "exp_borrow_info";
            }
        }
        return returnPage;
    }

    /**
     * 投标操作
     * @param bidRequestId
     * @param amount
     * @return
     */
    @RequestMapping("/borrow_bid")
    @ResponseBody
    public AjaxResult bid(Long bidRequestId, BigDecimal amount){
         AjaxResult result = null;
         try {
             bidRequestService.bid(bidRequestId,amount);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }
}
