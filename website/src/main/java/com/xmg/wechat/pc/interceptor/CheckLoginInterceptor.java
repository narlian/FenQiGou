package com.xmg.wechat.pc.interceptor;


import com.xmg.wechat.base.util.RequiredPermission;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckLoginInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(handler instanceof HandlerMethod){
            HandlerMethod method = (HandlerMethod) handler;
            if(method.getMethodAnnotation(RequiredPermission.class) != null){
                if(UserContext.getCurrent() == null){
                    //跳转到登录页面
                    response.sendRedirect("/login.html");
                    return false;
                }
            }
        }
        return true;
    }
}
