package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.domain.SystemDictionaryItem;
import com.xmg.wechat.base.domain.UserFile;
import com.xmg.wechat.base.service.ISystemDictionaryItemService;
import com.xmg.wechat.base.service.IUserFileService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.RequiredPermission;
import com.xmg.wechat.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UserFileController {
    @Value("${uploadPath}")
    private String uploadPath;
    @Autowired
    private IUserFileService userFileService;
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;

    @RequiredPermission
    @RequestMapping("/userFile")
    public String userFile(Model model, HttpSession session){
        //查询未选择风控类型的记录
        List<UserFile> unselectedFileTypeList = userFileService.selectedFileTypeList(false);
        model.addAttribute("sessionid", session.getId());
        if(unselectedFileTypeList.size() == 0){
            model.addAttribute("userFiles",userFileService.selectedFileTypeList(true));
            return "userFiles";
        }else{
            model.addAttribute("userFiles",unselectedFileTypeList);
            //构建下拉列框
            List<SystemDictionaryItem> userFileType = systemDictionaryItemService.queryListBySn("userFileType");
            model.addAttribute("fileTypes",userFileType);
            return "userFiles_commit";
        }
    }

    @RequestMapping("/userFileUpload")
    public String uploadFile(MultipartFile file){
        String imagePath = UploadUtil.upload(file, uploadPath);
        userFileService.apply(imagePath);
        return "userFiles_commit";
    }

    @RequestMapping("/userFile_selectType")
    @ResponseBody
    public AjaxResult selectType(Long[] id, Long[] fileType){
         AjaxResult result = null;
         try {
             userFileService.choiseType(id,fileType); //此处是DML操作, 用select 会导致事务嵌套
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }
}
