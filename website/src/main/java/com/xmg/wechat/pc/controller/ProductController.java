package com.xmg.wechat.pc.controller;

import com.alibaba.fastjson.JSON;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.domain.Product;
import com.xmg.wechat.base.query.ProductQueryObject;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.service.IProductService;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.wc.util.HttpUtil;
import com.xmg.wechat.wc.util.UrlUtil;
import com.xmg.wechat.wc.vo.OAuthAccessTokenVo;
import com.xmg.wechat.wc.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Administrator on 2017/11/7.
 */
@Controller
public class ProductController {
    @Value("${wechat.appid}")
    private String appid;
    @Value("${wechat.secret}")
    private String secret;

    @Autowired
    private ILogininfoSercvice logininfoSercvice;
    @Autowired
    private IProductService productService;

    /**
     * 前台商品首页
     * @param model
     * @param qo
     * @param code
     * @return
     */
    @RequestMapping("product_list")
    public String list3(Model model,ProductQueryObject qo,String code) {
        if (UserContext.getCurrent()==null) {
            String result = HttpUtil.get(UrlUtil.OAUTH2_ACCESS_TOKEN_URL
                    .replace("APPID", appid)
                    .replace("SECRET", secret)
                    .replace("CODE", code));
            OAuthAccessTokenVo oAuthAccessToken = JSON.parseObject(result, OAuthAccessTokenVo.class);
            String userInfo = HttpUtil.get(UrlUtil.USERINFO_URL
                    .replace("ACCESS_TOKEN", oAuthAccessToken.getAccess_token())
                    .replace("OPENID", oAuthAccessToken.getOpenid()));
            UserInfoVo userInfoVo = JSON.parseObject(userInfo, UserInfoVo.class);
            Logininfo logininfo = logininfoSercvice.login(userInfoVo.getOpenid(), userInfoVo.getOpenid());
        }
         model.addAttribute("productList", productService.selectAll());
        //model.addAttribute("pageResult", productService.queryPage(qo));
        return "product/list";
    }

    /**
     * 前台商品详情+购买页面
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/product_pay")
    public String pay(Model model, Long id) {
        model.addAttribute("product", productService.get(id));
        return "product/pay";
    }

    /**
     * 活跃商品
     */

    @RequestMapping("/activeProduct")
    public String activeProduct(Model model,String imagePath){
        imagePath = imagePath.substring(26);
        System.out.println(imagePath);
        model.addAttribute("product", productService.selectByImagePath(imagePath));
        return "product/activeProduct";
    }

    /**
     * 每期应付
     * @param productId
     * @param periods
     * @return
     */
    @RequestMapping("/getAverageAmount")
    @ResponseBody
    public BigDecimal page(Long productId, BigDecimal periods) {
        BigDecimal avgPrice = null;
        try {
            avgPrice = productService.getAverageAmount(productId, periods);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return avgPrice;
    }

    /**
     * 随机展示商品
     */
    @RequestMapping("/randomProduct")
    public String randomProduct(Model model){
        List<Product> products = productService.selectAll();
        ArrayList<Long> productIds = new ArrayList();
        for(Product p:products){
            productIds.add(p.getId());
        }
        Random random = new Random();
        Long productId = productIds.get(random.nextInt(productIds.size() - 1));
        model.addAttribute("product",productService.get(productId));
        model.addAttribute("productList", productService.selectAll());
        return "product/randomProduct";
    }
}
