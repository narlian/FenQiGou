package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LogininfoController {
    @Autowired
    private ILogininfoSercvice logininfoSercvice;
    @RequestMapping("/registerIndex")
    public String register(){
        return "register";
    }

    @RequestMapping("/userRegister")
    @ResponseBody
    public AjaxResult userRegister(String username,String password){
        AjaxResult result = null;
        try {
            logininfoSercvice.register(username,password);
            result = new AjaxResult("注册成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,"注册失败"+e.getMessage());
        }
        return result;
    }

    @RequestMapping("/checkUsername")
    @ResponseBody
    public Boolean checkUsername(String username){
        return  logininfoSercvice.checkUsername(username);
    }

}
