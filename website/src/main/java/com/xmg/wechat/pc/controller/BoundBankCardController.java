package com.xmg.wechat.pc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BoundBankCardController {
    @RequestMapping("/boundBankCard")
    public String boundBankCardPage(){
        return "boundBankCard";
    }

    @RequestMapping("/payment")
    public String payment(){
        return "payment";
    }
}
