package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.domain.Account;
import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BorrowIndexController {
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IUserinfoService userinfoService;

    @RequestMapping("/borrowIndex")
    public String borrowIndex(Model model){
        Logininfo current = UserContext.getCurrent();
        if(current == null ){
            return "redirect:borrow.html";
        }else {
            Account account = accountService.selectByPrimaryKey(current.getId());
            model.addAttribute("account",account);
            Userinfo userinfo = userinfoService.selectByPrimaryKey(current.getId());
            model.addAttribute("userinfo",userinfo);
            model.addAttribute("creditBorrowScore", BidConst.CREDIT_BORROW_SCORE);
            return "borrow";
        }
    }
}
