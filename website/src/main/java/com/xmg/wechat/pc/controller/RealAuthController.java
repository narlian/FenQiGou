package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.domain.RealAuth;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.service.IRealAuthService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class RealAuthController {
    @Autowired
    private IUserinfoService userinfoService;
    @Autowired
    private IRealAuthService realAuthService;
    @Value("${uploadPath}")
    private String uploadPath;

    @RequestMapping("/realAuth")
    public String realAuth(Model model){
        //判断用户是否已实名认证
        Userinfo current = userinfoService.getCurrent();
        if(current.getIsRealAuth()){
        //1 如果是 查询出对应RealAuth对象放入模型中 并跳转到realAuth_result
            RealAuth realAuth = realAuthService.get(current.getRealAuthId());
            model.addAttribute("realAuth",realAuth);
            model.addAttribute("auditing",false);
            return "realAuth_result";
        }else {
            //2 如果不是 判断userinfo中realAuthId是否为空
            if(current.getRealAuthId() != null){
                //2.1 如果不为空 跳转到realAuth_result
                model.addAttribute("auditing",true);
                return "realAuth_result";
            }else {
                //2.2 如果为空 跳转到realAuth
                return "realAuth";
            }
        }
    }

    @RequestMapping("/realAuth_save")
    public String apply(RealAuth realAuth){
        realAuthService.apply(realAuth);
        return "redirect:realAuth";
    }

    @RequestMapping("/uploadImage")
    @ResponseBody
    public String uploadImage(MultipartFile image){
        String imagePath = UploadUtil.upload(image, uploadPath);
        return imagePath;
    }
}
