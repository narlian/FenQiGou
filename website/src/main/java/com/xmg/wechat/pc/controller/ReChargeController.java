package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.business.domain.RechargeOffline;
import com.xmg.wechat.business.service.IPlatformBankinfoService;
import com.xmg.wechat.business.service.IRechargeOfflineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ReChargeController {

    @Autowired
    private IRechargeOfflineService rechargeOfflineService;
    @Autowired
    private IPlatformBankinfoService platformBankinfoService;

    @RequestMapping("/recharge")
    public String recharge(Model model){
        model.addAttribute("banks",platformBankinfoService.sellectAll());
        return "recharge";
    }

    @RequestMapping("/recharge_save")
    @ResponseBody
    public AjaxResult applyCharge(RechargeOffline rechargeOffline){
         AjaxResult result = null;
         try {
             rechargeOfflineService.applyCharge(rechargeOffline);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }

}
