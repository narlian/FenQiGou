package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.service.IMailVerifyService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EmailVerifyController {
    @Autowired
    private IMailVerifyService mailVerifyService;
    @RequestMapping("/sendEmail")
    @ResponseBody
    public AjaxResult sendEmail(String email){
         AjaxResult result = null;
                 try {
                     mailVerifyService.sendEmail(email);
                     result = new AjaxResult(true,"邮件发送成功成功");
                 } catch (Exception e) {
                     e.printStackTrace();
                     result = new AjaxResult(false,e.getMessage());
                 }
                 return result;
    }
}
