package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.business.domain.UserBankinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BankInfoController {
    @Autowired
    private IUserinfoService userinfoService;

    @RequestMapping("/bankInfo_save")
    @ResponseBody
    public AjaxResult bankInfoSave(UserBankinfo userBankinfo) {
        AjaxResult result = null;
        try {
            userinfoService.save(userBankinfo);
            result = new AjaxResult("成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,"失败");
        }
        return result;
    }
}
