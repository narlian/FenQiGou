package com.xmg.wechat.pc.controller;

import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.OrderBill;
import com.xmg.wechat.business.domain.OrderBillItem;
import com.xmg.wechat.business.query.OrderBillItemQueryObject;
import com.xmg.wechat.business.service.IOrderBillItemService;
import com.xmg.wechat.business.service.IOrderBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.List;

@Controller
public class OrderBillItemController {
    @Autowired
    private IOrderBillItemService orderBillItemService;
    @Autowired
    private IOrderBillService orderBillService;

    /**
     * 查看订单
     * @param qo
     * @param model
     * @return
     */
    @RequestMapping("orderBillItem_list")
    public String itemList(@ModelAttribute("qo") OrderBillItemQueryObject qo, Model model){
        qo.setCurrentUserId(UserContext.getCurrent().getId());
        model.addAttribute("pageResult",orderBillItemService.queryPage(qo));
        //model.addAttribute("pageResult",orderBillItemService.queryListByORderBillId(qo));
        OrderBill orderBill = orderBillService.getById(qo.getOrderBillId());
        model.addAttribute("totalSum",orderBill.getTotalSum());
        return "orderBillItem_list";
    }

    /**
     * 添加商品
     */
    @RequestMapping("add_product")
    @ResponseBody
    public AjaxResult addProduct(Long id){
         AjaxResult result = null;
         try {
             orderBillItemService.add(id);
             result = new AjaxResult(true,"加入购物车成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }

    /**
     * 查看购物车
     */
    @RequestMapping("/orderBillItem")
    public String orderBillItem(Model model){
        List<OrderBillItem> orderBillItems = orderBillItemService.queryList();
        model.addAttribute("orderBillItems",orderBillItems);
        //回显总价格
        BigDecimal totalsum = BigDecimal.ZERO;
        for (OrderBillItem orderBillItem : orderBillItems) {
            totalsum = totalsum.add(orderBillItem.getAmount());
        }
        model.addAttribute("totalSum",totalsum);
        return "wechat/orderBillItems";
    }

    /**
     * 下单
     */
    @RequestMapping("/normalOrder")
    @ResponseBody
    public AjaxResult normalOrder(){
        AjaxResult result = null;
        try {
            orderBillItemService.normalOrder();
            result = new AjaxResult(true,"下单成功成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }

    /**
     * 购物车中添加商品
     */
    @RequestMapping("addOrderBillItem")
    @ResponseBody
    public AjaxResult addShoproduct(Long id){
        AjaxResult result = null;
        try {
            orderBillItemService.addItem(id);
            result = new AjaxResult(true,"添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }

    /**
     * 购物车中减少商品
     */
    @RequestMapping("subOrderBillItem")
    @ResponseBody
    public AjaxResult subShoproduct(Long id){
        AjaxResult result = null;
        try {
            orderBillItemService.subItem(id);
            result = new AjaxResult(true,"减少成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }

    /**
     * 购物车中删除商品
     */
    @RequestMapping("deleteOrderBillItem")
    @ResponseBody
    public AjaxResult delShoproduct(Long id){
        AjaxResult result = null;
        try {
            orderBillItemService.deleteItem(id);
            result = new AjaxResult(true,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }
    /**
     * 页面回显购物车商品数量
     */
    @RequestMapping("/getProductsum")
    @ResponseBody
    public Integer getProductSum(){
        List<OrderBillItem> orderBillItems = orderBillItemService.queryList();
        return orderBillItems.size();
    }
}
