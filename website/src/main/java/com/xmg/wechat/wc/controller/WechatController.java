package com.xmg.wechat.wc.controller;


import com.alibaba.fastjson.JSON;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.domain.Message;
import com.xmg.wechat.base.service.IAutoResponseService;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.service.IMessageService;
import com.xmg.wechat.business.domain.WeChatMenu;
import com.xmg.wechat.business.domain.WeChatMenuItem;
import com.xmg.wechat.business.service.IWeChatMenuItemService;
import com.xmg.wechat.business.service.IWeChatMenuService;
import com.xmg.wechat.wc.util.*;
import com.xmg.wechat.wc.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class WechatController {

    @Autowired
    private IMessageService messageService;
    @Autowired
    private ILogininfoSercvice logininfoSercvice;
    @Autowired
    private IAutoResponseService autoResponseService;
    @Autowired
    private IWeChatMenuService weChatMenuService;
    @Autowired
    private IWeChatMenuItemService weChatMenuItemService;

    @Value("${wechat.token}")
    private String token;
    @Value("${wechat.appid}")
    private String appid;
    @Value("${wechat.secret}")
    private String secret;
    @Value("${wechat.url}")
    private String url;
    @Value("${tuling.url}")
    private String tulingUrl;

    @RequestMapping(value = "/wechat",method = RequestMethod.GET)
    @ResponseBody
    public String wechat(String signature,String timestamp,String nonce,String echostr) {
        //1）将token、timestamp、nonce三个参数进行字典序排序
        TreeSet<String> set = new TreeSet<String>();
        set.add(token);
        set.add(timestamp);
        set.add(nonce);
        //2）将三个参数字符串拼接成一个字符串进行sha1加密
        StringBuilder sb = new StringBuilder();
        for (String str : set) {
            sb.append(str);
        }
        String sha1Str = SecurityUtil.SHA1(sb.toString());
        //3）开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
        if (sha1Str.equals(signature)) {
            //如果对比一样就返回echostr
            return echostr;
        }

        return "success";
    }

    @RequestMapping(value = "/wechat",method = RequestMethod.POST)
    @ResponseBody
    public String wechat(@RequestBody WeChatRequest vo) {

        String msgType = vo.getMsgType();
        //如果是事件的话
        if (msgType.equals("event")) {
            //判断是什么事件,获取事件的类型参数
            String event = vo.getEvent();
            if (event.equals("subscribe")) {
                //如果是订阅事件,做具体的业务,比如保存该用户到数据库
                logininfoSercvice.register(vo.getFromUserName(),vo.getFromUserName());
                return sendText(vo,"欢迎您订阅分期购,为表示感谢,赠送您一只女装大佬!");
            } else if (event.equals("unsubscribe")){
                //如果是取消订阅事件,做具体的业务,比如删除该用户
                logininfoSercvice.updateState(vo.getFromUserName(), Logininfo.STATE_LOCK);
                System.out.println(vo.getFromUserName()+"取消订阅了");
            } else if(vo.getEventKey().equals("666")){
                return sendArticles(vo);
            }
        }else{
            Message message = new Message();
            message.setInputTime(new Date());
            message.setReceiveContent(vo.getContent());
            message.setLogininfo(logininfoSercvice.selectByName(vo.getFromUserName()));
            String replyContent = autoResponseService.selectByKeyword(vo.getContent());
            if(vo.getContent().equals("今日推荐")){
                message.setReplyContent(replyContent);
                messageService.save(message);
                return sendArticles(vo);
            }
            if(replyContent != null){
                //识别关键字，发送特定内容
                message.setReplyContent(replyContent);
                messageService.save(message);
                return sendText(vo,replyContent);
            }

            //接入图灵
            if(vo.getMsgType().equals("text")){
                String content = vo.getContent();
                TuLingRequest tuLingRequest = new TuLingRequest(content, vo.getFromUserName());
                String result = HttpUtil.post(tulingUrl, JSON.toJSONString(tuLingRequest));
                TuLingResponse tuLingResponse = JSON.parseObject(result, TuLingResponse.class);
                message.setReplyContent(tuLingResponse.getText());
                messageService.save(message);
                return sendText(vo, tuLingResponse.getText());
            }
        }
        return null;
    }

    private String sendArticles(@RequestBody WeChatRequest vo) {
        List<ArticleResponse> articleList = new ArrayList<ArticleResponse>();

        ArticleResponse ar1 = new ArticleResponse();
        ar1.setDescription("1");
        ar1.setTitle("跳楼价大甩卖组长一个");
        ar1.setPicUrl("http://k64a9u.natappfree.cc/images/6.gif");
        ar1.setUrl("https://www.baidu.com");
        articleList.add(ar1);

        ArticleResponse ar2 = new ArticleResponse();
        ar2.setDescription("2");
        ar2.setTitle("第一个");
        ar2.setPicUrl("http://k64a9u.natappfree.cc/images/8.jpg");
        ar2.setUrl(url+"/randomProduct");
        articleList.add(ar2);

        ArticleResponse ar3 = new ArticleResponse();
        ar3.setDescription("3");
        ar3.setTitle("第二个");
        ar3.setPicUrl("http://k64a9u.natappfree.cc/images/9.jpg");
        ar3.setUrl(url+"/randomProduct");
        articleList.add(ar3);

        ArticleResponse ar4 = new ArticleResponse();
        ar4.setDescription("4");
        ar4.setTitle("第三个");
        ar4.setPicUrl("http://k64a9u.natappfree.cc/images/10.jpg");
        ar4.setUrl(url+"/randomProduct");
        articleList.add(ar4);

        WeChatResponse weChatResponse = new WeChatResponse();
        weChatResponse.setToUserName(vo.getFromUserName());
        weChatResponse.setFromUserName(vo.getToUserName());
        weChatResponse.setCreateTime(new Date().getTime() + "");
        weChatResponse.setMsgType("news");
        weChatResponse.setArticleCount(articleList.size() + "");
        weChatResponse.setArticle(articleList);
        String result = JAXBUtil.convertToXml(weChatResponse);
        System.out.println(result);
        return result;
    }

    /**
     * 发送文本消息的方法
     * @param vo
     * @return
     */
    private String sendText(@RequestBody WeChatRequest vo,String msg) {
        WeChatResponse weChatResponse = new WeChatResponse();
        weChatResponse.setToUserName(vo.getFromUserName());
        weChatResponse.setFromUserName(vo.getToUserName());
        weChatResponse.setCreateTime(new Date()+"");
        weChatResponse.setContent(msg);
        weChatResponse.setMsgType("text");

        String responseXmlStr = JAXBUtil.convertToXml(weChatResponse);
        System.out.println(responseXmlStr);
        return responseXmlStr;
    }

    @RequestMapping("/sendTemplate")
    @ResponseBody
    public String sendTemplate() {

        TemplateVo templateVo = new TemplateVo();
        templateVo.setTemplate_id("J6ZriU8SR5XtRkdGKncOz45sluo1732o9T3cnhxeQJ8");
        templateVo.setTouser("oEzvzwa_61cS83azZeG6A95TzqhA");
        templateVo.setUrl("http://jandan.net/pic");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateTime = sdf.format(new Date());
        Map<String,TemplateValueVo> dataMap = new HashMap<String,TemplateValueVo>();
        dataMap.put("first",new TemplateValueVo("恭喜你购买成功","#173177"));
        dataMap.put("keynote1",new TemplateValueVo("Tenga(极短款)","#173177"));
        dataMap.put("keynote2",new TemplateValueVo("299.8元","#173177"));
        dataMap.put("keynote3",new TemplateValueVo(dateTime,"#173177"));
        dataMap.put("remark",new TemplateValueVo("欢迎再次购买","#173177"));
        templateVo.setData(dataMap);

        //发送模板消息
        String sendTemplateUrl = UrlUtil.MESSAGE_TEMPLATE_SEND_URL.replace("ACCESS_TOKEN", AccessTokenUtil.getAccessToken().getAccess_token());
        String resultTemplate = HttpUtil.post(sendTemplateUrl, JSON.toJSONString(templateVo));
        return resultTemplate;
    }

    @RequestMapping("/customMenu")
    @ResponseBody
    public String customMenu() {
        //总的button
        Map<String, List<CustomMenuVo>> buttonMap = new HashMap<String, List<CustomMenuVo>>();
        //一级菜单列表
        List<CustomMenuVo> customMenuList1 = new ArrayList<CustomMenuVo>();
        //一级菜单中的第一个按钮
        CustomMenuVo customMenu11 = new CustomMenuVo();
        customMenu11.setName("分期购");
        customMenuList1.add(customMenu11);

        //一级菜单中的第一个按钮的二级菜单列表
        ArrayList<CustomMenuVo> customMenuList11 = new ArrayList<CustomMenuVo>();
        //一级菜单中的第一个按钮的二级菜单
        CustomMenuVo customMenu111 = new CustomMenuVo();
        customMenu111.setName("智能手机");
        customMenu111.setType("view");
        String userInfoUrl11 = url+"/product_list";
        String url11 = UrlUtil.AUTHORIZE_URL.replace("APPID",appid)
                .replace("REDIRECT_URI",userInfoUrl11)
                .replace("SCOPE","snsapi_userinfo");
        customMenu111.setUrl(url11);
        customMenuList11.add(customMenu111);

        //一级菜单中的第一个按钮的二级菜单
        CustomMenuVo customMenu112 = new CustomMenuVo();
        customMenu112.setName("平板电脑");
        customMenu112.setType("view");
        String userInfoUrl12 = url+"/product_list";
        String url12 = UrlUtil.AUTHORIZE_URL.replace("APPID",appid)
                .replace("REDIRECT_URI",userInfoUrl12)
                .replace("SCOPE","snsapi_userinfo");
        customMenu112.setUrl(url12);
        customMenuList11.add(customMenu112);

        //一级菜单中的第一个按钮的二级菜单
        CustomMenuVo customMenu113 = new CustomMenuVo();
        customMenu113.setName("今日推荐");
        customMenu113.setType("click");
        customMenu113.setKey("666");
        customMenuList11.add(customMenu113);

        //把一级菜单中的第一个按钮列表添加到按钮中
        customMenu11.setSub_button(customMenuList11);

        //一级菜单中的第二个按钮
        CustomMenuVo customMenu12 = new CustomMenuVo();
        customMenu12.setName("我的还款");
        customMenu12.setType("view");
        String userInfoUr2 = url+"/wechatReturnMonryList";
        String url2 = UrlUtil.AUTHORIZE_URL.replace("APPID",appid)
                .replace("REDIRECT_URI",userInfoUr2)
                .replace("SCOPE","snsapi_userinfo");
        customMenu12.setUrl(url2);
        customMenuList1.add(customMenu12);

        //一级菜单中的第三个按钮
        CustomMenuVo customMenu13 = new CustomMenuVo();
        customMenu13.setName("个人中心");
        customMenu13.setType("view");
        String userInfoUrl3 = url+"/personal";
        String url3 = UrlUtil.AUTHORIZE_URL
                .replace("APPID",appid)
                .replace("REDIRECT_URI",userInfoUrl3)
                .replace("SCOPE","snsapi_userinfo");
        customMenu13.setUrl(url3);
        customMenuList1.add(customMenu13);

        //把一级菜单的列表添加到总的mapButton中
        buttonMap.put("button",customMenuList1);
        String cutomMenuUrl = UrlUtil.MENU_CREATE_URL.replace("ACCESS_TOKEN", AccessTokenUtil.getAccessToken().getAccess_token());
        String resultStr = HttpUtil.post(cutomMenuUrl, JSON.toJSONString(buttonMap));
        return resultStr;
    }

    /**
     * 用户中心
     * @param code
     * @return
     */
    @RequestMapping("/userInfo")
    public String userInfo(String code,Map map) {
        String result = HttpUtil.get(UrlUtil.OAUTH2_ACCESS_TOKEN_URL
                .replace("APPID", appid)
                .replace("SECRET", secret)
                .replace("CODE", code));
        OAuthAccessTokenVo oAuthAccessToken = JSON.parseObject(result, OAuthAccessTokenVo.class);
        String userInfo = HttpUtil.get(UrlUtil.USERINFO_URL
                .replace("ACCESS_TOKEN", oAuthAccessToken.getAccess_token())
                .replace("OPENID", oAuthAccessToken.getOpenid()));
        map.put("userInfo",userInfo);
        System.out.println(userInfo);
        return "user_info";
    }

    @RequestMapping("/customMenu1")
    @ResponseBody
    public String customMenu1() {
        //总的button
        Map<String, List<CustomMenuVo>> buttonMap = new HashMap<String, List<CustomMenuVo>>();
        //一级菜单列表
        List<CustomMenuVo> customMenuList1 = new ArrayList<CustomMenuVo>();
        List<CustomMenuVo> customMenuList11;
        List<WeChatMenu> weChatMenus = weChatMenuService.selectAll();
        List<WeChatMenuItem> weChatMenuItems;
        CustomMenuVo customMenu;
        CustomMenuVo customMenu1;
        for (WeChatMenu weChatMenu : weChatMenus) {
            customMenu = new CustomMenuVo();
            if ("view".equals(weChatMenu.getType())) {
                customMenu.setName(weChatMenu.getName());
                customMenu.setType("view");
                String userInfoUr = url+weChatMenu.getUrl();
                String ur = UrlUtil.AUTHORIZE_URL.replace("APPID",appid)
                        .replace("REDIRECT_URI",userInfoUr)
                        .replace("SCOPE","snsapi_userinfo");
                customMenu.setUrl(ur);
            } else if ("click".equals(weChatMenu.getType())) {
                customMenu.setName(weChatMenu.getName());
                customMenu.setType("click");
                customMenu.setKey(weChatMenu.getKey());
            }else {
                customMenu.setName(weChatMenu.getName());
            }
            weChatMenuItems = weChatMenuItemService.selectAllId(weChatMenu.getId());
            if (weChatMenuItems.size()>0) {
                customMenuList11 = new ArrayList<CustomMenuVo>();
                for (WeChatMenuItem weChatMenuItem : weChatMenuItems) {
                    customMenu1 = new CustomMenuVo();
                    if ("view".equals(weChatMenuItem.getType())) {
                        customMenu1.setName(weChatMenuItem.getName());
                        customMenu1.setType("view");
                        String userInfoUr1 = url+weChatMenuItem.getUrl();
                        String ur1 = UrlUtil.AUTHORIZE_URL.replace("APPID",appid)
                                .replace("REDIRECT_URI",userInfoUr1)
                                .replace("SCOPE","snsapi_userinfo");
                        customMenu1.setUrl(ur1);
                    } else if ("click".equals(weChatMenuItem.getType())) {
                        customMenu1.setName(weChatMenuItem.getName());
                        customMenu1.setType("click");
                        customMenu1.setKey(weChatMenuItem.getKey());
                    }
                    customMenuList11.add(customMenu1);
                }
                customMenu.setSub_button(customMenuList11);
            }
            customMenuList1.add(customMenu);
        }

        //把一级菜单的列表添加到总的mapButton中
        buttonMap.put("button",customMenuList1);
        String cutomMenuUrl = UrlUtil.MENU_CREATE_URL.replace("ACCESS_TOKEN", AccessTokenUtil.getAccessToken().getAccess_token());
        String resultStr = HttpUtil.post(cutomMenuUrl, JSON.toJSONString(buttonMap));
        return resultStr;
    }
}
