package com.xmg.wechat.wc.vo;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class OAuthAccessTokenVo {
    private String access_token;
    private String expires_in;
    private String openid;
}
