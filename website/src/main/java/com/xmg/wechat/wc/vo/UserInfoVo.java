package com.xmg.wechat.wc.vo;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class UserInfoVo {
    private String openid;
    private String nickname;
    private String province;
    private String city;
    private String country;
    private String language;
    private String headimgurl;
    private String privilege;
}
