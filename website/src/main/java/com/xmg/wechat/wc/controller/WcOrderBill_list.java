package com.xmg.wechat.wc.controller;

import com.xmg.wechat.business.query.OrderBillQueryObject;
import com.xmg.wechat.business.service.IOrderBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WcOrderBill_list {
    @Autowired
    private IOrderBillService orderBillService;

    @RequestMapping("/WcOrderBill_list")
    public String orderBillPage(@ModelAttribute("qo")OrderBillQueryObject qo, Model model ){
        //查询出分期购买列表
        model.addAttribute("bidOrders",orderBillService.queryBidOrderList());
        //查询出普通购买列表
        model.addAttribute("normalOrders",orderBillService.queryNormalOrderList());
        return "wechat/orderBill_list";
    }
}
