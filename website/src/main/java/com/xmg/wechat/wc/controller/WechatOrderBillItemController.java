package com.xmg.wechat.wc.controller;

import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.domain.OrderBill;
import com.xmg.wechat.business.query.OrderBillItemQueryObject;
import com.xmg.wechat.business.service.IOrderBillItemService;
import com.xmg.wechat.business.service.IOrderBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WechatOrderBillItemController {
    @Autowired
    private IOrderBillItemService orderBillItemService;
    @Autowired
    private IOrderBillService orderBillService;

    @RequestMapping("wcOrderBillItem_list")
    public String itemList(@ModelAttribute("qo") OrderBillItemQueryObject qo, Model model){
        qo.setCurrentUserId(UserContext.getCurrent().getId());
        //model.addAttribute("pageResult",orderBillItemService.queryPage(qo));
        model.addAttribute("pageResult",orderBillItemService.queryListByORderBillId(qo));
        OrderBill orderBill = orderBillService.getById(qo.getOrderBillId());
        model.addAttribute("totalSum",orderBill.getTotalSum());
        return "wechat/orderBillItem_list";
    }
}
