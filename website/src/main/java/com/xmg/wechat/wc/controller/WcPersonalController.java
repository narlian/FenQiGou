package com.xmg.wechat.wc.controller;

import com.alibaba.fastjson.JSON;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.service.IAccountService;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.service.IExpAccountService;
import com.xmg.wechat.wc.util.HttpUtil;
import com.xmg.wechat.wc.util.UrlUtil;
import com.xmg.wechat.wc.vo.OAuthAccessTokenVo;
import com.xmg.wechat.wc.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WcPersonalController {
    @Value("${wechat.appid}")
    private String appid;
    @Value("${wechat.secret}")
    private String secret;

    @Autowired
    private IAccountService accountService;
    @Autowired
    private IUserinfoService userinfoService;
    @Autowired
    private IExpAccountService expAccountService;
    @Autowired
    private ILogininfoSercvice logininfoSercvice;


    @RequestMapping("/wcPersonal")
    public String personal(String code,Model model){
        if (UserContext.getCurrent()==null) {
            String result = HttpUtil.get(UrlUtil.OAUTH2_ACCESS_TOKEN_URL
                    .replace("APPID", appid)
                    .replace("SECRET", secret)
                    .replace("CODE", code));
            OAuthAccessTokenVo oAuthAccessToken = JSON.parseObject(result, OAuthAccessTokenVo.class);
            String userInfo = HttpUtil.get(UrlUtil.USERINFO_URL
                    .replace("ACCESS_TOKEN", oAuthAccessToken.getAccess_token())
                    .replace("OPENID", oAuthAccessToken.getOpenid()));
            UserInfoVo userInfoVo = JSON.parseObject(userInfo, UserInfoVo.class);
            Logininfo logininfo = logininfoSercvice.login(userInfoVo.getOpenid(), userInfoVo.getOpenid());
            System.out.println(logininfo);
        }
        model.addAttribute("account",accountService.selectByPrimaryKey(UserContext.getCurrent().getId()));
        model.addAttribute("userinfo",userinfoService.selectByPrimaryKey(UserContext.getCurrent().getId()));
        model.addAttribute("expAccount",expAccountService.get(UserContext.getCurrent().getId()));
        return "wechat/personal";
    }
}
