package com.xmg.wechat.wc.util;


import com.alibaba.fastjson.JSON;
import com.xmg.wechat.wc.vo.AccessTokenVo;

import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

public class AccessTokenUtil {
    private static AccessTokenVo token;
    private static Properties p = new Properties();

    static {
        InputStream input = AccessTokenUtil.class.getClassLoader().getResourceAsStream("application_website.properties");
        try {
            p.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        reflash();
    }

    /**
     * 重新获取AccessToken
     */
    private static void reflash() {
        String appid = (String) p.get("wechat.appid");
        String secret = (String) p.get("wechat.secret");
        //调用获取accesstoken接口,获取accesstoken
        String accessToken = UrlUtil.ACCESS_TOKEN_URL.replace("APPID",appid).replace("APPSECRET",secret);
        String result = HttpUtil.get(accessToken);
        token = JSON.parseObject(result, AccessTokenVo.class);
        token.setCreateTime(new Date().getTime());
    }

    public static AccessTokenVo getAccessToken() {
        if (!token.isValid()) {
            reflash();
        }
        return token;
    }
}
