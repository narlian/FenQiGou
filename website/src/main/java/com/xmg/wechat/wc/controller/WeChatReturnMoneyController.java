package com.xmg.wechat.wc.controller;

import com.alibaba.fastjson.JSON;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.UserContext;
import com.xmg.wechat.business.service.IPaymentScheduleService;
import com.xmg.wechat.wc.util.HttpUtil;
import com.xmg.wechat.wc.util.UrlUtil;
import com.xmg.wechat.wc.vo.OAuthAccessTokenVo;
import com.xmg.wechat.wc.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WeChatReturnMoneyController {
    @Value("${wechat.appid}")
    private String appid;
    @Value("${wechat.secret}")
    private String secret;

    @Autowired
    private IPaymentScheduleService paymentScheduleService;
    @Autowired
    private ILogininfoSercvice logininfoSercvice;

    @RequestMapping("/wechatReturnMonryList")
    private String wechatReturnMonryList(String code,String orderBillId, Model model){
        if (UserContext.getCurrent()==null) {
            String result = HttpUtil.get(UrlUtil.OAUTH2_ACCESS_TOKEN_URL
                    .replace("APPID", appid)
                    .replace("SECRET", secret)
                    .replace("CODE", code));
            OAuthAccessTokenVo oAuthAccessToken = JSON.parseObject(result, OAuthAccessTokenVo.class);
            String userInfo = HttpUtil.get(UrlUtil.USERINFO_URL
                    .replace("ACCESS_TOKEN", oAuthAccessToken.getAccess_token())
                    .replace("OPENID", oAuthAccessToken.getOpenid()));
            UserInfoVo userInfoVo = JSON.parseObject(userInfo, UserInfoVo.class);
            Logininfo logininfo = logininfoSercvice.login(userInfoVo.getOpenid(), userInfoVo.getOpenid());
            System.out.println(logininfo);
        }
        if(orderBillId != null){
            model.addAttribute("paymentSchedules",paymentScheduleService.selectByOrderBillId(orderBillId));
        }else{
            model.addAttribute("paymentSchedules",paymentScheduleService.selectByLoginfoId(UserContext.getCurrent().getId()));
        }
        return "wechat/wechatreturnMoney";
    }

    @RequestMapping("/wechatReturnMoney")
    @ResponseBody
    public AjaxResult wechatReturnMoney(Long id){
        AjaxResult result = null;
        try {
            paymentScheduleService.wechatReturnMoney(id);
            result = new AjaxResult("还款成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,"还款失败，"+e.getMessage());
        }
        return result;
    }
}
