package com.xmg.wechat.wc.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter@Getter@NoArgsConstructor
public class TuLingRequest {
    private String key = "98481dfb7b19428f95e207226257b425";
    private String info;
    private String loc;
    private String userid;

    public TuLingRequest(String info, String userid) {
        this.info = info;
        this.userid = userid;
    }
}
