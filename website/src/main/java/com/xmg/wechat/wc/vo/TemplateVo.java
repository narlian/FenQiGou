package com.xmg.wechat.wc.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Setter@Getter
public class TemplateVo {

    private String touser;
    private String template_id;
    private String url;
    private Map<String,TemplateValueVo> data;
}
