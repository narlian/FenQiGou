package com.xmg.wechat.wc.vo;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class TuLingResponse {
    private String code;
    private String text;
}
