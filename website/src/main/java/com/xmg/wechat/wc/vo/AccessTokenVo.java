package com.xmg.wechat.wc.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter@Getter
public class AccessTokenVo {
    private String access_token;
    private Long expires_in;//有效时间
    private Long createTime;//创建时间

    /**
     * 判断accessToken是否有效
     * @return
     * expires_in * 1000:毫秒值
     */
    public Boolean isValid() {
        return (createTime + (expires_in * 1000)) > new Date().getTime();
    }
}
