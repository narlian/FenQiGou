package com.xmg.wechat.wc.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter@Getter
public class CustomMenuVo {
    private String name;
    private String type;
    private String url;
    private String key;
    private List<CustomMenuVo> sub_button;
}
