package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.SystemDictionary;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.SystemDictionaryQueryObject;
import com.xmg.wechat.base.service.ISystemDictionaryService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SystemDictionaryController {
    @Autowired
    private ISystemDictionaryService systemDictionaryService;

    @RequestMapping("/systemDictionary_list")
    @RequiredPermission
    public String systemDictionaryList(@ModelAttribute("qo") SystemDictionaryQueryObject qo, Model model){
        PageResult pageResult = systemDictionaryService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "systemdic/systemDictionary_list";
    }

    @RequiredPermission
    @RequestMapping("/systemDictionary_update")
    @ResponseBody
    public AjaxResult saveOrUpdate(SystemDictionary systemDictionary){
        AjaxResult result = null;
        try {
            systemDictionaryService.saveOrUpdate(systemDictionary);
            result = new AjaxResult(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }
}
