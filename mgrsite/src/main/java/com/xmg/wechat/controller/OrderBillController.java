package com.xmg.wechat.controller;

import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.business.domain.OrderBill;
import com.xmg.wechat.business.query.OrderBillQueryObject;
import com.xmg.wechat.business.service.IOrderBillItemService;
import com.xmg.wechat.business.service.IOrderBillService;
import com.xmg.wechat.business.service.IPaymentScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderBillController {
    @Autowired
    private IOrderBillService orderBillService;
    @Autowired
    private IPaymentScheduleService paymentScheduleService;
    @RequestMapping("/orderbill_list")
    public String orderbill_list(@ModelAttribute("qo")OrderBillQueryObject qo, Model model){
        model.addAttribute("pageResult",orderBillService.query(qo));
        return "orderbill/list";
    }

    @RequestMapping("/averagePayment_list")
    public String averagePayment_list(@ModelAttribute("qo")OrderBillQueryObject qo, Model model){
        qo.setState(OrderBill.PAYMENT_STATE_AUDITING);
        qo.setBuyType(OrderBill.BUYTYPE_AVERAGEPAYMENT);
        model.addAttribute("pageResult",orderBillService.query(qo));
        return "orderbill/audit";
    }

    @RequestMapping("/averagePaymentItem")
    public String averagePaymentItem(Long id,Model model){
        model.addAttribute("orderBill",orderBillService.selectById(id));
        model.addAttribute("payMentSchedules",paymentScheduleService.selectItemsByOrderBillId(id));
        return "orderbill/averagerpaymentitem";
    }

    @RequestMapping("/audit")
    @ResponseBody
    public AjaxResult audit(Long id,int state){
        AjaxResult result = null;
        try {
            orderBillService.audit(id,state);
            result = new AjaxResult("审核成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,"审核失败，"+e.getMessage());
        }
        return result;
    }
}
