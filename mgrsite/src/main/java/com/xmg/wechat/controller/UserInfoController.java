package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.query.LogininfoQueryObject;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserInfoController {
    @Autowired
    private ILogininfoSercvice logininfoSercvice;
    @RequestMapping("/wechat_user_list")
    public String wechatUserList(@ModelAttribute("qo") LogininfoQueryObject qo, Model model) {
        qo.setUserType(Logininfo.USERTYPE_WECHAT);
        model.addAttribute("pageResult",logininfoSercvice.queryPage(qo));
        return "wechatuser/list";
    }
}
