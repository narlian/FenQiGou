package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.VideoAuthQueryObject;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.service.IVideoAuthService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class VideoAuthController {
    @Autowired
    private IVideoAuthService videoAuthService;
    @Autowired
    private ILogininfoSercvice logininfoSercvice;

    @RequestMapping("/vedioAuth")
    public String videoAuthPage(@ModelAttribute("qo") VideoAuthQueryObject qo , Model model){
        PageResult pageResult = videoAuthService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "videoAuth/list";
    }

    @RequestMapping("/vedioAuth_audit")
    @ResponseBody
    public AjaxResult audit(Long loginInfoValue,int state,String remark){
         AjaxResult result = null;
         try {
             videoAuthService.audit(loginInfoValue,state,remark);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }

    @RequestMapping("/videoAuth_autoComplete")
    @ResponseBody
    public List<Logininfo> autoComplete(String keyword){
        List<Logininfo> autoList = new ArrayList<Logininfo>();
        autoList = logininfoSercvice.autoComplete(keyword);
        return autoList;
    }
}
