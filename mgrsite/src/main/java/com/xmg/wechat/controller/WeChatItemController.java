package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.SystemDictionary;
import com.xmg.wechat.base.domain.SystemDictionaryItem;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.SystemDictionaryItemQueryObject;
import com.xmg.wechat.base.service.ISystemDictionaryItemService;
import com.xmg.wechat.base.service.ISystemDictionaryService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.RequiredPermission;
import com.xmg.wechat.business.domain.WeChatMenu;
import com.xmg.wechat.business.domain.WeChatMenuItem;
import com.xmg.wechat.business.query.WeChatMenuItemQueryObject;
import com.xmg.wechat.business.service.IWeChatMenuItemService;
import com.xmg.wechat.business.service.IWeChatMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class WeChatItemController {
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;
    @Autowired
    private ISystemDictionaryService systemDictionaryService;
    @Autowired
    private IWeChatMenuService weChatMenuService;
    @Autowired
    private IWeChatMenuItemService weChatMenuItemService;

    @RequestMapping("/child_menu_list")
    @RequiredPermission
    public String systemDictionaryList(@ModelAttribute("qo") WeChatMenuItemQueryObject qo, Model model){
        PageResult pageResult = weChatMenuItemService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        List<WeChatMenu> weChatMenuGroups = weChatMenuService.selectAll();
        model.addAttribute("weChatMenuItemGroups",weChatMenuGroups);
        return "wechatmenu/wechatmenuItem";
    }

    @RequiredPermission
    @RequestMapping("/wechatmenu_list_update")
    @ResponseBody
    public AjaxResult saveOrUpdate(WeChatMenuItem weChatMenuItem){
        AjaxResult result = null;
        try {
            weChatMenuItemService.saveOrUpdate(weChatMenuItem);
            result = new AjaxResult(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }
}
