package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.SystemDictionary;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.RequiredPermission;
import com.xmg.wechat.business.domain.WeChatMenu;
import com.xmg.wechat.business.query.WeChatMenuQueryObject;
import com.xmg.wechat.business.service.IWeChatMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WeChatController {
    @Value("${wechat.url}")
    private String url;
    @Autowired
    private IWeChatMenuService weChatMenuService;

    @RequestMapping("/parent_menu_list")
    @RequiredPermission
    public String weChatPage(@ModelAttribute("qo") WeChatMenuQueryObject qo, Model model) {
        model.addAttribute("pageResult", weChatMenuService.queryPage(qo));
        return "wechatmenu/wechatmenu";
    }

    @RequiredPermission
    @RequestMapping("/wechatmenu_update")
    @ResponseBody
    public AjaxResult saveOrUpdate(WeChatMenu weChatMenu) {
        AjaxResult result = null;
        try {
            weChatMenuService.saveOrUpdate(weChatMenu);
            result = new AjaxResult(true, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false, e.getMessage());
        }
        return result;
    }
}