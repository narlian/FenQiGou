package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.business.query.BidRequestQueryObject;
import com.xmg.wechat.business.service.IBidRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BidRequestController {
    @Autowired
    private IBidRequestService bidRequestService;

    @RequestMapping("/bidrequest_publishaudit_list")
    public String bidRequestPage(@ModelAttribute("qo") BidRequestQueryObject qo, Model model){
        qo.setBidRequestState(BidConst.BIDREQUEST_STATE_PUBLISH_PENDING);
        PageResult pageResult = bidRequestService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "bidrequest/publish_audit";
    }

    @RequestMapping("/bidrequest_publishaudit")
    @ResponseBody
    public AjaxResult audit(Long id, int state,String remark){
         AjaxResult result = null;
         try {
             bidRequestService.audit(id,state,remark);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }


    @RequestMapping("/bidrequest_audit1_list")
    public String audit1List(@ModelAttribute("qo") BidRequestQueryObject qo, Model model){
        qo.setBidRequestState(BidConst.BIDREQUEST_STATE_APPROVE_PENDING_1);
        PageResult pageResult = bidRequestService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "bidrequest/audit1";
    }

    @RequestMapping("/bidrequest_audit1")
    @ResponseBody
    public AjaxResult fullAudit1(Long id, int state,String remark){
         AjaxResult result = null;
         try {
             bidRequestService.fullAudit1(id,state,remark);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }

    @RequestMapping("/bidrequest_audit2_list")
    public String audit2List(@ModelAttribute("qo") BidRequestQueryObject qo, Model model){
        qo.setBidRequestState(BidConst.BIDREQUEST_STATE_APPROVE_PENDING_2);
        PageResult pageResult = bidRequestService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "bidrequest/audit2";
    }

    @RequestMapping("/bidrequest_audit2")
    @ResponseBody
    public AjaxResult fullAudit2(Long id, int state,String remark){
        AjaxResult result = null;
        try {
            bidRequestService.fullAudit2(id,state,remark);
            result = new AjaxResult(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }
}
