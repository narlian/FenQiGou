package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.Product;
import com.xmg.wechat.base.query.ProductQueryObject;
import com.xmg.wechat.base.service.IProductService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2017/11/8.
 */
@Controller
public class ProductController {
    @Autowired
    private IProductService productService;

    @RequestMapping("/product_list")
    public String productPage(@ModelAttribute("qo") ProductQueryObject qo, Model model) {
        model.addAttribute("pageResult", productService.queryPage(qo));
        return "product/product_list";
    }

    @RequestMapping("/product_update")
    @ResponseBody
    public AjaxResult saveOrUpdate(Product product) {
        AjaxResult result = null;
        try {
            productService.saveOrUpdate(product);
            result = new AjaxResult("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false, e.getMessage());
        }
        return result;
    }

    @RequestMapping("/product_delete")
    @ResponseBody
    public AjaxResult delete(Long id) {
        AjaxResult result = null;
        try {
            productService.delete(id);
            result = new AjaxResult("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false, e.getMessage());
        }
        return result;
    }
}
