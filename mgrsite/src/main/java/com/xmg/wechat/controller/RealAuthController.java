
package com.xmg.wechat.controller;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.RealAuthQueryObject;
import com.xmg.wechat.base.service.IRealAuthService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RealAuthController {
    @Autowired
    private IRealAuthService realAuthService;


    @RequestMapping("/realAuth")
    public String realAuth(@ModelAttribute("qo") RealAuthQueryObject qo, Model model){
        PageResult pageResult = realAuthService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "realAuth/list";
    }

    @RequestMapping("/realAuth_audit")
    @ResponseBody
    public AjaxResult audit(Long id,int state,String remark){
         AjaxResult result = null;
                 try {
                     realAuthService.audit(id,state,remark);
                     result = new AjaxResult(true,"操作成功");
                 } catch (Exception e) {
                     e.printStackTrace();
                     result = new AjaxResult(false,e.getMessage());
                 }
                 return result;
    }
}
