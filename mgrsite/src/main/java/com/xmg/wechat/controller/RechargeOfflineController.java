package com.xmg.wechat.controller;

import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.business.query.RechargeOfflineQueryObject;
import com.xmg.wechat.business.service.IPlatformBankinfoService;
import com.xmg.wechat.business.service.IRechargeOfflineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RechargeOfflineController {
    @Autowired
    private IRechargeOfflineService rechargeOfflineService;
    @Autowired
    private IPlatformBankinfoService platformBankinfoService;

    @RequestMapping("/rechargeOffline")
    public String rechargePage(@ModelAttribute("qo") RechargeOfflineQueryObject qo, Model model){
        model.addAttribute(rechargeOfflineService.queryPage(qo));
        model.addAttribute("banks",platformBankinfoService.sellectAll());
        return "rechargeoffline/list";
    }

    @RequestMapping("/rechargeOffline_audit")
    @ResponseBody
    public AjaxResult audit(Long id,int state,String  remark){
         AjaxResult result = null;
         try {
             rechargeOfflineService.audit(id, state,remark);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;

    }
}
