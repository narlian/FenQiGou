package com.xmg.wechat.controller;

import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.UserFileQueryObject;
import com.xmg.wechat.base.service.IUserFileService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserFileController {
    @Autowired
    private IUserFileService userFileService;

    @RequestMapping("/userFileAuth")
    public String userFileAuth(@ModelAttribute("qo")UserFileQueryObject qo , Model model){
        PageResult pageResult = userFileService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        return "userFileAuth/list";
    }

    @RequestMapping("/userFile_audit")
    @ResponseBody
    public AjaxResult audit(Long id,int score,int state,String remark){
         AjaxResult result = null;
         try {
             userFileService.audit(id,score,state,remark);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }
}
