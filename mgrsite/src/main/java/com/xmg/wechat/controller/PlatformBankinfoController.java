package com.xmg.wechat.controller;

import com.xmg.wechat.business.domain.PlatformBankinfo;
import com.xmg.wechat.business.query.PlatformBankinfoQueryObject;
import com.xmg.wechat.business.service.IPlatformBankinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PlatformBankinfoController {

    @Autowired
    private IPlatformBankinfoService platformBankinfoService;
    @RequestMapping("/companyBank_list")
    public String platformBankinfoPage(@ModelAttribute("qo") PlatformBankinfoQueryObject qo, Model model){
        model.addAttribute("pageResult",platformBankinfoService.queryPage(qo));
        return "platformbankinfo/list";
    }

    @RequestMapping("/companyBank_update")
    public String saveOrUpdate(PlatformBankinfo platformBankinfo){
        platformBankinfoService.saveOrUpdate(platformBankinfo);
        return "redirect:/companyBank_list";
    }
}
