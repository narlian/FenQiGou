package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.business.domain.BidRequest;
import com.xmg.wechat.business.query.BidRequestQueryObject;
import com.xmg.wechat.business.query.PaymentScheduleQueryObject;
import com.xmg.wechat.business.service.IBidRequestService;
import com.xmg.wechat.business.service.IPaymentScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExpBidRequestController {
    @Autowired
    private IBidRequestService bidRequestService;
    @Autowired
    private IPaymentScheduleService paymentScheduleService;

    @RequestMapping("/expBidRequestPublishPage")
    public String expBidRequestPublishPage(Model model){
        model.addAttribute("minBidRequestAmount", BidConst.SMALLEST_BIDREQUEST_AMOUNT);
        model.addAttribute("minBidAmount", BidConst.SMALLEST_BID_AMOUNT);
        return "expbidrequest/expbidrequestpublish";
    }

    @RequestMapping("/expBidRequestPublish")
    @ResponseBody
    public AjaxResult expBidRequestPublish(BidRequest bidRequest){
         AjaxResult result = null;
         try {
             bidRequestService.applyExp(bidRequest);
             result = new AjaxResult(true,"操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             result = new AjaxResult(false,e.getMessage());
         }
         return result;
    }

    @RequestMapping("/expBidRequest_list")
    public String expBidRequestList(@ModelAttribute("qo") BidRequestQueryObject qo, Model model){
        qo.setBidRequestType(BidConst.BIDREQUEST_TYPE_EXP);
        model.addAttribute("pageResult", bidRequestService.queryPage(qo));
        return "expbidrequest/expbidrequestlist";
    }
    /**
     *列出体验标还款列表
     */
    @RequestMapping("/expBidRequestReturn_list")
    public String expBidRequestReturnList(@ModelAttribute("qo") PaymentScheduleQueryObject qo , Model model){
        qo.setBidRequestType(BidConst.BIDREQUEST_TYPE_EXP);
        model.addAttribute("pageResult",paymentScheduleService.queryPage(qo));
        return "expbidrequest/expbidreturnrequestlist";
    }

    @RequestMapping("/returnMoney")
    @ResponseBody
    public AjaxResult returnMoney(Long id){
        AjaxResult result = null;
        try {
            paymentScheduleService.returnMoney(id);
            result = new AjaxResult(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }
}
