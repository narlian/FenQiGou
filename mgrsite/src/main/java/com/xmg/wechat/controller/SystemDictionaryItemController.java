package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.SystemDictionary;
import com.xmg.wechat.base.domain.SystemDictionaryItem;
import com.xmg.wechat.base.page.PageResult;
import com.xmg.wechat.base.query.SystemDictionaryItemQueryObject;
import com.xmg.wechat.base.service.ISystemDictionaryItemService;
import com.xmg.wechat.base.service.ISystemDictionaryService;
import com.xmg.wechat.base.util.AjaxResult;
import com.xmg.wechat.base.util.RequiredPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class SystemDictionaryItemController {
    @Autowired
    private ISystemDictionaryItemService systemDictionaryItemService;
    @Autowired
    private ISystemDictionaryService systemDictionaryService;

    @RequestMapping("/systemDictionaryItem_list")
    @RequiredPermission
    public String systemDictionaryList(@ModelAttribute("qo") SystemDictionaryItemQueryObject qo, Model model){
        PageResult pageResult = systemDictionaryItemService.queryPage(qo);
        model.addAttribute("pageResult",pageResult);
        List<SystemDictionary> systemDictionaryGroups = systemDictionaryService.selectAll();
        model.addAttribute("systemDictionaryGroups",systemDictionaryGroups);
        return "systemdic/systemDictionaryItem_list";
    }

    @RequiredPermission
    @RequestMapping("/systemDictionaryItem_update")
    @ResponseBody
    public AjaxResult saveOrUpdate(SystemDictionaryItem systemDictionaryItem){
        AjaxResult result = null;
        try {
            systemDictionaryItemService.saveOrUpdate(systemDictionaryItem);
            result = new AjaxResult(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }
}
