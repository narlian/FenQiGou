package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.BidConst;
import com.xmg.wechat.base.domain.Userinfo;
import com.xmg.wechat.base.service.IRealAuthService;
import com.xmg.wechat.base.service.IUserFileService;
import com.xmg.wechat.base.service.IUserinfoService;
import com.xmg.wechat.business.domain.BidRequest;
import com.xmg.wechat.business.service.IBidRequestAuditHistoryService;
import com.xmg.wechat.business.service.IBidRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BorrowInfoController {
    @Autowired
    private IBidRequestService bidRequestService;
    @Autowired
    private IUserinfoService userinfoService;
    @Autowired
    private IBidRequestAuditHistoryService bidRequestAuditHistoryService;
    @Autowired
    private IRealAuthService realAuthService;
    @Autowired
    private IUserFileService userFileService;

    @RequestMapping("/borrow_info")
    public String borrowInfoPage(Long id,Model model){
        BidRequest bidRequest = bidRequestService.get(id);
        String returnPage = "";
        if(bidRequest != null){
            model.addAttribute("bidRequest",bidRequest);
            model.addAttribute("audits",bidRequestAuditHistoryService.getListByBidRequestId(bidRequest.getId()));
            if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_NORMAL){
                Userinfo createUser = userinfoService.selectByPrimaryKey(bidRequest.getCreateUser().getId());
                model.addAttribute("userInfo",createUser);
                model.addAttribute("realAuth",realAuthService.get(createUser.getRealAuthId()));
                model.addAttribute("userFiles",userFileService.queryAuditListByLogininfo(bidRequest.getCreateUser().getId()));
                returnPage="bidrequest/borrow_info";
            }else  if(bidRequest.getBidRequestType() == BidConst.BIDREQUEST_TYPE_EXP){
                returnPage="expbidrequest/borrow_info";
            }
        }
        return returnPage;
    }
}
