package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {
    @Autowired
    private ILogininfoSercvice logininfoSercvice;
    @RequestMapping("/mgrLogin")
    @ResponseBody
    public AjaxResult mgrLogin(String username,String password){
        AjaxResult result = null;
            Logininfo logininfo = logininfoSercvice.login(username, password);
            if(logininfo != null ){
                result = new AjaxResult("登录成功");
            }else {
                result = new AjaxResult(false,"登录失败");
            }
        return result;
    }
}
