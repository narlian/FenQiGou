package com.xmg.wechat.controller;

import com.xmg.wechat.base.domain.AutoResponse;
import com.xmg.wechat.base.domain.Logininfo;
import com.xmg.wechat.base.query.AutoResponseQueryObject;
import com.xmg.wechat.base.query.MessageQueryObject;
import com.xmg.wechat.base.service.IAutoResponseService;
import com.xmg.wechat.base.service.ILogininfoSercvice;
import com.xmg.wechat.base.service.IMessageService;
import com.xmg.wechat.base.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.jws.WebParam;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MessageController {

    @Autowired
    private ILogininfoSercvice logininfoSercvice;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private IAutoResponseService autoResponseService;

    @RequestMapping("/message_list")
    public String messageListPage(@ModelAttribute("qo") MessageQueryObject qo, Model model){
        model.addAttribute("pageResult", messageService.queryPage(qo));
        return "message/list";
    }

    @RequestMapping("/message_autoComplete")
    @ResponseBody
    public List<Logininfo> autoComplete(String keyword){
        List<Logininfo> autoList = new ArrayList<Logininfo>();
        autoList = logininfoSercvice.autoComplete(keyword);
        return autoList;
    }

    @RequestMapping("/auto_response_list")
    public String autoResponsePage(@ModelAttribute("qo") AutoResponseQueryObject qo , Model model){
        model.addAttribute("pageResult", autoResponseService.queryPage(qo));
        return "autoResponse/list";
    }

    @RequestMapping("/autoResponse_save")
    @ResponseBody
    public AjaxResult save(AutoResponse autoResponse){
        AjaxResult result = null;
        try {
            autoResponseService.saveOrUpdate(autoResponse);
            result = new AjaxResult();
        } catch (Exception e){
            e.printStackTrace();
            result = new AjaxResult(false,e.getMessage());
        }
        return result;
    }


}
