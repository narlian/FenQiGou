package com.xmg.wechat;

import com.CoreConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@SpringBootConfiguration
@Import({CoreConfig.class,WebMVCConfig.class})
@PropertySource("classpath:application_mgrsite.properties")
public class MgrsiteConfig {

    public static void main(String[] args) {
        SpringApplication.run(MgrsiteConfig.class,args);
    }
}
