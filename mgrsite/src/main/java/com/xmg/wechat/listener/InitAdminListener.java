package com.xmg.wechat.listener;

import com.xmg.wechat.base.service.ILogininfoSercvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class InitAdminListener implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private ILogininfoSercvice logininfoSercvice;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        //System.out.println("Spring容器监听事件" + logininfoSercvice);
        //初始化一个后台管理员
        logininfoSercvice.initAdmin();
    }
}
