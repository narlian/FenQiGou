package com.xmg.wechat.interceptor;


import com.xmg.wechat.base.util.UserContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckLoginInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(handler instanceof HandlerMethod){
            //后台处了登录请求, 所有请求都要求登录
            if(UserContext.getCurrent() == null){
                response.sendRedirect("/login.html");
                return false;
            }
        }
        return true;
    }
}
