<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>蓝源Eloan-P2P平台(系统管理平台)</title>
<#include "../common/header.ftl"/>
<script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
<script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js" ></script>

<script type="text/javascript">
	$(function() {
		$("#beginDate,#endDate").click(function(){
			WdatePicker();
		});
		
		$('#pagination').twbsPagination({
			totalPages : ${pageResult.totalPage}||1,
			startPage : ${pageResult.currentPage},
			visiblePages : 5,
			first : "首页",
			prev : "上一页",
			next : "下一页",
			last : "最后一页",
			onPageClick : function(event, page) {
				$("#currentPage").val(page);
				$("#searchForm").submit();
			}
		});
		
		$("#query").click(function(){
			$("#currentPage").val(1);
			$("#searchForm").submit();
		})
		
		//注册审核按钮事件
		$(".auditClass").click(function(){
			var json=$(this).data("json");
			$("#editform")[0].reset();
			$("#editForm_id").val(json.id);
			$("#applier").text(json.applier);
			$("#fileType").text(json.fileType);
			$("#image").attr("src",json.image);
			$("#myModal").modal("show");
		});
	});
</script>
</head>
<body>
	<div class="container">
		<#include "../common/top.ftl"/>
		<div class="row">
			<div class="col-sm-3">
				<#assign currentMenu="orderbill_list" />
				<#include "../common/menu.ftl" />
			</div>
			<div class="col-sm-9">
				<div class="page-header">
					<h3>订单列表页面</h3>
				</div>
				<form id="searchForm" class="form-inline" method="post" action="/orderbill_list">
					<input type="hidden" id="currentPage" name="currentPage" value=""/>
					<div class="form-group">
					    <label>状态</label>
					    <select class="form-control" name="state">
					    	<option value="-1">全部</option>
					    	<option value="0">待审核</option>
					    	<option value="1">还款中</option>
					    	<option value="2">已还清</option>
					    </select>
					    <script type="text/javascript">
					    	$("[name=state] option[value='${(qo.state)!''}']").attr("selected","selected");
					    </script>
					</div>
					<div class="form-group">
					    <label>购买类型</label>
					    <select class="form-control" name="buyType">
					    	<option value="-1">全部</option>
					    	<option value="0">全额付款</option>
					    	<option value="1">分期付款</option>
					    </select>
					    <script type="text/javascript">
					    	$("[name=buyType] option[value='${(qo.buyType)!''}']").attr("selected","selected");
					    </script>
					</div>
					<div class="form-group">
					    <label>购买时间</label>
					    <input class="form-control" type="text" name="beginDate" id="beginDate" value="${(qo.beginDate?string('yyyy-MM-dd'))!''}" />到
					    <input class="form-control" type="text" name="endDate" id="endDate" value="${(qo.endDate?string('yyyy-MM-dd'))!''}" />
					</div>
					<div class="form-group">
						<button id="query" class="btn btn-success"><i class="icon-search"></i> 查询</button>
					</div>
				</form>
				<div class="panel panel-default">
					<table class="table">
						<thead>
							<tr>
								<th>商品名称</th>
								<th>商品数量</th>
								<th>总金额</th>
								<th>还款期数</th>
								<th>订单状态</th>
								<th>购买人</th>
								<th>购买类型</th>
								<th>购买时间</th>
								<th>审核人</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<#list pageResult.listData as orderbill>
							<form action="/averagePaymentItem" method="post">
							<tr>
								<input type="hidden" name="id" value="${orderbill.id}"/>
								<td>${orderbill.product.name}</td>
								<td>${orderbill.count}</td>
								<td>${(orderbill.totalAmount)!''}</td>
								<td>${(orderbill.repaymentNumber)!''}</td>
								<td>${(orderbill.stateDisplay)!""}</td>
								<td>${(orderbill.purchaser.realName)!""}</td>
								<td>${(orderbill.buyTypeDisplay)!""}</td>
								<td>${(orderbill.buyTime)!""}</td>
								<td>${(orderbill.auditor.realName)!""}</td>
                                <td>
									<#if (orderbill.buyType == 1 && orderbill.state > 0)>
                                        <input class="btn btn-default" type="submit" value="查看详情">
									</#if>
                                </td>
							</tr>
                            </form>
						</#list>
						</tbody>
					</table>
					<div style="text-align: center;">
						<ul id="pagination" class="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>