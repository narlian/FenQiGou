<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>蓝源Eloan-P2P平台(系统管理平台)</title>
	<#include "../common/header.ftl"/>
	<script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
	<script type="text/javascript" src="/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>
	
	<script type="text/javascript">
	$(function(){
		$(function() {
			$("#pagination").twbsPagination({
				totalPages:${pageResult.totalPage}||1,
				visiblePages:5,
				startPage:${qo.currentPage},
				first:"首页",
				prev:"上一页",
				next:"下一页",
				last:"尾页",
				onPageClick:function(event,page){
					$("#currentPage").val(page);
					$("#searchForm").submit();
				}
			});
			
			$("#query").click(function(){
				$("#currentPage").val(1);
				$("#searchForm").submit();
			});
			//添加数据字典按钮
			$("#addSystemDictionaryBtn").click(function(){
			    $("#editForm").clearForm(true);
			    $("#systemDictionaryModal").modal("show");
			});
			//
			$("#saveBtn").click(function () {
				$("#editForm").ajaxSubmit({
					dataType:'json',
					success:function (data) {
						if (data.success) {
                            $("#currentPage").val(1);
                            $("#searchForm").submit();
						} else {
                            $.messager.popup(data.msg);
						}
                    }
				})
            });
			//修改按钮
            $(".edit_Btn").click(function () {
                $("#editForm").clearForm(true);
                var data = $(this).data("json");
				$("#wechatmenuId").val(data.id);
				$("#name").val(data.name);
				$("#type").val(data.type);
				$("#key").val(data.key);
				$("#url").val(data.url);
                $("#systemDictionaryModal").modal("show");
            });
			//
		});
	});
	</script>
</head>

<body>
	<div class="container">
		<#include "../common/top.ftl"/>
		<div class="row">
			<div class="col-sm-3">
				<#assign currentMenu = "wechatmenu" />
				<#include "../common/menu.ftl" />
			</div>
			<div class="col-sm-9">
				<div class="page-header">
					<h3>父菜单管理</h3>
				</div>
				<div class="row">
					<!-- 提交分页的表单 -->
					<form id="searchForm" class="form-inline" method="post" action="/parent_menu_list">
						<input type="hidden" name="currentPage" id="currentPage" value=""/>
						<div class="form-group">
						</div>
						<div class="form-group">
						    <label>关键字</label>
						    <input class="form-control" type="text" name="keyword" value="${(qo.keyword)!''}">
						</div>
						<div class="form-group">
							<button id="query" type="button" class="btn btn-success"><i class="icon-search"></i> 查询</button>
							<a href="javascript:void(-1);" class="btn btn-success" id="addSystemDictionaryBtn">添加父类菜单</a>
							<a href="http://helen.s1.natapp.cc/customMenu1" class="btn btn-success"">刷新微信菜单</a>
						</div>
					</form>
				</div>
				<div class="row">
					<table class="table">
						<thead>
							<tr>
								<th>名称</th>
								<th>类型</th>
								<th>菜单值</th>
								<th>路径</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						<#list pageResult.listData as vo>
							<tr>
								<td>${vo.name}</td>
								<td>${vo.type}</td>
								<td>${vo.key}</td>
								<td>${vo.url}</td>
								<td>
									<a href="javascript:void(-1);" data-json='${vo.jsonStr}' class="edit_Btn">修改</a>
								</td>
							</tr>
						</#list>
						</tbody>
					</table>
					
					<div style="text-align: center;">
						<ul id="pagination" class="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="systemDictionaryModal" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">编辑/增加</h4>
	      </div>
	      <div class="modal-body">
	      		<form id="editForm" class="form-horizontal" method="post" action="/wechatmenu_update" style="margin: -3px 118px">
			    <input id="wechatmenuId" type="hidden" name="id" value="" />
			   	<div class="form-group">
				    <label class="col-sm-3 control-label">名称</label>
				    <div class="col-sm-6">
				    	<input type="text" class="form-control" id="name" name="name" placeholder="菜单名称">
				    </div>
				</div>
				<div class="form-group">
				    <label class="col-sm-3 control-label">类型</label>
				    <div class="col-sm-6">
				    	<input type="text" class="form-control" id="type" name="type" placeholder="菜单类型">
				    </div>
				</div>
				<div class="form-group">
				    <label class="col-sm-3 control-label">菜单值</label>
				    <div class="col-sm-6">
				    	<input type="text" class="form-control" id="key" name="key" placeholder="菜单值">
				    </div>
				</div>
				<div class="form-group">
				    <label class="col-sm-3 control-label">路径</label>
				    <div class="col-sm-6">
				    	<input type="text" class="form-control" id="url" name="url" placeholder="菜单路径">
				    </div>
				</div>
		   </form>
		  </div>
	      <div class="modal-footer">
	      	<a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
		    <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
	      </div>
	    </div>
	  </div>
	</div>
</body>
</html>