<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>蓝源Eloan-P2P平台(系统管理平台)</title> <#include "../common/header.ftl"/>
<script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
<script type="text/javascript"
	src="/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript"
	src="/js/plugins/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript"
	src="/js/plugins/bootstrap3-typeahead.min.js"></script>

<script type="text/javascript">
	$(function(){
		$("#beginDate,#endDate").click(function(){
			WdatePicker();
		});

		$('#pagination').twbsPagination({
			totalPages : ${pageResult.totalPage} || 1,
			startPage : ${pageResult.currentPage},
			visiblePages : 5,
			first : "首页",
			prev : "上一页",
			next : "下一页",
			last : "最后一页",
			onPageClick : function(event, page) {
			    console.log(page);
				$("#currentPage").val(page);
				$("#searchForm").submit();
			}
		});
		
		$("#query").click(function(){
            $("[name=currentPage]").val("1");
            $("#searchForm").submit();
        });
		//添加审核
		$("#addVedioAuthBtn").click(function () {
		    $("#editForm").clearForm(true);
			$("#vedioAuthModal").modal("show");
        });
		//保存按钮
		$(".btn_audit").click(function () {
			$("#editForm").ajaxSubmit({
				dataType:'json',
				success:function (data) {
					if(data.success){
                        $("[name=currentPage]").val("1");
                        $("#searchForm").submit();
					}else{
					    $.messager.popup(data.msg);
					}
                }
			});
        });

        //更改按钮
        $(".edit_Btn").click(function () {
            $("#editForm").clearForm(true);
            var data = $(this).data("json");
            $("#autoResponseId").val(data.id);
            $("#keyword").val(data.keyword);
            $("[name = 'replyContent']").val(data.replyContent);
            $("#vedioAuthModal").modal("show");
        });

    });
	</script>
</head>

<body>
	<div class="container">
		<#include "../common/top.ftl"/>
		<div class="row">
			<div class="col-sm-3"><#assign currentMenu="auto_response_list" />
				<#include "../common/menu.ftl" /></div>
			<div class="col-sm-9">
				<div class="page-header">
					<h3>自动回复信息管理</h3>
				</div>
				<div class="row">
					<!-- 提交分页的表单 -->
					<form id="searchForm" class="form-inline" method="post"
						action="/auto_response_list">
						<input type="hidden" name="currentPage" id="currentPage" value="" />
						<div class="form-group">
							<label>关键字</label>
							<input class="form-control" type="text" name="keywords" value="${(qo.keywords)!''}"  autocomplete="off">
                            <input type="hidden" name="loginInfoValue" id="loginInfoValue"/>
						</div>
						<div class="form-group">
							<button id="query" class="btn btn-success">
								<i class="icon-search"></i> 查询
							</button>
							<a href="javascript:void(-1);" class="btn btn-success"
								id="addVedioAuthBtn">添加</a>
						</div>
					</form>
				</div>
				<div class="row">
					<table class="table">
						<thead>
							<tr>
								<th>接收内容</th>
								<th>回复内容</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<#list pageResult.listData as vo>
							<tr>
								<td>${vo.keyword}</td>
								<td>${vo.replyContent}</td>
                                <td>
                                    <a href="javascript:void(-1);" data-json='${vo.jsonStr}' class="edit_Btn">修改</a>
                                </td>
							</tr>
							</#list>
						</tbody>
					</table>

					<div style="text-align: center;">
						<ul id="pagination" class="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="vedioAuthModal" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">添加自动回复模板</h4>
				</div>
				<div class="modal-body">
					<form id="editForm" class="form-horizontal" method="post"
						action="/autoResponse_save">
                        <input id="autoResponseId" type="hidden" name="id" value="" />
						<div class="form-group">
							<label class="col-sm-2 control-label">接收内容</label>
							<div class="col-sm-6">
								<input id="keyword" type="text" name="keyword">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="name">回复内容</label>
							<div class="col-sm-6">
								<textarea name="replyContent" rows="4" cols="40"></textarea>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-success btn_audit">添加</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>