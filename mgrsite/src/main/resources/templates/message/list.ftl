<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>蓝源Eloan-P2P平台(系统管理平台)</title> <#include "../common/header.ftl"/>
<script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
<script type="text/javascript"
	src="/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript"
	src="/js/plugins/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript"
	src="/js/plugins/bootstrap3-typeahead.min.js"></script>

<script type="text/javascript">
	$(function(){
		$("#beginDate,#endDate").click(function(){
			WdatePicker();
		});

		$('#pagination').twbsPagination({
			totalPages : ${pageResult.totalPage} || 1,
			startPage : ${pageResult.currentPage},
			visiblePages : 5,
			first : "首页",
			prev : "上一页",
			next : "下一页",
			last : "最后一页",
			onPageClick : function(event, page) {
				$("#currentPage").val(page);
				$("#searchForm").submit();
			}
		});
		
		$("#query").click(function(){
			$("[name=currentPage]").val("1");
			$("#searchForm").submit();
		});

		//动态回显
        var $input = $("input[name = 'keyword']");
        console.log($input);
        $input.typeahead({
			source:function(query,process){
			    $.ajax({
					url:"/message_autoComplete",
                    type: 'post',
					dataType:'json',
					data:{keyword:query},
					success:function(data){
					    process(data);
					}
				})
			},
			minLength:4,
			autoSelect:true,
			items:5,
			displayText:function(item){
				return item.username;
			}
        }).change(function() {
            var current = $input.typeahead("getActive");
            if (current) {
				$("#loginInfoValue").val(current.id);
            }
        });

    });
	</script>
</head>

<body>
	<div class="container">
		<#include "../common/top.ftl"/>
		<div class="row">
			<div class="col-sm-3"><#assign currentMenu="vedioAuth" />
				<#include "../common/menu.ftl" /></div>
			<div class="col-sm-9">
				<div class="page-header">
					<h3>微信信息收发管理</h3>
				</div>
				<div class="row">
					<!-- 提交分页的表单 -->
					<form id="searchForm" class="form-inline" method="post"
						action="/message_list">
						<input type="hidden" name="currentPage" value="" />
						<div class="form-group">
							<label>用户ID</label>
                            <input type="hidden" name="loginInfoValue" id="loginInfoValue"/>
							<input class="form-control" type="text" name="keyword" value="${(qo.keyword)!''}"  autocomplete="off">
						</div>
						<div class="form-group">
							<label>发送时间</label> <input class="form-control"
								style="width: 130px;" type="text" name="beginDate"
								id="beginDate" value="${(qo.beginDate?string('yyyy-MM-dd'))!''}" />到
							<input class="form-control" style="width: 130px;" type="text"
								name="endDate" id="endDate"
								value="${(qo.endDate?string('yyyy-MM-dd'))!''}" />
						</div>
						<div class="form-group">
							<button id="query" class="btn btn-success">
								<i class="icon-search"></i> 查询
							</button>
						</div>
					</form>
				</div>
				<div class="row">
					<table class="table">
						<thead>
							<tr>
								<th>用户名</th>
								<th>接收内容</th>
								<th>回复内容</th>
								<th>收发时间</th>
							</tr>
						</thead>
						<tbody>
							<#list pageResult.listData as vo>
							<tr>
								<td>${vo.logininfo.username}</td>
								<td>${vo.receiveContent}</td>
								<td>${vo.replyContent}</td>
								<td>${(vo.inputTime?string("yyyy-MM-dd HH:mm:ss"))!""}
							</tr>
							</#list>
						</tbody>
					</table>

					<div style="text-align: center;">
						<ul id="pagination" class="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>