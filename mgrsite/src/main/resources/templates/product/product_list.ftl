<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>蓝源Eloan-P2P平台(系统管理平台)</title>
<#include "../common/header.ftl"/>
    <script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery.twbsPagination.min.js"></script>

    <script type="text/javascript">
        $(function () {
            $(function () {
                $("#pagination").twbsPagination({
                            totalPages:${pageResult.totalPage} || 1,
                        visiblePages
                :
                5,
                        startPage
                :${qo.currentPage},
                first:"首页",
                        prev
                :
                "上一页",
                        next
                :
                "下一页",
                        last
                :
                "尾页",
                        onPageClick
                :
                function (event, page) {
                    $("#currentPage").val(page);
                    $("#searchForm").submit();
                }
            })
                ;
                //添加按钮
                $("#addProductBtn").click(function () {
                    $("#editForm").clearForm(true);
                    $("#productModal").modal("show");
                });
                //保存按钮
                $("#saveBtn").click(function () {
                    $("#editForm").ajaxSubmit({
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                $("#currentPage").val(1);
                                $("#searchForm").submit();
                            } else {
                                $.messager.popup(data.msg);
                            }
                        }
                    });
                });
                //更改按钮
                $(".edit_Btn").click(function () {
                    $("#editForm").clearForm(true);
                    var data = $(this).data("json");
                    $("#productId").val(data.id);
                    $("#name").val(data.name);
                    $("#costPrice").val(data.costPrice);
                    $("#salePrice").val(data.salePrice);
                    $("#imagePath").val(data.imagePath);
                    $("#intro").val(data.intro);
                    $("#brandName").val(data.brandName);
                    $("#sn").val(data.sn);
                    $("#productModal").modal("show");
                });
                //查询按钮
                $("#query").click(function () {
                    $("#currentPage").val(1);
                    $("#searchForm").submit();
                });
                //删除按钮
                $(".delete_Btn").click(function () {
                    var data = $(this).data("json");
                    $("#productId").val(data.id);
                    var idVal = $("#productId").val();
                    alert(idVal);
                    $.ajax({
                        dataType:"json",
                        data:{id:idVal},
                        url:"/product_delete",
                        success: function (data) {
                            if(data.success){
                                window.location.reload();
                            }else{
                                $.messager.popup(data.msg);
                            }
                        }
                    })
                });
            });
        });
    </script>
</head>

<body>
<div class="container">
<#include "../common/top.ftl"/>
    <div class="row">
        <div class="col-sm-3">
        <#assign currentMenu = "product" />
				<#include "../common/menu.ftl" />
        </div>
        <div class="col-sm-9">
            <div class="page-header">
                <h3>商品管理</h3>
            </div>
            <div class="row">
                <!-- 提交分页的表单 -->
                <form id="searchForm" class="form-inline" method="post" action="/product_list">
                    <input type="hidden" name="currentPage" id="currentPage" value=""/>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <label>关键字</label>
                        <input class="form-control" type="text" name="keyword" value="${(qo.keyword)!''}">
                    </div>
                    <div class="form-group">
                        <button id="query" type="button" class="btn btn-success"><i class="icon-search"></i> 查询</button>
                        <a href="javascript:void(-1);" class="btn btn-success" id="addProductBtn">添加商品</a>
                    </div>
                </form>
            </div>
            <div class="row">
                <table class="table">
                    <thead>
                    <tr>
                        <th>名称</th>
                        <th>编码</th>
                        <th>成本价格</th>
                        <th>销售价格</th>
                        <th>商品描述</th>
                        <th>品牌名称</th>
                        <th>图片</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#list pageResult.listData as vo>
                    <tr>
                        <td>${vo.name}</td>
                        <td>${vo.sn}</td>
                        <td>${vo.costPrice}</td>
                        <td>${vo.salePrice}</td>
                        <td>${vo.intro}</td>
                        <td>${vo.brandName}</td>
                        <td><img src="${vo.imagePath}" class="img-rounded" style="width: 50px;height: 50px"></td>
                        <td>
                            <a href="javascript:void(-1);" data-json='${vo.jsonStr}' class="edit_Btn">修改</a>
                            <a href="javascript:void(-1);" data-json='${vo.jsonStr}' class="delete_Btn">删除</a>
                        </td>
                    </tr>
                    </#list>
                    </tbody>
                </table>

                <div style="text-align: center;">
                    <ul id="pagination" class="pagination"></ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="productModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/product_update"
                      style="margin: -3px 118px">
                    <input id="productId" type="hidden" name="id" value=""/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" placeholder="名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">编码</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="sn" name="sn" placeholder="编码">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">成本价格</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="costPrice" name="costPrice" placeholder="成本价格">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">销售价格</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="salePrice" name="salePrice" placeholder="销售价格">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">商品描述</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="intro" name="intro" placeholder="商品描述">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">品牌名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="brandName" name="brandName" placeholder="品牌名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">图片路径</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="imagePath" name="imagePath" placeholder="图片路径">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>